/******************************************************************************

                      M M C L I E N T . H

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
#ifndef _MMCLIENT_H_
#define _MMCLIENT_H_
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <stdint.h>
#include <stdlib.h>

/*============================================================================
                             COMMON DEFS
============================================================================*/
#define MMCLIENT_MAX_DBUS_NAME_LEN  (256)
#define MMCLIENT_MAX_SRVC_HNDL_LEN   25
#define VAR_UNUSED(X) (void)(X)

typedef enum
{
  MMCLIENT_NAD_INSTANCE_INVALID = -1,
  MMCLIENT_NAD_INSTANCE_0,
  MMCLIENT_NAD_INSTANCE_1,
  MMCLIENT_NAD_INSTANCE_2
} mmclient_nad_inst_t;

typedef enum
{
  MMCLIENT_FAILURE,
  MMCLIENT_SUCCESS
} mmclient_status_t;

typedef enum
{
  MMCLIENT_SERVER_DOWN,
  MMCLIENT_SERVER_READY
} mmclient_server_status_t;

typedef enum
{
  MMCLIENT_SYS_IND_OOS,
  MMCLIENT_SYS_IND_IS
} mmclient_sys_ind_t;

typedef struct
{
  mmclient_nad_inst_t nad_inst;   /* NAD which went down/came back up */
  mmclient_sys_ind_t  sys_ind;    /* OOS and IS event */
} mmclient_sys_status_t;

typedef enum
{
  MMCLIENT_IND_TYPE_SERVER,
  MMCLIENT_IND_TYPE_SYSTEM
} mmclient_sys_ind_type_t;

typedef struct
{
  mmclient_sys_ind_type_t ind_type;
  union
  {
    mmclient_server_status_t server_status;
    mmclient_sys_status_t    system_status;
  } data;
} mmclient_sys_ind_info_t;

typedef void (*mmclient_sys_cb_func_t)(mmclient_sys_ind_info_t*, void *);

/* System CB information */
typedef struct
{
  mmclient_sys_cb_func_t cb;
  void                   *userdata;
} mmclient_sys_cb_info_t;

typedef void *mmclient_hndl_t;

/*============================================================================
                             NAD DEFS
============================================================================*/
#define MMCLIENT_IMEI_LEN       (32)
#define MMCLIENT_MEID_LEN       (32)
#define MMCLIENT_SW_VERSION_LEN (128)

/* Opaque handle to the NAD (Network Access Device) instance */
typedef mmclient_hndl_t mmclient_nad_hndl_t;

/* NAD status */
typedef enum
{
  MMCLIENT_NAD_STATUS_UNKNOWN,
  MMCLIENT_NAD_OFFLINE,
  MMCLIENT_NAD_ONLINE,
  MMCLIENT_NAD_FORCE_DETACH,
  MMCLIENT_NAD_SHUTDOWN
} mmclient_nad_status_t;

/* Information that will be received as part of the NAD ind callback function */
typedef struct
{
  mmclient_nad_inst_t   nad_inst;
  mmclient_nad_status_t nad_status;
} mmclient_nad_ind_info_t;

/* Callback function which will receive status of a particular NAD instance */
typedef void (*mmclient_nad_cb_func_t)(mmclient_nad_ind_info_t*, void *);

/* NAD Callback information */
typedef struct
{
  mmclient_nad_cb_func_t cb;
  void                   *userdata;
} mmclient_nad_cb_info_t;

typedef uint32_t mmclient_nad_info_validity_mask_t;

#define MMCLIENT_NAD_IMEI_MASK        0x01
#define MMCLIENT_NAD_MEID_MASK        0x02
#define MMCLIENT_NAD_SW_VERSION_MASK  0x04

typedef struct
{
  char                              imei[MMCLIENT_IMEI_LEN+1];
  char                              meid[MMCLIENT_MEID_LEN+1];
  char                              sw_version[MMCLIENT_SW_VERSION_LEN+1];
  mmclient_nad_info_validity_mask_t validity_mask;
} mmclient_nad_device_info_t;

typedef uint64_t mmclient_nad_time_t;

/*===========================================================================
  FUNCTION  mmclient_get_num_nad_instances
===========================================================================*/
/*!
@brief
  Function to query the number of configured NAD instances

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg *num_nads - will be filled with the number of configured NAD instances
*/
/*=========================================================================*/
mmclient_status_t mmclient_get_num_nad_instances(uint32_t *num_nads);

/*===========================================================================
  FUNCTION  mmclient_get_nad_handle
===========================================================================*/
/*!
@brief
  Function to get a handle for the specified NAD instance

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_inst - NAD instance to connect
@arg *cb_info - Callback information

@note
  Dependencies
    - mmclient_get_num_nad_instances() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_nad_hndl_t mmclient_get_nad_handle
(
  mmclient_nad_inst_t    nad_inst,
  mmclient_nad_cb_info_t *cb_info
);

/*===========================================================================
  FUNCTION  mmclient_get_nad_status
===========================================================================*/
/*!
@brief
  Function to query current status of a given NAD

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl    - NAD handle
@arg *nad_status - status of the NAD will be filled here

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_get_nad_status
(
  mmclient_nad_hndl_t   nad_hndl,
  mmclient_nad_status_t *nad_status
);

/*===========================================================================
  FUNCTION  mmclient_set_nad_status
===========================================================================*/
/*!
@brief
  Function to set the status (operating mode) of a given NAD

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl   - NAD handle
@arg nad_status - status of the NAD

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_set_nad_status
(
  mmclient_nad_hndl_t   nad_hndl,
  mmclient_nad_status_t nad_status
);

/*===========================================================================
  FUNCTION  mmclient_get_nad_device_info
===========================================================================*/
/*!
@brief
  Function to get the device info from a given NAD

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl     - NAD handle
@arg *device_info - output param containing IMEI, MEID, and sw_version strings

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_get_nad_device_info
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_nad_device_info_t *device_info
);

/*===========================================================================
  FUNCTION  mmclient_release_nad_handle
===========================================================================*/
/*!
@brief
  Function to release a particular NAD reference

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle to release

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_release_nad_handle
(
  mmclient_nad_hndl_t nad_hndl
);

/*============================================================================
                             DATA CONTROL DEFS
============================================================================*/

#define MMCLIENT_MAX_ADDR_INFO    (10)
#define MMCLIENT_MAX_IFACE_NAME_LEN (12)
#define MMCLIENT_IPV6_ADDR_SIZE   (16)
#define MMCLIENT_MAX_STR_LEN      (512)
#define MMCLIENT_MAX_APN_LEN      (MMCLIENT_MAX_STR_LEN)
#define MMCLIENT_MAX_USERNAME_LEN (MMCLIENT_MAX_STR_LEN)
#define MMCLIENT_MAX_PASSWORD_LEN (MMCLIENT_MAX_STR_LEN)

#define MMCLIENT_STR_V4_BUF_SIZE     (16)
#define MMCLIENT_STR_V6_BUF_SIZE     (43)

typedef mmclient_hndl_t mmclient_data_hndl_t;
typedef uint32_t mmclient_ce_code_t;

typedef enum
{
  MMCLIENT_IP_FAMILY_V4,
  MMCLIENT_IP_FAMILY_V6,
  MMCLIENT_IP_FAMILY_V4V6
} mmclient_ip_family_t;

typedef enum
{
  MMCLIENT_TECH_PREF_NONE,
  MMCLIENT_TECH_PREF_3GPP2,
  MMCLIENT_TECH_PREF_3GPP
} mmclient_tech_pref_t;

typedef enum
{
  MMCLIENT_AUTH_PREF_NONE,
  MMCLIENT_AUTH_PREF_PAP,
  MMCLIENT_AUTH_PREF_CHAP,
  MMCLIENT_AUTH_PREF_PAP_CHAP
} mmclient_auth_pref_t;

typedef enum
{
  MMCLIENT_DATA_EVT_INVALID,
  MMCLIENT_DATA_EVT_NET_IS_CONN,       /* Call connected */
  MMCLIENT_DATA_EVT_NET_NO_NET,        /* Call disconnected */
  MMCLIENT_DATA_EVT_NET_RECONFIGURED,  /* Existing call address was reconfigured */
  MMCLIENT_DATA_EVT_NET_NEWADDR,       /* A new address was added on an existing call */
  MMCLIENT_DATA_EVT_NET_DELADDR,       /* Address was removed from the interface */
  MMCLIENT_DATA_EVT_MAX
} mmclient_data_call_evt_t;

typedef enum
{
  MMCLIENT_CE_TYPE_UNKNOWN,
  MMCLIENT_CE_TYPE_MOBILE_IP,
  MMCLIENT_CE_TYPE_INTERNAL,
  MMCLIENT_CE_TYPE_CALL_MANAGER_DEFINED,
  MMCLIENT_CE_TYPE_3GPP_SPEC_DEFINED,
  MMCLIENT_CE_TYPE_PPP,
  MMCLIENT_CE_TYPE_EHRPD,
  MMCLIENT_CE_TYPE_IPV6
} mmclient_ce_reason_t;

typedef enum
{
  MMCLIENT__DATA_BEARER_TECH_UNKNOWN,       /* Unknown bearer. */

  /* CDMA related data bearer technologies */
  MMCLIENT_DATA_BEARER_TECH_CDMA_1X,       /* 1X technology. */
  MMCLIENT_DATA_BEARER_TECH_EVDO_REV0,     /* CDMA Rev 0. */
  MMCLIENT_DATA_BEARER_TECH_EVDO_REVA,     /* CDMA Rev A. */
  MMCLIENT_DATA_BEARER_TECH_EVDO_REVB,     /* CDMA Rev B. */
  MMCLIENT_DATA_BEARER_TECH_EHRPD,         /* EHRPD. */
  MMCLIENT_DATA_BEARER_TECH_HRPD,          /* HRPD */

  /* UMTS related data bearer technologies */
  MMCLIENT_DATA_BEARER_TECH_WCDMA,         /* WCDMA. */
  MMCLIENT_DATA_BEARER_TECH_GPRS,          /* GPRS. */
  MMCLIENT_DATA_BEARER_TECH_HSDPA,         /* HSDPA. */
  MMCLIENT_DATA_BEARER_TECH_HSUPA,         /* HSUPA. */
  MMCLIENT_DATA_BEARER_TECH_EDGE,          /* EDGE. */
  MMCLIENT_DATA_BEARER_TECH_LTE,           /* LTE. */
  MMCLIENT_DATA_BEARER_TECH_HSDPA_PLUS,    /* HSDPA+. */
  MMCLIENT_DATA_BEARER_TECH_DC_HSDPA_PLUS, /* DC HSDPA+. */
  MMCLIENT_DATA_BEARER_TECH_HSPA,          /* HSPA */
  MMCLIENT_DATA_BEARER_TECH_64_QAM,        /* 64 QAM. */
  MMCLIENT_DATA_BEARER_TECH_TDSCDMA,       /* TD-SCDMA. */
  MMCLIENT_DATA_BEARER_TECH_GSM            /* GSM. */
} mmclient_data_bearer_tech_t;

typedef struct
{
  mmclient_ip_family_t ip_family;
  mmclient_tech_pref_t tech_pref;
  mmclient_auth_pref_t auth_pref;
  unsigned char        apn_name[MMCLIENT_MAX_APN_LEN];
  unsigned char        user_name[MMCLIENT_MAX_USERNAME_LEN];
  unsigned char        password[MMCLIENT_MAX_PASSWORD_LEN];
  uint8_t              enable_silent_retry; /* Default Disabled (0) */
  uint8_t              enable_partial_retry; /* Default Disabled (0) */
} mmclient_call_params_t;

typedef struct
{
  uint32_t ip_type; /* AF_INET or AF_INET6 */
  union
  {
    uint32_t ipv4;
    uint8_t  ipv6[MMCLIENT_IPV6_ADDR_SIZE];
  } addr;
} mmclient_ip_addr_t;

typedef struct
{
  mmclient_ip_addr_t iface_addr;
  uint32_t           iface_mask;
  mmclient_ip_addr_t gtwy_addr;
  uint32_t           gtwy_mask;
  mmclient_ip_addr_t dnsp_addr;
  mmclient_ip_addr_t dnss_addr;
} mmclient_addr_info_t;

typedef struct
{
  unsigned char      iface[MMCLIENT_MAX_IFACE_NAME_LEN+1];
  uint32_t           num_addrs;
  uint32_t           valid_v4;
  uint32_t           valid_v6;

  /*V4*/
  uint32_t           ip_type_v4;
  unsigned char      iface_addr_v4[MMCLIENT_STR_V4_BUF_SIZE];
  uint32_t           iface_mask_v4;
  unsigned char      gtwy_addr_v4[MMCLIENT_STR_V4_BUF_SIZE];
  uint32_t           gtwy_mask_v4;
  unsigned char      dnsp_addr_v4[MMCLIENT_STR_V4_BUF_SIZE];
  unsigned char      dnss_addr_v4[MMCLIENT_STR_V4_BUF_SIZE];

  /*V6*/
  uint32_t           ip_type_v6;
  unsigned char      iface_addr_v6[MMCLIENT_STR_V6_BUF_SIZE];
  uint32_t           iface_mask_v6;
  unsigned char      gtwy_addr_v6[MMCLIENT_STR_V6_BUF_SIZE];
  uint32_t           gtwy_mask_v6;
  unsigned char      dnsp_addr_v6[MMCLIENT_STR_V6_BUF_SIZE];
  unsigned char      dnss_addr_v6[MMCLIENT_STR_V6_BUF_SIZE];

} mmclient_call_info_formatter_t;

typedef struct
{
  unsigned char         iface[MMCLIENT_MAX_IFACE_NAME_LEN+1];
  uint32_t              num_addrs;
  mmclient_addr_info_t  addrs[MMCLIENT_MAX_ADDR_INFO];
} mmclient_call_info_t;

typedef struct
{
  mmclient_ce_reason_t  type;
  mmclient_ce_code_t    code;
} mmclient_ce_info_t;

typedef struct
{
  unsigned char apn_name[MMCLIENT_MAX_APN_LEN];
  uint8_t       is_ipv4_throttled;
  uint8_t       is_ipv6_throttled;
  uint32_t      remaining_ipv4_throttled_time; /* In Milliseconds */
  uint32_t      remaining_ipv6_throttled_time; /* In Milliseconds */
}mmclient_throttle_info_t;

typedef struct
{
  mmclient_ip_family_t     ip_family;
  mmclient_ce_info_t       ip4;
  mmclient_ce_info_t       ip6;
  uint8_t                  is_ipv4_reason_permanent;
  uint8_t                  is_ipv6_reason_permanent;
  mmclient_throttle_info_t throttle_info;
} mmclient_call_end_reason_t;

typedef void (*mmclient_data_cb_func_t)
(
  mmclient_data_hndl_t *data_hndl,
  void* user_data,
  mmclient_data_call_evt_t evt
);

/*===========================================================================
  FUNCTION  mmclient_data_get_srvc_hndl
===========================================================================*/
/*!
@brief
  Used to get data service handle. This handle can be used by the caller
  to exercise data service functionalities. The data handle will be tied
  to the specified NAD handle. The user needs to pass in a callback
  function in order to receive notifications about the call state
  ex. connection, disconnected etc

@return
  mmclient_data_hndl_t reference

@arg nad_hndl - NAD handle
@arg cb - Callback function to return events related to call state
@arg user_data - any information the client wants to pass along

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_data_hndl_t mmclient_data_get_srvc_hndl
(
  mmclient_nad_hndl_t      nad_hndl,
  mmclient_data_cb_func_t  cb,
  void                     *user_data
);

/*===========================================================================
  FUNCTION  mmclient_data_start_call
===========================================================================*/
/*!
@brief
  Used to trigger a data call. The data handle would be tied to a specific
  NAD so the call also will be brought up on that specific NAD. The caller
  will pass in the call parameters on which the call needs to be brought up

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg call_params - call parameters

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_start_call
(
  mmclient_data_hndl_t   data_hndl,
  mmclient_call_params_t *call_params
);

/*===========================================================================
  FUNCTION  mmclient_data_stop_call
===========================================================================*/
/*!
@brief
  This function can be used to stop a data call

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_stop_call
(
  mmclient_data_hndl_t data_hndl
);

/*===========================================================================
  FUNCTION  mmclient_data_get_call_info
===========================================================================*/
/*!
@brief
  Function used for querying the call information

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg *call_info - call information

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_get_call_info
(
  mmclient_data_hndl_t data_hndl,
  mmclient_call_info_t *call_info
);

/*===========================================================================
  FUNCTION  mmclient_data_release_srvc_hndl
===========================================================================*/
/*!
@brief
  Function used to release the data service handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_release_srvc_hndl
(
  mmclient_data_hndl_t data_hndl
);

/*===========================================================================
  FUNCTION  mmclient_data_get_call_end_reason
===========================================================================*/
/*!
@brief
  Function used to query call end information

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg ce - out param for passing call end reason information

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_get_call_end_reason
(
  mmclient_data_hndl_t       data_hndl,
  mmclient_call_end_reason_t *ce
);

/*===========================================================================
  FUNCTION  mmclient_data_get_current_bearer_tech
===========================================================================*/
/*!
@brief
  Function used to query current bearer technology

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg bt - out param for passing bearer tech information

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_get_current_bearer_tech
(
  mmclient_data_hndl_t        data_hndl,
  mmclient_data_bearer_tech_t *bt
);

/*============================================================================
                       PROFILE MANAGEMENT DEFS
============================================================================*/
typedef struct
{
  mmclient_ip_family_t  ip_family;
  mmclient_tech_pref_t  tech_pref;
  mmclient_auth_pref_t  auth_pref;
  unsigned char         apn_name[MMCLIENT_MAX_APN_LEN];
  unsigned char         user_name[MMCLIENT_MAX_USERNAME_LEN];
  unsigned char         password[MMCLIENT_MAX_PASSWORD_LEN];
} mmclient_profile_params_t;

typedef uint32_t mmclient_profile_id_t;

/*===========================================================================
  FUNCTION  mmclient_profile_query
===========================================================================*/
/*!
@brief
  Function used to lookup a modem profile based on provided parameters

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *profile_params - Profile parameters that will be used in the
                       lookup
@arg *prof_num - Profile number if found

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_query
(
  mmclient_nad_hndl_t       nad_hndl,
  mmclient_profile_params_t *profile_params,
  mmclient_profile_id_t     *prof_num
);

/*===========================================================================
  FUNCTION  mmclient_profile_create
===========================================================================*/
/*!
@brief
  Function used to create a modem profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *profile_params - Profile parameters that will be used in creation
@arg *prof_num - Profile number if created successfully

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_create
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_profile_params_t  *profile_params,
  mmclient_profile_id_t      *prof_num
);

/*===========================================================================
  FUNCTION  mmclient_profile_delete
===========================================================================*/
/*!
@brief
  Function used to delete a modem profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *prof_num - Profile number to be deleted

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_delete
(
  mmclient_nad_hndl_t    nad_hndl,
  mmclient_profile_id_t  prof_num
);

/*===========================================================================
  FUNCTION  mmclient_profile_modify
===========================================================================*/
/*!
@brief
  Function used to modify an existing profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *prof_num - Profile number to modify
@arg *new_profile_params - new profile parameters

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_modify
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_profile_id_t      prof_num,
  mmclient_profile_params_t  *new_profile_params
);

/*===========================================================================
  FUNCTION  mmclient_profile_set_initial_attach
===========================================================================*/
/*!
@brief
  Function used to set initial attach profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *prof_num - Profile num used for initial attach

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_set_initial_attach
(
  mmclient_nad_hndl_t    nad_hndl,
  mmclient_profile_id_t  prof_num
);

/*============================================================================
                          BANDWIDTH MANAGEMENT DEFS
============================================================================*/

typedef enum mmclient_bw_ind_events_e {
  MMCLIENT_BW_INVALID_IND_EV,
  MMCLIENT_BW_DOWNLINK_THROUGHPUT_IND_EV,
  MMCLIENT_BW_UPLINK_THROUGHPUT_IND_EV,
  MMCLIENT_BW_UPLINK_DOWNLINK_THROUGHPUT_IND_EV,
  MMCLIENT_BW_MAX_IND_EV
} mmclient_bw_ind_events_t;

typedef struct
{
  uint32_t  uplink_rate;
  uint32_t  queue_size;
  uint32_t  confidence_level;
}mmclient_bw_ul_ind_info_t;

typedef struct
{
  uint32_t  downlink_rate;
  uint32_t  confidence_level;
}mmclient_bw_dl_ind_info_t;


typedef struct
{
  uint8_t                    ul_info_valid;
  mmclient_bw_ul_ind_info_t  ul_info;
  uint8_t                    dl_info_valid;
  mmclient_bw_dl_ind_info_t  dl_info;
} mmclient_bw_ind_t;

typedef struct
{
  mmclient_nad_inst_t  inst;
  mmclient_bw_ind_t    ind;
} mmclient_bw_ind_info_t;

typedef struct
{
  unsigned char             nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  unsigned int              evt;
} mmclient_bw_query_t;

typedef struct
{
  unsigned char             nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  unsigned int              evt;
  mmclient_bw_ind_info_t    ind_cb;
}mmclient_bw_ind_container_t;

typedef void (*mmclient_bw_ind_cb_func_t)
(
  mmclient_bw_ind_events_t  evt,
  mmclient_bw_ind_info_t *info,
  void *user_data
);

typedef struct
{
  mmclient_bw_ind_cb_func_t  cb;
  void                       *user_data;
} mmclient_bw_cb_info_t;

/*============================================================================
  FUNCTION  mmclient_bandwidth_register_ind
============================================================================*/
  /*!
  @brief
    Registers for the requested bandwidth related indication on the modem
    associated with the given NAD handle

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
mmclient_status_t mmclient_bandwidth_register_ind
(
  mmclient_nad_hndl_t      nad_hndl,
  mmclient_bw_ind_events_t evt,
  mmclient_bw_cb_info_t   *bw_cb
);

/*============================================================================
  FUNCTION  mmclient_bandwidth_query
============================================================================*/
  /*!
  @brief
    Quries current bandwidth setting on the modem associated with the
    given NAD handle

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
mmclient_status_t mmclient_bandwidth_query
(
  mmclient_nad_hndl_t     nad_hndl,
  mmclient_bw_ind_info_t  *bw_info,
  mmclient_bw_ind_events_t evt
);

/*============================================================================
                        NETWORK MANAGEMENT DEFS
============================================================================*/
typedef mmclient_hndl_t mmclient_nw_reg_hndl_t;

#define MMCLIENT_MCC_LEN            (3)
#define MMCLIENT_MNC_LEN            MMCLIENT_MCC_LEN

#define MMCLIENT_MAX_OPERATORS      (40) /* NAS_3GPP_NETWORK_INFO_LIST_MAX_V01 */
#define MMCLIENT_OPERATOR_NAME_LEN  (63)
#define MMCLIENT_IMSI_LEN           (15)
#define MMCLIENT_SPN_LEN            (15)

typedef uint32_t mmclient_nw_reg_validity_mask_t;

#define MMCLIENT_NW_REG_MODE_MASK             0x01
#define MMCLIENT_NW_REG_STATUS_MASK           0x02
#define MMCLIENT_NW_REG_LAC_MASK              0x04
#define MMCLIENT_NW_REG_CELL_ID_MASK          0x08
#define MMCLIENT_NW_REG_MCC_MASK              0x10
#define MMCLIENT_NW_REG_MNC_MASK              0x20
#define MMCLIENT_NW_REG_TECH_MASK             0x40
#define MMCLIENT_NW_REG_OPERATOR_NAME_MASK    0x80
#define MMCLIENT_NW_REG_SIGNAL_STRENGTH_MASK  0x100
#define MMCLIENT_NW_REG_OPERATOR_STATUS_MASK  0x200

typedef enum
{
  MMCLIENT_NW_REG_MODE_AUTO,
  MMCLIENT_NW_REG_MODE_MANUAL
} mmclient_nw_reg_mode_t;

typedef enum
{
  MMCLIENT_NW_REG_STATUS_UNREGISTERED,
  MMCLIENT_NW_REG_STATUS_REGISTERED,
  MMCLIENT_NW_REG_STATUS_ROAMING,
  MMCLIENT_NW_REG_STATUS_SEARCHING,
  MMCLIENT_NW_REG_STATUS_DENIED,
  MMCLIENT_NW_REG_STATUS_UNKNOWN
} mmclient_nw_reg_status_t;

typedef enum
{
  MMCLIENT_NW_REG_OPERATOR_STATUS_CURRENT,
  MMCLIENT_NW_REG_OPERATOR_STATUS_AVAILABLE,
  MMCLIENT_NW_REG_OPERATOR_STATUS_FORBIDDEN,
  MMCLIENT_NW_REG_OPERATOR_STATUS_UNKNOWN
} mmclient_nw_reg_operator_status_t;

typedef enum
{
  MMCLIENT_NW_REG_TECH_MIN,
  MMCLIENT_NW_REG_TECH_GSM = MMCLIENT_NW_REG_TECH_MIN,
  MMCLIENT_NW_REG_TECH_EDGE,
  MMCLIENT_NW_REG_TECH_UMTS,
  MMCLIENT_NW_REG_TECH_HSPA,
  MMCLIENT_NW_REG_TECH_LTE,
  MMCLIENT_NW_REG_TECH_MAX = MMCLIENT_NW_REG_TECH_LTE
} mmclient_nw_reg_tech_t;

typedef uint16_t mmclient_nw_reg_lac_t;
typedef uint32_t mmclient_nw_reg_cell_id_t;
typedef int8_t   mmclient_nw_reg_signal_strength_t;

typedef struct
{
  mmclient_nw_reg_mode_t             mode;
  mmclient_nw_reg_status_t           status;
  mmclient_nw_reg_lac_t              lac;
  mmclient_nw_reg_cell_id_t          cell_id;
  mmclient_nw_reg_tech_t             technology;
  mmclient_nw_reg_signal_strength_t  signal_strength;
  char                               mcc[MMCLIENT_MCC_LEN+1];
  char                               mnc[MMCLIENT_MNC_LEN+1];
  char                               operator_name[MMCLIENT_OPERATOR_NAME_LEN+1];
  mmclient_nw_reg_operator_status_t  operator_status;
  mmclient_nw_reg_validity_mask_t    validity_mask;
} mmclient_nw_reg_properties_t;

typedef struct
{
  mmclient_nw_reg_properties_t  operators[MMCLIENT_MAX_OPERATORS];
  uint16_t                      num_operators;
} mmclient_nw_reg_scanned_operators_t;

typedef enum
{
  MMCLIENT_NW_REG_SEL_PREF_MANUAL,
  MMCLIENT_NW_REG_SEL_PREF_AUTO
} mmclient_nw_reg_system_sel_pref_t;

typedef struct
{
  mmclient_nw_reg_system_sel_pref_t system_sel_pref;
  char                              mcc[MMCLIENT_MCC_LEN+1];
  char                              mnc[MMCLIENT_MNC_LEN+1];
  mmclient_nw_reg_tech_t            technology;
} mmclient_nw_reg_register_network_info_t;

typedef uint32_t mmclient_nw_reg_radio_settings_tech_pref_t;

#define  MMCLIENT_NW_REG_TECH_PREF_ANY  0x1
#define  MMCLIENT_NW_REG_TECH_PREF_GSM  0x2
#define  MMCLIENT_NW_REG_TECH_PREF_UMTS 0x4
#define  MMCLIENT_NW_REG_TECH_PREF_LTE  0x8

typedef uint32_t mmclient_nw_reg_radio_settings_gsm_band_t;

#define  MMCLIENT_NW_REG_GSM_BAND_ANY   0x1
#define  MMCLIENT_NW_REG_GSM_BAND_850   0x2
#define  MMCLIENT_NW_REG_GSM_BAND_900P  0x4
#define  MMCLIENT_NW_REG_GSM_BAND_900E  0x8
#define  MMCLIENT_NW_REG_GSM_BAND_1800  0x10
#define  MMCLIENT_NW_REG_GSM_BAND_1900  0x20

typedef uint32_t mmclient_nw_reg_radio_settings_umts_band_t;

#define MMCLIENT_NW_REG_UMTS_BAND_ANY   0x1
#define MMCLIENT_NW_REG_UMTS_BAND_850   0x2
#define MMCLIENT_NW_REG_UMTS_BAND_900   0x4
#define MMCLIENT_NW_REG_UMTS_BAND_1700  0x8
#define MMCLIENT_NW_REG_UMTS_BAND_1900  0x10
#define MMCLIENT_NW_REG_UMTS_BAND_2100  0x20

typedef uint64_t mmclient_nw_reg_radio_settings_lte_band_t;
#define MMCLIENT_NW_REG_LTE_BAND_1  (0x000000000000001ull) /* Bit  0  -- E-UTRA Operating Band 1 */
#define MMCLIENT_NW_REG_LTE_BAND_2  (0x000000000000002ull) /* Bit  1  -- E-UTRA Operating Band 2 */
#define MMCLIENT_NW_REG_LTE_BAND_3  (0x000000000000004ull) /* Bit  2  -- E-UTRA Operating Band 3 */
#define MMCLIENT_NW_REG_LTE_BAND_4  (0x000000000000008ull) /* Bit  3  -- E-UTRA Operating Band 4 */
#define MMCLIENT_NW_REG_LTE_BAND_5  (0x000000000000010ull) /* Bit  4  -- E-UTRA Operating Band 5 */
#define MMCLIENT_NW_REG_LTE_BAND_6  (0x000000000000020ull) /* Bit  5  -- E-UTRA Operating Band 6 */
#define MMCLIENT_NW_REG_LTE_BAND_7  (0x000000000000040ull) /* Bit  6  -- E-UTRA Operating Band 7 */
#define MMCLIENT_NW_REG_LTE_BAND_8  (0x000000000000080ull) /* Bit  7  -- E-UTRA Operating Band 8 */
#define MMCLIENT_NW_REG_LTE_BAND_9  (0x000000000000100ull) /* Bit  8  -- E-UTRA Operating Band 9 */
#define MMCLIENT_NW_REG_LTE_BAND_10 (0x000000000000200ull) /* Bit  9  -- E-UTRA Operating Band 10 */
#define MMCLIENT_NW_REG_LTE_BAND_11 (0x000000000000400ull) /* Bit  10 -- E-UTRA Operating Band 11 */
#define MMCLIENT_NW_REG_LTE_BAND_12 (0x000000000000800ull) /* Bit  11 -- E-UTRA Operating Band 12 */
#define MMCLIENT_NW_REG_LTE_BAND_13 (0x000000000001000ull) /* Bit  12 -- E-UTRA Operating Band 13 */
#define MMCLIENT_NW_REG_LTE_BAND_14 (0x000000000002000ull) /* Bit  13 -- E-UTRA Operating Band 14 */
#define MMCLIENT_NW_REG_LTE_BAND_17 (0x000000000010000ull) /* Bit  16 -- E-UTRA Operating Band 17 */
#define MMCLIENT_NW_REG_LTE_BAND_18 (0x000000000020000ull) /* Bit  17 -- E-UTRA Operating Band 18 */
#define MMCLIENT_NW_REG_LTE_BAND_19 (0x000000000040000ull) /* Bit  18 -- E-UTRA Operating Band 19 */
#define MMCLIENT_NW_REG_LTE_BAND_20 (0x000000000080000ull) /* Bit  19 -- E-UTRA Operating Band 20 */
#define MMCLIENT_NW_REG_LTE_BAND_21 (0x000000000100000ull) /* Bit  20 -- E-UTRA Operating Band 21 */
#define MMCLIENT_NW_REG_LTE_BAND_23 (0x000000000400000ull) /* Bit  22 -- E-UTRA Operating Band 23 */
#define MMCLIENT_NW_REG_LTE_BAND_24 (0x000000000800000ull) /* Bit  23 -- E-UTRA Operating Band 24 */
#define MMCLIENT_NW_REG_LTE_BAND_25 (0x000000001000000ull) /* Bit  24 -- E-UTRA Operating Band 25 */
#define MMCLIENT_NW_REG_LTE_BAND_26 (0x000000002000000ull) /* Bit  25 -- E-UTRA Operating Band 26 */
#define MMCLIENT_NW_REG_LTE_BAND_28 (0x000000008000000ull) /* Bit  27 -- E-UTRA Operating Band 28 */
#define MMCLIENT_NW_REG_LTE_BAND_29 (0x000000010000000ull) /* Bit  28 -- E-UTRA Operating Band 29 */
#define MMCLIENT_NW_REG_LTE_BAND_32 (0x000000020000000ull) /* Bit  29 -- E-UTRA Operating Band 32 */
#define MMCLIENT_NW_REG_LTE_BAND_30 (0x000000080000000ull) /* Bit  31 -- E-UTRA Operating Band 30 */
#define MMCLIENT_NW_REG_LTE_BAND_33 (0x000000100000000ull) /* Bit  32 -- E-UTRA Operating Band 33 */
#define MMCLIENT_NW_REG_LTE_BAND_34 (0x000000200000000ull) /* Bit  33 -- E-UTRA Operating Band 34 */
#define MMCLIENT_NW_REG_LTE_BAND_35 (0x000000400000000ull) /* Bit  34 -- E-UTRA Operating Band 35 */
#define MMCLIENT_NW_REG_LTE_BAND_36 (0x000000800000000ull) /* Bit  35 -- E-UTRA Operating Band 36 */
#define MMCLIENT_NW_REG_LTE_BAND_37 (0x000001000000000ull) /* Bit  36 -- E-UTRA Operating Band 37 */
#define MMCLIENT_NW_REG_LTE_BAND_38 (0x000002000000000ull) /* Bit  37 -- E-UTRA Operating Band 38 */
#define MMCLIENT_NW_REG_LTE_BAND_39 (0x000004000000000ull) /* Bit  38 -- E-UTRA Operating Band 39 */
#define MMCLIENT_NW_REG_LTE_BAND_40 (0x000008000000000ull) /* Bit  39 -- E-UTRA Operating Band 40 */
#define MMCLIENT_NW_REG_LTE_BAND_41 (0x000010000000000ull) /* Bit  40 -- E-UTRA Operating Band 41 */
#define MMCLIENT_NW_REG_LTE_BAND_42 (0x000020000000000ull) /* Bit  41 -- E-UTRA Operating Band 42 */
#define MMCLIENT_NW_REG_LTE_BAND_43 (0x000040000000000ull) /* Bit  42 -- E-UTRA Operating Band 43 */

typedef uint32_t mmclient_nw_reg_radio_setting_validity_mask_t;

#define MMCLIENT_RADIO_SETTING_TECH_PREF_MASK  0x1
#define MMCLIENT_RADIO_SETTING_GSM_BAND_MASK   0x2
#define MMCLIENT_RADIO_SETTING_UMTS_BAND_MASK  0x4
#define MMCLIENT_RADIO_SETTING_LTE_BAND_MASK   0x8

typedef struct
{
  mmclient_nw_reg_radio_setting_validity_mask_t  validity_mask;
  mmclient_nw_reg_radio_settings_tech_pref_t     tech_pref;
  mmclient_nw_reg_radio_settings_gsm_band_t      gsm_band;
  mmclient_nw_reg_radio_settings_umts_band_t     umts_band;
  mmclient_nw_reg_radio_settings_lte_band_t      lte_band;
} mmclient_nw_reg_radio_settings_t;

typedef enum
{
  MMCLIENT_TIME_SOURCE_UNKNOWN,
  MMCLIENT_TIME_SOURCE_CDMA_1X,
  MMCLIENT_TIME_SOURCE_CDMA_1XEVDO,
  MMCLIENT_TIME_SOURCE_GSM,
  MMCLIENT_TIME_SOURCE_UMTS,
  MMCLIENT_TIME_SOURCE_LTE,
  MMCLIENT_TIME_SOURCE_TDSCMA
} mmclient_nw_reg_time_source_t;

typedef struct
{
  uint16_t year;          /* Year - such as 2017 */
  uint8_t  month;         /* Month - 1 is January and 12 is December. */
  uint8_t  day;           /* Day - Range: 1 to 31 */
  uint8_t  hour;          /* Hour - Range: 0 to 23 */
  uint8_t  minute;        /* Minute - Range: 0 to 59 */
  uint8_t  second;        /* Second - Range: 0 to 59 */
  uint8_t  day_of_week;   /* Day of the week - 0 is Monday and 6 is Sunday. */
  int8_t   time_zone;     /* Time zone offset from Universal Time i.e., the difference
                             between local time and Universal time, in
                             increments of 15 min (signed value).*/
  uint8_t  daylt_sav_adj; /* Daylight saving adjustment in hours.
                             Possible values: 0, 1, and 2. This field is ignored if
                             radio_if is MMCLIENT_TIME_SOURCE_CDMA_1XEVDO. */
  mmclient_nw_reg_time_source_t time_source; /* Radio interface - Source of time info */
} mmclient_nw_reg_time_t;

typedef void (*mmclient_nw_reg_ind_cb_fn_t)
(
  mmclient_nw_reg_properties_t *nw_reg_properties,
  void* userdata
);

typedef void (*mmclient_nw_reg_scan_cb_fn_t)
(
  mmclient_nw_reg_scanned_operators_t *scanned_operators,
  void* userdata
);


/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_srvc_hndl
===========================================================================*/
/*!
@brief
  Used to get network registration service handle. This handle can be used
  by the caller to exercise network service related functionalities.

@return
  mmclient_nw_reg_hndl_t reference

@arg nad_hndl - Data call handle
@arg user_data - any information which the caller wants to pass along

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_nw_reg_hndl_t mmclient_nw_reg_get_srvc_hndl
(
  mmclient_nad_hndl_t  nad_hndl,
  void                 *user_data
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_release_srvc_hndl
===========================================================================*/
/*!
@brief
  Release the network service handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_release_srvc_hndl
(
  mmclient_nw_reg_hndl_t nw_reg_hndl
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_properties_register_ind
===========================================================================*/
/*!
@brief
  Function to register for network service related indications

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg cb_fn - Callback to be invoked when one of the registered events happen
@arg validity_mask - bitmask to indicate which indications caller is
                     interested in
@arg *user_data - any information user wishes to pass along

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_properties_register_ind
(
  mmclient_nw_reg_hndl_t          nw_reg_hndl,
  mmclient_nw_reg_ind_cb_fn_t     cb_fn,
  mmclient_nw_reg_validity_mask_t validity_mask,
  void*                           user_data
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_properties
===========================================================================*/
/*!
@brief
  Function to query for the registered network properties

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *nw_reg_properties - out param which will get filled with the
                          network properties

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_properties
(
  mmclient_nw_reg_hndl_t       nw_reg_hndl,
  mmclient_nw_reg_properties_t *nw_reg_properties
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_register_network
===========================================================================*/
/*!
@brief
  Function to register to a particular network

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *register_network_info - Information about the network to register to

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_register_network
(
  mmclient_nw_reg_hndl_t                  nw_reg_hndl,
  mmclient_nw_reg_register_network_info_t *register_network_info
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_operator
===========================================================================*/
/*!
@brief
  Function to get the current operator

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *curr_operator - information about the current operator will be
                      filled here

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_operator
(
  mmclient_nw_reg_hndl_t  nw_reg_hndl,
  mmclient_nw_reg_properties_t *curr_operator
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_scan_operators
===========================================================================*/
/*!
@brief
  Function to scan for all the available operators in the system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg cb_fn - callback function where scanned operators will be passed back
@arg *user_data - data to pass back to user

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_scan_operators
(
  mmclient_nw_reg_hndl_t        nw_reg_hndl,
  mmclient_nw_reg_scan_cb_fn_t  cb_fn,
  void*                         user_data
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_radio_settings
===========================================================================*/
/*!
@brief
  Function to get radio settings

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *radio_settings - current radio settings

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_radio_settings
(
  mmclient_nw_reg_hndl_t           nw_reg_hndl,
  mmclient_nw_reg_radio_settings_t *radio_settings
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_set_radio_settings
===========================================================================*/
/*!
@brief
  Function to get radio settings

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *radio_settings - current radio settings

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_set_radio_settings
(
  mmclient_nw_reg_hndl_t            nw_reg_hndl,
  mmclient_nw_reg_radio_settings_t  *radio_settings
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_network_time
===========================================================================*/
/*!
@brief
  Function to get the network time

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *network_time - current network time

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_network_time
(
  mmclient_nw_reg_hndl_t nw_reg_hndl,
  mmclient_nw_reg_time_t *network_time
);

/* Attached APIs */
typedef enum
{
  MMCLIENT_ATTACH,
  MMCLIENT_DETACH
} mmclient_attach_t;

/*===========================================================================
  FUNCTION  mmclient_attach_get_status
===========================================================================*/
/*!
@brief
  Function to get the current network attach status

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - Network registration handle
@arg *attach_info - attach information out param

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_attach_get_status
(
  mmclient_nad_hndl_t  nad_hndl,
  mmclient_attach_t    *attach_info
);

/*===========================================================================
  FUNCTION  mmclient_attach_set_attach
===========================================================================*/
/*!
@brief
  Function to initiate attach

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - Network registration handle
@arg attach_info - attach information

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_attach_set_attach
(
  mmclient_nad_hndl_t  nad_hndl,
  mmclient_attach_t    attach_info
);

/*============================================================================
                        SIM MANAGEMENT DEFS
============================================================================*/
typedef mmclient_hndl_t mmclient_sim_hndl_t;

#define MMCLIENT_UIM_PIN_MAX_LEN   (8)

typedef enum
{
  UIM_PIN_ID_1,
  UIM_PIN_ID_2,
  UIM_PIN_ID_UNIVERSAL,
  UIM_PIN_ID_HIDDEN_KEY,
  /* Below are currently not supported */
  UIM_PIN_ID_PERSO_PS, /* PH-SIM - lock phone to current SIM */
  UIM_PIN_ID_PERSO_PF, /* Lock Phone to first SIM inserted */
  UIM_PIN_ID_PERSO_PN, /* Network Personalization */
  UIM_PIN_ID_PERSO_PU, /* Network Subset Personalization */
  UIM_PIN_ID_PERSO_PP, /* Service Provider Personalization */
  UIM_PIN_ID_PERSO_PC  /* Corporate Personalization */
} mmclient_sim_pin_type_t;

typedef enum
{
  UIM_PIN_OPERATION_DISABLE,
  UIM_PIN_OPERATION_ENABLE
} mmclient_sim_pin_lock_operation_t;

typedef enum
{
  MMCLIENT_SIM_ABSENT,
  MMCLIENT_SIM_PRESENT,
  MMCLIENT_SIM_LOCKED_OUT,
  MMCLIENT_SIM_READY,
  MMCLIENT_SIM_RESETTING,
  MMCLIENT_SIM_UNKNOWN /* Not one of the above states */
} mmclient_sim_availability_t;

typedef struct
{
  mmclient_sim_pin_type_t           type;
  mmclient_sim_pin_lock_operation_t pin_operation;
  uint8_t                           pin_len;
  char                              pin[MMCLIENT_UIM_PIN_MAX_LEN+1];
} mmclient_sim_lock_pin_info_t;

typedef struct
{
  mmclient_sim_pin_type_t type;
  uint8_t                 old_pin_len;
  char                    old_pin[MMCLIENT_UIM_PIN_MAX_LEN+1];
  uint8_t                 new_pin_len;
  char                    new_pin[MMCLIENT_UIM_PIN_MAX_LEN+1];
} mmclient_sim_change_pin_info_t;

typedef struct
{
  mmclient_sim_pin_type_t type;
  uint8_t                 puk_len;
  char                    puk[MMCLIENT_UIM_PIN_MAX_LEN+1];
  uint8_t                 new_pin_len;
  char                    new_pin[MMCLIENT_UIM_PIN_MAX_LEN+1];
} mmclient_sim_reset_pin_info_t;

typedef struct
{
  mmclient_sim_pin_type_t type;
  uint8_t                 pin_len;
  char                    pin[MMCLIENT_UIM_PIN_MAX_LEN+1];
} mmclient_sim_enter_pin_info_t;


typedef uint32_t mmclient_sim_pin_req_mask_t;

#define MMCLIENT_SIM_PIN1_REQ_MASK       0x1
#define MMCLIENT_SIM_PUK1_REQ_MASK       0x2
#define MMCLIENT_SIM_UNIV_PIN_REQ_MASK   0x4 /* Universal Pin is required */
#define MMCLIENT_SIM_UNKNOWN_REQ_MASK    0x8

typedef uint32_t mmclient_sim_locked_pins_mask_t;

#define MMCLIENT_SIM_PIN1_LOCKED_MASK       0x1
#define MMCLIENT_SIM_PIN2_LOCKED_MASK       0x2
#define MMCLIENT_SIM_UNIV_PIN_LOCKED_MASK   0x4 /* Universal Pin is locked */
#define MMCLIENT_SIM_HIDDEN_KEY_LOCKED_MASK 0x8

typedef uint32_t mmclient_sim_properties_validity_mask_t;

#define MMCLIENT_SIM_CARD_PRESENT_MASK   0x1
#define MMCLIENT_SIM_IMSI_MASK           0x2
#define MMCLIENT_SIM_MCC_MASK            0x4
#define MMCLIENT_SIM_MNC_MASK            0x8
#define MMCLIENT_SUB_NUMBERS_MASK        0x10 /* Set if refresh occured */
#define MMCLIENT_PREF_LANG_MASK          0x20
#define MMCLIENT_SERVICE_PROVIDER_NAME_MASK 0x40
#define MMCLIENT_SERVICE_NUMS_MASK          0x80 /* Set if refresh occured */
#define MMCLIENT_PIN_REQUIRED_MASK          0x100
#define MMCLIENT_LOCKED_PINS_MASK           0x200
#define MMCLIENT_CARD_IDENTIFIER_MASK       0x400 /* ICCID */
#define MMCLIENT_FIXED_DAILING_MASK         0x800
#define MMCLIENT_BARRED_DAILING_MASK        0x1000
#define MMCLIENT_SIM_RETRIES_MASK           0x2000
#define MMCLIENT_SIM_APP_TYPE_MASK          0x4000
#define MMCLIENT_SIM_AID_LEN_MASK           0x8000

#define MMCLIENT_SIM_SPN_LEN              (15)
#define MMCLIENT_SIM_MAX_PREF_LANG_LEN    (30)
#define MMCLIENT_SIM_MAX_PINS             (16)
#define MMCLIENT_SIM_ICCID_LEN            (20)
#define MMCLIENT_SIM_TRANSPARENT_MAX_LEN  (4096)
#define MMCLIENT_SIM_RECORD_MAX_LEN       (255)
#define MMCLIENT_SIM_PATH_MAX             (10)

typedef struct
{
  uint8_t upin_retries;
  uint8_t upuk_retries;
  uint8_t pin1_retries;
  uint8_t puk1_retries;
  uint8_t pin2_retries;
  uint8_t puk2_retries;
} mmclient_sim_retry_pin_info_t;

typedef enum
{
  MMCLIENT_SIM_APP_TYPE_UNKNOWN = 0x00, /* Unknown */
  MMCLIENT_SIM_APP_TYPE_SIM     = 0x01, /* SIM card (2G) */
  MMCLIENT_SIM_APP_TYPE_USIM    = 0x02, /* USIM application (3G) */
  MMCLIENT_SIM_APP_TYPE_RUIM    = 0x03, /* RUIM card */
  MMCLIENT_SIM_APP_TYPE_CSIM    = 0x04, /* CSIM application */
  MMCLIENT_SIM_APP_TYPE_ISIM    = 0x05, /* ISIM application */
} mmclient_sim_app_type_t;

typedef struct
{
  mmclient_sim_availability_t              availability;
  char                                     imsi[MMCLIENT_IMSI_LEN+1];
  char                                     mcc[MMCLIENT_MCC_LEN+1];
  char                                     mnc[MMCLIENT_MNC_LEN+1];
  char                                     iccid[MMCLIENT_SIM_ICCID_LEN+1];
  char                                     spn[MMCLIENT_SIM_SPN_LEN+1];
  mmclient_sim_pin_req_mask_t              pin_req;
  mmclient_sim_locked_pins_mask_t          locked_pins;
  uint8_t                                  pref_language[MMCLIENT_SIM_MAX_PREF_LANG_LEN];
  uint8_t                                  pref_lang_len;
  uint8_t                                  fixed_dialing_enabled;
  uint8_t                                  barred_dialing_enabled;
  mmclient_sim_retry_pin_info_t            pin_retries;
  mmclient_sim_app_type_t                  app_type;
  uint32_t                                 aid_len; /* App ID Length - to determine 2g or 3g card*/
  mmclient_sim_properties_validity_mask_t  validity_mask;
} mmclient_sim_properties_t;

typedef struct
{
  uint16_t file_id;
  uint32_t path_len;
  uint8_t  path[MMCLIENT_SIM_PATH_MAX];
  /* Complete path of the file, which
     is a sequence block of 2 bytes (e.g., 0x3F00 0x7FFF). */
} mmclient_sim_file_info_t;

typedef struct
{
  uint8_t  content_valid;
  uint8_t  content[MMCLIENT_SIM_RECORD_MAX_LEN];
  uint16_t content_len;
} mmclient_sim_record_content_t;

typedef struct
{
  uint8_t  content_valid;
  uint8_t  content[MMCLIENT_SIM_TRANSPARENT_MAX_LEN];
  uint16_t content_len;
} mmclient_sim_transparent_content_t;

typedef enum
{
  MMCLIENT_SIM_FILE_TYPE_TRANSPARENT,
  MMCLIENT_SIM_FILE_TYPE_CYCLIC,
  MMCLIENT_SIM_FILE_TYPE_LINEAR_FIXED,
  MMCLIENT_SIM_FILE_TYPE_DEDICATED_FILE,
  MMCLIENT_SIM_FILE_TYPE_MASTER_FILE
} mmclient_sim_file_type_t;

typedef struct
{
  uint8_t                  file_attributes_valid; /* True if below are valid */
  uint16_t                 file_size;
  uint16_t                 file_id;
  mmclient_sim_file_type_t file_type;
  uint16_t                 rec_size; /* linear-fixed and cyclic files only */
  uint16_t                 rec_count; /* liner-fixed files only */
} mmclient_sim_file_attributes_t;

/* Deprecated - use mmclient_sim_register_ind and pass mmclient_sim_ind_cb_fn_t
                to register for sim indications */
typedef void (*mmclient_sim_cb_func_t)
(
  mmclient_sim_availability_t sim_avail,
  void* user_data
);

typedef void (*mmclient_sim_ind_cb_fn_t)
(
  mmclient_sim_properties_t *sim_properties,
  void* user_data
);

/*===========================================================================
  FUNCTION  mmclient_sim_get_srvc_hndl
===========================================================================*/
/*!
@brief
  Function to get handle for SIM service

@return
  mmclient_sim_hndl_t reference

@arg nad_hndl - NAD handle
@arg cb - callback function to receive indication about SIM status
@arg *user_data - any information the caller wants to pass through

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_sim_hndl_t mmclient_sim_get_srvc_hndl
(
  mmclient_nad_hndl_t      nad_hndl,
  mmclient_sim_cb_func_t   cb, /* Deprecated - use mmclient_sim_register_ind */
  void                     *user_data
);

/*===========================================================================
  FUNCTION  mmclient_sim_release_srvc_hndl
===========================================================================*/
/*!
@brief
  Function to release SIM service handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_release_srvc_hndl
(
   mmclient_sim_hndl_t sim_hndl
);

/*===========================================================================
  FUNCTION  mmclient_sim_enter_pin
===========================================================================*/
/*!
@brief
  Function to enter SIM pin

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *enter_pin_info - Information about SIM pin

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_enter_pin
(
  mmclient_sim_hndl_t           sim_hndl,
  mmclient_sim_enter_pin_info_t *enter_pin_info
);

/*===========================================================================
  FUNCTION  mmclient_sim_change_pin
===========================================================================*/
/*!
@brief
  Function to change the PIN

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *reset_pin_info - New PIN information

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_change_pin
(
  mmclient_sim_hndl_t            sim_hndl,
  mmclient_sim_change_pin_info_t *change_pin_info
);

/*===========================================================================
  FUNCTION  mmclient_sim_reset_pin
===========================================================================*/
/*!
@brief
  Function to reset SIM pin

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *reset_pin_info - New PIN information

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_reset_pin
(
  mmclient_sim_hndl_t           sim_hndl,
  mmclient_sim_reset_pin_info_t *reset_pin_info
);

/*===========================================================================
  FUNCTION  mmclient_sim_set_pin_protection
===========================================================================*/
/*!
@brief
  Function to set PIN protection

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *lock_pin_info - PIN information

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_set_pin_protection
(
  mmclient_sim_hndl_t          sim_hndl,
  mmclient_sim_lock_pin_info_t *lock_pin_info
);

/*===========================================================================
  FUNCTION  mmclient_sim_get_properties
===========================================================================*/
/*!
@brief
  Function to get SIM properties

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *sim_properties - SIM properties

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_get_properties
(
  mmclient_sim_hndl_t sim_hndl,
  mmclient_sim_properties_t *sim_properties
);

/*===========================================================================
  FUNCTION  mmclient_sim_register_ind
===========================================================================*/
/*!
@brief
  Function to register for SIM related indications

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg cb_fn - callback function to listen for SIM related indications
@arg validity_mask - bitmask to identify the indications caller is
                     interested in
@arg user_data - Any information user wants to pass

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_register_ind
(
  mmclient_sim_hndl_t                     sim_hndl,
  mmclient_sim_ind_cb_fn_t                cb_fn,
  mmclient_sim_properties_validity_mask_t validity_mask,
  void*                                   user_data
);

/*===========================================================================
  FUNCTION  mmclient_sim_read_record
===========================================================================*/
/*!
@brief
  Function to read a linear fixed/cyclic file from the SIM file system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
     file_info_ptr - pointer to struct with the file_id, path, and path_len
     record - record number in file - starting from 1
     length - length of content to be read - 0 means read complete record
@arg *sim_record_content - output param containing content of record and len

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_read_record
(
  mmclient_sim_hndl_t           sim_hndl,
  mmclient_sim_file_info_t      *file_info_ptr,
  uint16_t                      record,
  uint16_t                      length,
  mmclient_sim_record_content_t *sim_record_content
);


/*===========================================================================
  FUNCTION  mmclient_sim_read_transparent
===========================================================================*/
/*!
@brief
  Function to read a transparent file from the SIM file system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
     file_info_ptr - pointer to struct with the file_id, path, and path_len
     offset - offset of the read operation
     length - length of content to be read - 0 means read complete record
@arg *sim_transparent_content - output param containing content and len

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_read_transparent
(
  mmclient_sim_hndl_t                sim_hndl,
  mmclient_sim_file_info_t           *file_info_ptr,
  uint16_t                           offset,
  uint16_t                           length,
  mmclient_sim_transparent_content_t *sim_transparent_content
);

/*===========================================================================
  FUNCTION  mmclient_sim_get_file_attributes
===========================================================================*/
/*!
@brief
  Function to get the file attributes from the SIM file system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
     file_info_ptr - pointer to struct with the file_id, path, and path_len
@arg *sim_file_attributes - output param containing file attributes

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_get_file_attributes
(
  mmclient_sim_hndl_t                sim_hndl,
  mmclient_sim_file_info_t           *file_info_ptr,
  mmclient_sim_file_attributes_t     *sim_file_attributes
);

/*============================================================================
                        DATA SYSTEM STATUS DEFS
============================================================================*/
typedef enum
{
  MMCLIENT_DATA_SYS_TECH_3GPP   = 0,
  MMCLIENT_DATA_SYS_TECH_3GPP2  = 1,
  MMCLIENT_DATA_SYS_TECH_WLAN   = 2
} mmclient_data_sys_tech_t;

typedef enum
{
  MMCLIENT_DATA_SYS_RAT_NULL_BEARER  = 0x0,
  MMCLIENT_DATA_SYS_RAT_3GPP_WCDMA   = 0x1,
  MMCLIENT_DATA_SYS_RAT_3GPP_GERAN   = 0x2,
  MMCLIENT_DATA_SYS_RAT_3GPP_LTE     = 0x3,
  MMCLIENT_DATA_SYS_RAT_3GPP_TDSCDMA = 0x4,
  MMCLIENT_DATA_SYS_RAT_3GPP_WLAN    = 0x5,
  MMCLIENT_DATA_SYS_RAT_3GPP2_1X     = 0x65,
  MMCLIENT_DATA_SYS_RAT_3GPP2_HRPD   = 0x66,
  MMCLIENT_DATA_SYS_RAT_3GPP2_EHRPD  = 0x67,
  MMCLIENT_DATA_SYS_RAT_3GPP2_WLAN   = 0x68,
  MMCLIENT_DATA_SYS_RAT_WLAN         = 0xC9
} mmclient_data_sys_rat_value_t;

/* SO mask unspecified */
#define MMCLIENT_DATA_SYS_SO_MASK_UNSPEC               (0x0000000000000000ull)

/* 3GPP SO masks */
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_WCDMA           (0x0000000000000001ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_HSDPA           (0x0000000000000002ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_HSUPA           (0x0000000000000004ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_HSDPAPLUS       (0x0000000000000008ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_DC_HSDPAPLUS    (0x0000000000000010ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_64QAM           (0x0000000000000020ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_HSPA            (0x0000000000000040ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_GPRS            (0x0000000000000080ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_EDGE            (0x0000000000000100ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_GSM             (0x0000000000000200ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_S2B             (0x0000000000000400ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_LTD_SRVC    (0x0000000000000800ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_FDD         (0x0000000000001000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_TDD         (0x0000000000002000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_TDSCDMA     (0x0000000000004000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_DC_HSUPA    (0x0000000000008000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_CA_DL       (0x0000000000010000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP_LTE_CA_UL       (0x0000000000020000ull)

/* 3GPP2 SO masks */
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_1X_IS95        (0x0000000001000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_1X_IS2000      (0x0000000002000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_1X_IS2000_RELA (0x0000000004000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REV0_DPA   (0x0000000008000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVA_DPA   (0x0000000010000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVB_DPA   (0x0000000020000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVA_MPA   (0x0000000040000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVB_MPA   (0x0000000080000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVA_EMPA  (0x0000000100000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVB_EMPA  (0x0000000200000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_REVB_MMPA  (0x0000000400000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_HDR_EVDO_FMC   (0x0000000800000000ull)
#define MMCLIENT_DATA_SYS_SO_MASK_3GPP2_1X_CS          (0x0000001000000000ull)

typedef struct
{
  uint8_t                       tech_valid;
  mmclient_data_sys_tech_t      tech;
  uint8_t                       rat_valid;
  mmclient_data_sys_rat_value_t rat;
  uint8_t                       so_mask_valid;
  uint64_t                      so_mask;
} mmclient_data_sys_status_ind_t;

typedef struct
{
  mmclient_nad_inst_t            nad_inst;
  mmclient_data_sys_status_ind_t data_sys_status;
} mmclient_data_sys_status_ind_info_t;

typedef void (*mmclient_data_sys_ind_cb_func_t)
                  (mmclient_data_sys_status_ind_info_t*, void*);

typedef struct
{
  mmclient_data_sys_ind_cb_func_t cb_func;
  void                            *user_data;
} mmclient_data_sys_cb_info_t;

typedef struct
{
  uint8_t suppress_so_mask_change;
  uint8_t deregister;
} mmclient_data_sys_reg_options_t;

/*===========================================================================
  FUNCTION  mmclient_query_data_sys_status
===========================================================================*/
/*!
@brief
  Function to get current Data System Status

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg data_sys_info - Data System Status info

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_query_data_sys_status
(
  mmclient_nad_hndl_t                 nad_hndl,
  mmclient_data_sys_status_ind_info_t *data_sys_info
);

/*===========================================================================
  FUNCTION  mmclient_register_data_sys_status_ind
===========================================================================*/
/*!
@brief
  Function to register Data System Status indications

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl    - NAD handle
@arg cb_info     - Data System Status callback info
@arg reg_options - Registratio options

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_register_data_sys_status_ind
(
  mmclient_nad_hndl_t              nad_hndl,
  mmclient_data_sys_cb_info_t     *cb_info,
  mmclient_data_sys_reg_options_t *reg_options
);

/*=================================================================================
  FUNCTION: mmclient_init
=================================================================================*/
/*!
@brief
  Initialize client library
*/
/*===============================================================================*/
mmclient_status_t mmclient_init(mmclient_sys_cb_info_t *sys_cb_info);

/*=================================================================================
  FUNCTION: mmclient_process_sys_indications
=================================================================================*/
/*!
@brief
  Process sys indications
*/
/*===============================================================================*/
void mmclient_process_sys_indications(mmclient_sys_ind_info_t *sys_ind_info);

/*=================================================================================
  FUNCTION: mmclient_cleanup_state
=================================================================================*/
/*!
@brief
  De-initialize state related to modem manager server
*/
/*===============================================================================*/
void mmclient_cleanup_state(void);

/*=================================================================================
  FUNCTION: mmclient_deinit
=================================================================================*/
/*!
@brief
  Cleanup the library resources
*/
/*===============================================================================*/
void mmclient_deinit(void);

#endif /* _MMCLIENT_H_ */
#endif /* FEATURE_DATAOSS_TARGET_MCTM */
