/******************************************************************************

                     M M C L I E N T _ M S G . H

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
#ifndef _MMCLIENT_MSG_H_
#define _MMCLIENT_MSG_H_

/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <stdint.h>
#include <dbus/dbus.h>

#include "mmclient.h"

/*============================================================================
                         GLOBAL DECLARATIONS
============================================================================*/
#define MMCLIENT_INVALID_NAD_HNDL    "nad_invalid"
#define MMCLIENT_INVALID_IP_ADDR     "ipaddr_invalid"
#define MMCLIENT_INVALID_PROFILE     ((unsigned int)0xFFFFFFFF)

typedef enum
{
  MSG_ID_INVALID = 0x0,

  /* NAD messages */
  MSG_ID_NAD_GET_NUM_NAD_INSTANCES_REQ = 0x00000001,
  MSG_ID_NAD_GET_NUM_NAD_INSTANCES_RESP,
  MSG_ID_NAD_GET_NAD_HNDL_REQ,
  MSG_ID_NAD_GET_NAD_HNDL_RESP,
  MSG_ID_NAD_GET_NAD_STATUS_REQ,
  MSG_ID_NAD_GET_NAD_STATUS_RESP,
  MSG_ID_NAD_SET_NAD_STATUS_REQ,
  MSG_ID_NAD_SET_NAD_STATUS_RESP,
  MSG_ID_NAD_GET_NAD_DEVICE_INFO_REQ,
  MSG_ID_NAD_GET_NAD_DEVICE_INFO_RESP,
  MSG_ID_NAD_RELEASE_NAD_HNDL_REQ,
  MSG_ID_NAD_RELEASE_NAD_HNDL_RESP,
  MSG_ID_NAD_STATUS_CHANGE_IND,

  /* Data call messages */
  MSG_ID_DATA_GET_SRVC_HNDL_REQ,
  MSG_ID_DATA_GET_SRVC_HNDL_RESP,
  MSG_ID_DATA_START_CALL_REQ,
  MSG_ID_DATA_START_CALL_RESP,
  MSG_ID_DATA_STOP_CALL_REQ,
  MSG_ID_DATA_STOP_CALL_RESP,
  MSG_ID_DATA_GET_CALL_INFO_REQ,
  MSG_ID_DATA_GET_CALL_INFO_RESP,
  MSG_ID_DATA_RELEASE_SRVC_HNDL_REQ,
  MSG_ID_DATA_RELEASE_SRVC_HNDL_RESP,
  MSG_ID_DATA_GET_CALL_END_REASON_REQ,
  MSG_ID_DATA_GET_CALL_END_REASON_RESP,
  MSG_ID_DATA_GET_CURR_BEARER_TECH_REQ,
  MSG_ID_DATA_GET_CURR_BEARER_TECH_RESP,
  MSG_ID_DATA_CALL_INFO_IND,

  /* Profile management messages*/
  MSG_ID_PROFILE_QUERY_REQ,
  MSG_ID_PROFILE_QUERY_RESP,
  MSG_ID_PROFILE_CREATE_REQ,
  MSG_ID_PROFILE_CREATE_RESP,
  MSG_ID_PROFILE_DELETE_REQ,
  MSG_ID_PROFILE_DELETE_RESP,
  MSG_ID_PROFILE_MODIFY_REQ,
  MSG_ID_PROFILE_MODIFY_RESP,
  MSG_ID_PROFILE_SET_INITIAL_ATTACH_REQ,
  MSG_ID_PROFILE_SET_INITIAL_ATTACH_RESP,

  /* Network Registration messages */
  MSG_ID_NW_REG_GET_SRVC_HNDL_REQ,
  MSG_ID_NW_REG_GET_SRVC_HNDL_RESP,
  MSG_ID_NW_REG_RELEASE_SRVC_HNDL_REQ,
  MSG_ID_NW_REG_RELEASE_SRVC_HNDL_RESP,
  MSG_ID_NW_REG_GET_PROPERTIES_REQ,
  MSG_ID_NW_REG_GET_PROPERTIES_RESP,
  MSG_ID_NW_REG_REGISTER_NETWORK_REQ,
  MSG_ID_NW_REG_REGISTER_NETWORK_RESP,
  MSG_ID_NW_REG_GET_OPERATOR_REQ,
  MSG_ID_NW_REG_GET_OPERATOR_RESP,
  MSG_ID_NW_REG_SCAN_OPERATORS_REQ,
  MSG_ID_NW_REG_SCAN_OPERATORS_RESP,
  MSG_ID_NW_REG_SCAN_OPERATORS_IND,
  MSG_ID_NW_REG_GET_RADIO_SETTINGS_REQ,
  MSG_ID_NW_REG_GET_RADIO_SETTINGS_RESP,
  MSG_ID_NW_REG_SET_RADIO_SETTINGS_REQ,
  MSG_ID_NW_REG_SET_RADIO_SETTINGS_RESP,
  MSG_ID_NW_REG_REGISTER_IND_REQ,
  MSG_ID_NW_REG_REGISTER_IND_RESP,
  MSG_ID_NW_REG_GET_NETWORK_TIME_REQ,
  MSG_ID_NW_REG_GET_NETWORK_TIME_RESP,
  MSG_ID_NW_REG_PROPERTIES_IND,

  /* SIM Management messages */
  MSG_ID_SIM_GET_SRVC_HNDL_REQ,
  MSG_ID_SIM_GET_SRVC_HNDL_RESP,
  MSG_ID_SIM_RELEASE_SRVC_HNDL_REQ,
  MSG_ID_SIM_RELEASE_SRVC_HNDL_RESP,
  MSG_ID_SIM_ENTER_PIN_REQ,
  MSG_ID_SIM_ENTER_PIN_RESP,
  MSG_ID_SIM_CHANGE_PIN_REQ,
  MSG_ID_SIM_CHANGE_PIN_RESP,
  MSG_ID_SIM_RESET_PIN_REQ,
  MSG_ID_SIM_RESET_PIN_RESP,
  MSG_ID_SIM_SET_PIN_PROTECTION_REQ,
  MSG_ID_SIM_SET_PIN_PROTECTION_RESP,
  MSG_ID_SIM_GET_PROPERTIES_REQ,
  MSG_ID_SIM_GET_PROPERTIES_RESP,
  MSG_ID_SIM_REGISTER_IND_REQ,
  MSG_ID_SIM_REGISTER_IND_RESP,
  MSG_ID_SIM_READ_RECORD_REQ,
  MSG_ID_SIM_READ_RECORD_RESP,
  MSG_ID_SIM_READ_TRANSPARENT_REQ,
  MSG_ID_SIM_READ_TRANSPARENT_RESP,
  MSG_ID_SIM_GET_FILE_ATTRIBUTES_REQ,
  MSG_ID_SIM_GET_FILE_ATTRIBUTES_RESP,
  MSG_ID_SIM_PROPERTIES_IND,
  MSG_ID_SIM_AVAILABILITY_IND,

  /* Attach messages */
  MSG_ID_ATTACH_GET_STATUS_REQ,
  MSG_ID_ATTACH_GET_STATUS_RESP,
  MSG_ID_ATTACH_SET_ATTACH_REQ,
  MSG_ID_ATTACH_SET_ATTACH_RESP,

  /*Bandwitdth APIS*/
  MSG_ID_BANDWIDTH_IND_REG_REQ,
  MSG_ID_BANDWIDTH_IND_REG_RESP,
  MSG_ID_BANDWIDTH_THROUGHPUT_IND,
  MSG_ID_BANDWIDTH_QUERY_REQ,
  MSG_ID_BANDWIDTH_QUERY_RESP,

  /* Data System Status */
  MSG_ID_DATA_SYSTEM_IND_REG_REQ,
  MSG_ID_DATA_SYSTEM_IND_REG_RESP,
  MSG_ID_DATA_SYSTEM_IND,
  MSG_ID_DATA_SYSTEM_QUERY_REQ,
  MSG_ID_DATA_SYSTEM_QUERY_RESP,

  MSG_ID_MDMMGR_CLIENT_DISCONNECT,

  /* Sys indications */
  MSG_ID_SYS_SERVER_DOWN_IND,
  MSG_ID_SYS_SERVER_READY_IND,
  MSG_ID_SYS_MODEM_OUT_OF_SERVICE_IND,
  MSG_ID_SYS_MODEM_IN_SERVICE_IND,

  MSG_ID_MAX_FORCE_32_BIT = 0x7FFFFFFF
} mmclient_msg_id_t;

#define METHOD_GET_NUM_NAD_INSTANCES            "mmclient_get_num_nad_instances"
#define METHOD_GET_NAD_HANDLE                   "mmclient_get_nad_handle"
#define METHOD_GET_NAD_STATUS                   "mmclient_get_nad_status"
#define METHOD_SET_NAD_STATUS                   "mmclient_set_nad_status"
#define METHOD_GET_NAD_DEVICE_INFO              "mmclient_get_nad_device_info"
#define METHOD_RELEASE_NAD_HANDLE               "mmclient_release_nad_handle"
#define SIGNAL_NAD_STATUS_CHANGE_IND            "mmclient_nad_status_change_ind"

#define METHOD_DATA_GET_SRVC_HNDL               "mmclient_data_get_srvc_hndl"
#define METHOD_DATA_START_CALL                  "mmclient_data_start_call"
#define METHOD_DATA_STOP_CALL                   "mmclient_data_stop_call"
#define METHOD_DATA_GET_CALL_INFO               "mmclient_data_get_call_info"
#define METHOD_DATA_RELEASE_SRVC_HNDL           "mmclient_data_release_srvc_hndl"
#define METHOD_DATA_GET_CALL_END_REASON         "mmclient_data_get_call_end_reason"
#define METHOD_DATA_GET_CURR_BEARER_TECH        "mmclient_data_get_current_data_bearer_tech"
#define SIGNAL_DATA_CALL_INFO_IND               "mmclient_data_call_info_ind"

#define METHOD_PROFILE_QUERY                    "mmclient_profile_query"
#define METHOD_PROFILE_CREATE                   "mmclient_profile_create"
#define METHOD_PROFILE_DELETE                   "mmclient_profile_delete"
#define METHOD_PROFILE_MODIFY                   "mmclient_profile_modify"

#define METHOD_NW_REG_GET_SRVC_HNDL             "mmclient_nw_reg_get_srvc_hndl"
#define METHOD_NW_REG_RELEASE_SRVC_HNDL         "mmclient_nw_reg_release_srvc_hndl"
#define METHOD_NW_REG_PROPERTIES_REGISTER_IND   "mmclient_nw_reg_properties_register_ind"
#define METHOD_NW_REG_GET_PROPERTIES            "mmclient_nw_reg_get_properties"
#define METHOD_NW_REG_REGISTER_NETWORK          "mmclient_nw_reg_register_network"
#define METHOD_NW_REG_GET_OPERATOR              "mmclient_nw_reg_get_operator"
#define METHOD_NW_REG_SCAN_OPERATORS            "mmclient_nw_reg_scan_operators"
#define METHOD_NW_REG_GET_RADIO_SETTINGS        "mmclient_nw_reg_get_radio_settings"
#define METHOD_NW_REG_SET_RADIO_SETTINGS        "mmclient_nw_reg_set_radio_settings"
#define SIGNAL_NW_REG_PROPERTIES_IND            "mmclient_nw_reg_properties_ind"
#define METHOD_NW_REG_GET_NETWORK_TIME          "mmclient_nw_reg_get_network_time"
#define SIGNAL_NW_REG_SCAN_OPERATORS_IND        "mmclient_nw_reg_scan_operators_ind"

#define METHOD_SIM_GET_SRVC_HNDL                "mmclient_sim_get_srvc_hndl"
#define METHOD_SIM_RELEASE_SRVC_HNDL            "mmclient_sim_release_srvc_hndl"
#define METHOD_SIM_ENTER_PIN                    "mmclient_sim_enter_pin"
#define METHOD_SIM_CHANGE_PIN                   "mmclient_sim_change_pin"
#define METHOD_SIM_RESET_PIN                    "mmclient_sim_reset_pin"
#define METHOD_SIM_SET_PIN_PROTECTION           "mmclient_sim_set_pin_protection"
#define METHOD_SIM_GET_PROPERTIES               "mmclient_sim_get_properties"
#define METHOD_SIM_REGISTER_IND                 "mmclient_sim_register_ind"
#define METHOD_SIM_READ_RECORD                  "mmclient_sim_read_record"
#define METHOD_SIM_READ_TRANSPARENT             "mmclient_sim_read_transparent"
#define METHOD_SIM_GET_FILE_ATTRIBUTES          "mmclient_sim_get_file_attributes"
#define SIGNAL_SIM_AVAILABILITY_IND             "mmclient_sim_availability_ind"
#define SIGNAL_SIM_PROPERTIES_IND               "mmclient_sim_properties_ind"

#define METHOD_ATTACH_GET_STATUS                "mmclient_attach_get_status"
#define METHOD_ATTACH_SET_ATTACH                "mmclient_attach_set_attach"

#define METHOD_BW_IND_REG                       "mmclient_bandwidth_register"
#define METHOD_BW_QUERY                         "mmclient_bandwidth_query"
#define SIGNAL_BW_IND                           "mmclient_bandwidth_ind"

#define METHOD_DATA_SYSTEM_IND_REG              "mmclient_data_system_ind_register"
#define METHOD_DATA_SYSTEM_QUERY                "mmclient_data_system_query"
#define SIGNAL_DATA_SYSTEM_IND                  "mmclient_data_system_ind"

#define METHOD_MDMMGR_CLIENT_DISCONNECT         "mmclient_dbus_client_disconnect"

#define SIGNAL_SYS_SERVER_DOWN_IND              "mmclient_sys_ind_server_down"
#define SIGNAL_SYS_SERVER_READY_IND             "mmclient_sys_ind_server_ready"
#define SIGNAL_SYS_MODEM_OUT_OF_SERVICE_IND     "mmclient_sys_ind_modem_oos"
#define SIGNAL_SYS_MODEM_IN_SERVICE_IND         "mmclient_sys_ind_modem_is"

#define MMCLIENT_INVALID_CLIENT_ID    (UINT16_MAX - 1)
#define MMCLIENT_INVALID_TXN_ID       (UINT32_MAX - 1)
#define MMCLIENT_INVALID_PAYLOAD_LEN  (0)
#define MMCLIENT_MAX_PAYLOAD_LEN      (UINT16_MAX - 1)

#define MMCLIENT_DBUS_DEFAULT_TIMEOUT 50000

typedef enum
{
  /* API succeeded */
  MMCLIENT_MSG_NO_ERR = 0,

  /* Could not allocate memory for internal operations */
  MMCLIENT_MSG_ERR_LOW_MEMORY = 1,

  /* Invalid parameters were passed */
  MMCLIENT_MSG_ERR_INVALID_PARAMS = 2,

  /* Failed to encode message */
  MMCLIENT_MSG_ERR_ENCODE_FAIL = 3,

  /* Failed to decode message */
  MMCLIENT_MSG_ERR_DECODE_FAIL = 4,

  /* Connection to IPC layer failed */
  MMCLIENT_MSG_ERR_CONNECTION_FAILURE = 5,

  /* Failed to dispatch message */
  MMCLIENT_MSG_ERR_MSG_DISPATCH = 6
} mmclient_msg_err_codes_t;

typedef enum
{
  SRVC_ID_INVALID = -1,
  SRVC_ID_NAD,
  SRVC_ID_DATA_CALL,
  SRVC_ID_PROFILE,
  SRVC_ID_NW_REG,
  SRVC_ID_SIM,
  SRVC_ID_DEVICE,
  SRVC_ID_DATA_SYSTEM,
  SRVC_ID_MDMMGR,
  SRVC_ID_BW_REG,
  SRVC_ID_MAX
} mmclient_srvc_id_t;

typedef enum
{
  TYPE_INVALID = -1,
  TYPE_REQUEST,
  TYPE_RESPONSE,
  TYPE_INDICATION,
  TYPE_MAX
} mmclient_msg_type_t;

typedef uint16_t  mmclient_client_id_t;
typedef uint32_t  mmclient_txn_id_t;
typedef uint16_t  mmclient_payload_len_t;

/* Header to encode in each message exchanged between client and server
   Service ID - Service to connect to on Modem Manager
   Message ID - ID of the message
   Client ID - ID of the client requesting for the message
   Transaction ID - Information to track each message transaction
   NAD inst - The NAD instance on which the request is being made
   Payload Len - Length of the encoded payload */
typedef struct
{
  mmclient_srvc_id_t     srvc_id;
  mmclient_msg_id_t      msg_id;
  mmclient_client_id_t   client_id;
  mmclient_txn_id_t      txn_id;
  mmclient_nad_inst_t    nad_inst;
  mmclient_payload_len_t payload_len;
} mmclient_msg_hdr_t;

/* Callback information */
typedef struct
{
  char                    nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nad_ind_info_t ind_info;
} nad_cb_info_t;

/* Set Nad status params */
typedef struct
{
  char nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nad_status_t nad_status;
} mmclient_nad_status_params_t;

typedef struct
{
  char                      sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_sim_properties_t sim_properties;
} sim_cb_properties_params_t;

typedef struct
{
  char                         nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nw_reg_properties_t nw_reg_properties;
} nw_reg_cb_properties_params_t;

typedef struct
{
  char                                nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nw_reg_scanned_operators_t nw_reg_scanned_operators;
} nw_reg_cb_scan_operators_params_t;

typedef struct
{
  char                        sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_sim_availability_t sim_availability;
} sim_cb_availability_params_t;

/*Data call params send stuct*/
typedef struct
{
  unsigned char            data_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_call_params_t   call_params;
} mmclient_data_call_param_info_t;

/*Data call params cb struct*/
typedef struct
{
  mmclient_data_call_evt_t   evt;
  unsigned char              data_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_call_info_param_info_t;

typedef struct
{
  unsigned char         nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_profile_id_t prof_num;
} mmclient_profile_delete_info_t;

/*Profile params container struct*/
typedef struct
{
  unsigned char              nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_profile_params_t  prof_param;
  mmclient_profile_id_t      prof_num;
} mmclient_prof_param_t;


/* SIM Register Ind Params */
typedef struct
{
  mmclient_sim_properties_validity_mask_t validity_mask;
  char                                    sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_register_ind_t;

typedef struct
{
  mmclient_sim_enter_pin_info_t  enter_pin_info;
  char                           sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_enter_pin_params_t;

typedef struct
{
  mmclient_sim_change_pin_info_t change_pin_info;
  char                           sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_change_pin_params_t;

typedef struct
{
  mmclient_sim_reset_pin_info_t  reset_pin_info;
  char                           sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_reset_pin_params_t;

typedef struct
{
  mmclient_sim_lock_pin_info_t   lock_pin_info;
  char                           sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_pin_protection_params_t;

typedef struct
{
  mmclient_sim_file_info_t      file_info;
  uint16_t                      record;
  uint16_t                      length;
  char                          sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_read_record_params_t;

typedef struct
{
  mmclient_sim_file_info_t      file_info;
  uint16_t                      offset;
  uint16_t                      length;
  char                          sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_read_transparent_params_t;

typedef struct
{
  mmclient_sim_file_info_t      file_info;
  char                          sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_sim_get_file_attributes_params_t;

/* Attach Params */
typedef struct
{
  mmclient_attach_t  attach_info;
  char               nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_attach_info_params_t;

/* NW Reg Register Network Params */
typedef struct
{
  mmclient_nw_reg_register_network_info_t register_info;
  char                                    nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_nw_reg_register_params_t;

/* NW Reg Set Radio Settings Params */
typedef struct
{
  mmclient_nw_reg_radio_settings_t radio_settings;
  char                             nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_nw_reg_radio_settings_params_t;

/* NW Reg Register Ind Params */
typedef struct
{
  mmclient_nw_reg_validity_mask_t validity_mask;
  char                            nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
} mmclient_nw_reg_register_ind_t;

/* Data System Status Params */
typedef struct
{
  char                            nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_data_sys_reg_options_t reg_options;
} mmclient_data_sys_req_param_t;

typedef struct
{
  char nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_data_sys_status_ind_info_t data_sys_info;
} mmclient_data_sys_info_param_t;

/*============================================================================
                         GLOBAL DECLARATIONS
============================================================================*/

/*============================================================================
  FUNCTION: mmclient_get_client_id
============================================================================*/
/*!
@brief
  Returns the client ID received from the server or INVALID if none not set
*/
/*==========================================================================*/
mmclient_client_id_t mmclient_get_client_id(void);

/*============================================================================
  FUNCTION: mmclient_get_msg_hdr
============================================================================*/
/*!
@brief
  Prepares an empty message header
*/
/*==========================================================================*/
mmclient_msg_hdr_t* mmclient_get_msg_hdr(void);

/*============================================================================
  FUNCTION: mmclient_free_msg_hdr
============================================================================*/
/*!
@brief
  Frees the message header
*/
/*==========================================================================*/
void mmclient_free_msg_hdr(mmclient_msg_hdr_t *msg_hdr);

/*============================================================================
  FUNCTION: mmclient_dbus_encode_hdr
============================================================================*/
/*!
@brief
  Encodes the message header into DBus format
*/
/*==========================================================================*/
int mmclient_dbus_encode_hdr
(
  DBusMessageIter     *args,
  mmclient_msg_hdr_t  *msg_hdr
);

/*============================================================================
  FUNCTION: mmclient_dbus_decode_hdr
============================================================================*/
/*!
@brief
  Decodes header information from DBus message
*/
/*==========================================================================*/
mmclient_msg_hdr_t* mmclient_dbus_decode_hdr
(
  DBusMessage     *msg,
  DBusMessageIter *iter
);

/*============================================================================
  FUNCTION: mmclient_dbus_get_msg
============================================================================*/
/*!
@brief
  Helper method to return a DBusMessage object

@arg msg_type - to determine the type of message (request/response/signal)
@arg method_name - the name of the method to be invoked
@arg req_msg - if we need to construct a response message based on the request
               message
*/
/*==========================================================================*/
DBusMessage* mmclient_dbus_get_msg
(
  mmclient_msg_type_t  msg_type,
  mmclient_msg_id_t    msg_id,
  DBusMessage          *req_msg
);

/*============================================================================
  FUNCTION: mmclient_dbus_encode_msg
============================================================================*/
/*!
@brief
  Encodes the message in DBus format

@arg msg_hdr  - Message header
@arg args     - DBusMessageIter for encoding the message
@arg data     - Data to encode
@arg data_len - Length of the payload
*/
/*==========================================================================*/
int mmclient_dbus_encode_msg
(
  mmclient_msg_hdr_t   *msg_hdr,
  DBusMessageIter      *args,
  void                 *data,
  unsigned int         data_len
);

/*=================================================================================
  FUNCTION: mmclient_dbus_decode_msg
=================================================================================*/
/*!
@brief
  Encodes the message in DBus format

@arg msg_hdr  - Message header
@arg args     - DBusMessageIter for decoding the message
@arg data     - Data to decode
@arg data_len - Length of the payload
*/
/*===============================================================================*/
int mmclient_dbus_decode_msg
(
  mmclient_msg_hdr_t   *msg_hdr,
  DBusMessageIter      *args,
  void                 *data,
  unsigned int         data_len
);

/*=================================================================================
  FUNCTION: mmclient_dbus_send_msg
=================================================================================*/
/*!
@brief
  Send via message via DBus

@arg conn     - DBusConnection reference
@arg msg      - DBusMessage reference
@arg hdr      - Message header
@arg data     - Data to be encoded and sent
@arg data_len - Length of the payload
*/
/*===============================================================================*/
int mmclient_dbus_send_msg
(
  DBusConnection       *conn,
  DBusMessage          *msg,
  mmclient_msg_hdr_t   *hdr,
  void                 *data,
  unsigned int         data_len
);

/*=================================================================================
  FUNCTION: mmclient_dbus_send_msg_sync
=================================================================================*/
/*!
@brief
  Send synchronous message via DBus

@arg conn          - DBusConnection reference
@arg msg           - DBusMessage reference
@arg hdr           - Message header
@arg req_data      - Data to be encoded and sent
@arg req_data_len  - Length of the payload
@arg resp_data     - Response data
@arg resp_data_len - Response data length
*/
/*===============================================================================*/
int mmclient_dbus_send_msg_sync
(
  DBusConnection      *conn,
  DBusMessage         *msg,
  mmclient_msg_hdr_t  *hdr,
  void                *req_data,
  unsigned int        req_data_len,
  void                *resp_data,
  unsigned int        resp_data_len
);

/*=================================================================================
  FUNCTION: mmclient_dbus_send_signal
=================================================================================*/
/*!
@brief
  Send signal/indication/unsolicited messages via DBus

@arg conn     - DBusConnection reference
@arg msg      - DBusMessage reference
@arg hdr      - Message header
@arg data     - Data to be encoded and sent
@arg data_len - Length of the payload
*/
/*===============================================================================*/
int mmclient_dbus_send_signal
(
  DBusConnection     *conn,
  DBusMessage        *msg,
  mmclient_msg_hdr_t *hdr,
  void               *data,
  unsigned int       data_len
);

#endif /* _MMCLIENT_MSG_H_ */

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
