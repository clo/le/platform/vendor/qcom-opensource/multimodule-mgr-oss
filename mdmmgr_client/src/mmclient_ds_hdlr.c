/******************************************************************************

             M M C L I E N T _ D S _ H D L R . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "mmclient_dbus.h"
#include "mmclient_util.h"
#include "mmclient_msg.h"

/*============================================================================
                             LOCAL DECLARATIONS
=============================================================================*/

typedef uint8_t boolean;

#define MMCLIENT_MAX_DATA_CLIENT  (25)

typedef struct
{
  boolean                   valid;
  mmclient_data_hndl_t      st_hndl;
  mmclient_data_cb_func_t   mmclient_cb_func;
  void                      *user_data;
  unsigned char             data_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nad_inst_t       nad_inst;
} mmclient_ds_client_info_t;

#define MMCLIENT_DBUS_GET_CONNECTION_OBJ(conn_state, conn)                      \
  conn_state = mmclient_get_connection_state();                                 \
  if (!conn_state)                                                              \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): failed to get connection state!\n", __func__);      \
    goto bail;                                                                  \
  }                                                                             \
                                                                                \
  conn = conn_state->conn;                                                      \
  if (!conn)                                                                    \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): invalid connection object!", __func__);             \
    goto bail;                                                                  \
  }                                                                             \
                                                                                \
  if (conn_state->connection_status != STATE_CONNECTED)                         \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): DBus connection was terminated. "                   \
                     "Please reconnect!\n", __func__);                          \
    goto bail;                                                                  \
  }

static mmclient_ds_client_info_t mm_ds_client_info[MMCLIENT_MAX_DATA_CLIENT];

pthread_mutex_t clnt_ds_list_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_DS_LIST_MUTEX_LOCK()                             \
  MMCLIENT_LOG_LOW("%s(): locking DS list mutex\n", __func__);    \
  pthread_mutex_lock(&clnt_ds_list_mtx);

#define MMCLIENT_DS_LIST_MUTEX_UNLOCK()                           \
  MMCLIENT_LOG_LOW("%s(): unlocking DS list mutex\n", __func__);  \
  pthread_mutex_unlock(&clnt_ds_list_mtx);

pthread_mutex_t mmclient_ds_global_mutex = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_DS_GLOBAL_LOCK()                                  \
  MMCLIENT_LOG_LOW("%s(): locking DS global mutex\n", __func__);   \
  pthread_mutex_lock(&mmclient_ds_global_mutex);

#define MMCLIENT_DS_GLOBAL_UNLOCK()                                \
  MMCLIENT_LOG_LOW("%s(): unlocking DS global mutex\n", __func__); \
  pthread_mutex_unlock(&mmclient_ds_global_mutex);

extern mmclient_nad_inst_t mmclient_get_nad_inst_for_nad_hndl
(
  mmclient_hndl_t *nad_hndl
);

/*===========================================================================
  FUNCTION  mmclient_lookup_data_handle
===========================================================================*/
/*!
@brief
  Lookup information for the given data handle

@arg dh - Data handle to lookup information for

@note
  - Dependencies
    - mmclient_data_get_srvc_hndl should have been called

  - Side effects
    - None
*/
/*==========================================================================*/
static int mmclient_lookup_data_handle(mmclient_data_hndl_t  dh, int *index)
{
  int i = 0;

  MMCLIENT_DS_LIST_MUTEX_LOCK();

  for(i = 0 ; i < MMCLIENT_MAX_DATA_CLIENT ; i++)
  {
    if(mm_ds_client_info[i].valid
         && mm_ds_client_info[i].st_hndl == dh)
      break;
  }

  if(i == MMCLIENT_MAX_DATA_CLIENT)
  {
    /* Error. No matching DSI handles to release */
    MMCLIENT_DS_LIST_MUTEX_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    *index = i;
    MMCLIENT_DS_LIST_MUTEX_UNLOCK();
    return MMCLIENT_SUCCESS;
  }
}

/*===========================================================================
  FUNCTION  mmclient_data_get_srvc_hndl
===========================================================================*/
/*!
@brief
  Used to get data service handle. This handle can be used by the caller
  to exercise data service functionalities. The data handle will be tied
  to the specified NAD handle. The user needs to pass in a callback
  function in order to receive notifications about the call state
  ex. connection, disconnected etc

@return
  mmclient_data_hndl_t reference

@arg nad_hndl - NAD handle
@arg cb - Callback function to return events related to call state
@arg user_data - any information the client wants to pass along

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_data_hndl_t mmclient_data_get_srvc_hndl
(
  mmclient_nad_hndl_t        nh,
  mmclient_data_cb_func_t    cb,
  void                       *user_data
)
{
  int                   i = 0;
  int                   bytes = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  unsigned char         data_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  for(i = 0 ; i < MMCLIENT_MAX_DATA_CLIENT ; i++)
  {
    if(!mm_ds_client_info[i].valid)
      break;
  }

  if(i == MMCLIENT_MAX_DATA_CLIENT)
  {
    /*Error. No more DSI handles*/
    MMCLIENT_LOG_ERR("%s(): No more handles available!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return NULL;
  }
  else
  {
    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_GET_SRVC_HNDL_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message header */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }

    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_GET_SRVC_HNDL_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nh);
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) nh,
                                      MMCLIENT_MAX_SRVC_HNDL_LEN, (void*) &data_hndl,
                                      MMCLIENT_MAX_SRVC_HNDL_LEN);

    if(ret == MMCLIENT_MSG_NO_ERR)
    {
      MMCLIENT_DS_LIST_MUTEX_LOCK();

      bytes = snprintf((char *) mm_ds_client_info[i].data_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                       (char *) data_hndl);
      if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
      {
        MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
        MMCLIENT_DS_LIST_MUTEX_UNLOCK();
        goto bail;
      }

      mm_ds_client_info[i].valid            = TRUE;
      mm_ds_client_info[i].mmclient_cb_func = cb;
      mm_ds_client_info[i].user_data        = user_data;
      mm_ds_client_info[i].st_hndl          = (mmclient_data_hndl_t) mm_ds_client_info[i].data_hndl;
      mm_ds_client_info[i].nad_inst         = mmclient_get_nad_inst_for_nad_hndl(nh);

      MMCLIENT_DS_LIST_MUTEX_UNLOCK();

      MMCLIENT_LOG_MED("%s(): Returning store handle %p for Data handle %s index %d!\n",
                       __func__, mm_ds_client_info[i].st_hndl, mm_ds_client_info[i].data_hndl, i);

      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_DS_GLOBAL_UNLOCK();
      return (mm_ds_client_info[i].st_hndl);
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    }
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return NULL;
}

/*===========================================================================
  FUNCTION  mmclient_data_release_srvc_hndl
===========================================================================*/
/*!
@brief
  Function used to release the data service handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg dh - Data call handle

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_release_srvc_hndl
(
  mmclient_data_hndl_t  dh
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  if(MMCLIENT_SUCCESS != mmclient_lookup_data_handle(dh, &i))
  {
    /* Error. No matching DSI handles to release */
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): Store handle %p for Data handle %s index %d!\n",
                     __func__, mm_ds_client_info[i].st_hndl, mm_ds_client_info[i].data_hndl, i);

    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_RELEASE_SRVC_HNDL_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message header */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }

    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_RELEASE_SRVC_HNDL_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_ds_client_info[i].nad_inst;
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    ret = mmclient_dbus_send_msg(conn, message, msg_hdr,
                                 (void*) &(mm_ds_client_info[i].data_hndl),
                                 MMCLIENT_MAX_SRVC_HNDL_LEN);

    if(ret == MMCLIENT_MSG_NO_ERR)
    {
      /*Call function to release DSI handle
      /Rel_data_srvc_handle never fails*/
      MMCLIENT_DS_LIST_MUTEX_LOCK();

      mm_ds_client_info[i].valid            = FALSE;
      mm_ds_client_info[i].st_hndl          = NULL;
      mm_ds_client_info[i].mmclient_cb_func = NULL;
      mm_ds_client_info[i].user_data        = NULL;
      mm_ds_client_info[i].nad_inst         = MMCLIENT_NAD_INSTANCE_INVALID;

      MMCLIENT_DS_LIST_MUTEX_UNLOCK();

      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_DS_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): request failed for for %s [%d]!\n",
                       __func__, mm_ds_client_info[i].data_hndl, ret);
    }
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_data_start_call
===========================================================================*/
/*!
@brief
  Used to trigger a data call. The data handle would be tied to a specific
  NAD so the call also will be brought up on that specific NAD. The caller
  will pass in the call parameters on which the call needs to be brought up

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg call_params - call parameters

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_start_call
(
  mmclient_data_hndl_t    dh,
  mmclient_call_params_t  *call_params
)
{
  int                             i = 0;
  int                             bytes = 0;
  int                             ret = MM_FAILURE;
  int                             reti = MM_FAILURE;
  mmclient_dbus_state_t           *dbus_client_state = NULL;
  mmclient_msg_hdr_t              *msg_hdr = NULL;
  DBusMessage                     *message = NULL;
  DBusConnection                  *conn = NULL;
  mmclient_data_call_param_info_t data_call_param;

  MMCLIENT_DS_GLOBAL_LOCK();

  memset(&data_call_param, 0, sizeof(data_call_param));

  if(MMCLIENT_SUCCESS != mmclient_lookup_data_handle(dh, &i))
  {
    /*Error. No matching DSI handles*/
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): Store handle %p for Data handle %s index %d!\n",
                     __func__, mm_ds_client_info[i].st_hndl, mm_ds_client_info[i].data_hndl, i);

    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_START_CALL_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message reply */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }

    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_START_CALL_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_ds_client_info[i].nad_inst;
    msg_hdr->payload_len = (unsigned int) sizeof(mmclient_data_call_param_info_t);

    bytes = snprintf((char *) data_call_param.data_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                     (char *) mm_ds_client_info[i].data_hndl);
    if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
    {
      MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
      goto bail;
    }

    data_call_param.call_params = *((mmclient_call_params_t *)call_params);

    MMCLIENT_LOG_LOW("%s(): CLIENT DATA handle     %s!\n",
                     __func__, (data_call_param.data_hndl));
    MMCLIENT_LOG_LOW("%s(): CLIENT IP family       %d!\n",
                     __func__, (data_call_param.call_params.ip_family));
    MMCLIENT_LOG_LOW("%s(): CLIENT tech pref       %d!\n",
                     __func__, (data_call_param.call_params.tech_pref));
    MMCLIENT_LOG_LOW("%s(): CLIENT auth pref       %d!\n",
                     __func__, (data_call_param.call_params.auth_pref));
    MMCLIENT_LOG_LOW("%s(): CLIENT apn name        %s!\n",
                     __func__, (data_call_param.call_params.apn_name));
    MMCLIENT_LOG_LOW("%s(): CLIENT User name       %s!\n",
                     __func__, (data_call_param.call_params.user_name));
    MMCLIENT_LOG_LOW("%s(): CLIENT password        %s!\n",
                     __func__, (data_call_param.call_params.password));

    ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &data_call_param,
                                      (unsigned int) sizeof(mmclient_data_call_param_info_t),
                                      (void*) &reti, (unsigned int) sizeof(int));
  }

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_LOG_LOW("%s(): Start Data call succeeded!\n", __func__);
      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_DS_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): request failed!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): start data call request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_data_stop_call
===========================================================================*/
/*!
@brief
  This function can be used to stop a data call

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_stop_call
(
  mmclient_data_hndl_t  dh
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  int                   reti = MM_FAILURE;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  if(MMCLIENT_SUCCESS != mmclient_lookup_data_handle(dh, &i))
  {
    /*Error. No matching DSI handles*/
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): Store handle %p for Data handle %s index %d!\n",
                     __func__, mm_ds_client_info[i].st_hndl, mm_ds_client_info[i].data_hndl, i);

    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_STOP_CALL_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message reply */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }
    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_STOP_CALL_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_ds_client_info[i].nad_inst;
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                      (void*)(mm_ds_client_info[i].data_hndl),
                                      MMCLIENT_MAX_SRVC_HNDL_LEN,
                                      (void*) &reti, (unsigned int) sizeof(int));
  }

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_LOG_LOW("%s(): Stop Data call succeeded!\n", __func__);
      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_DS_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): request failed!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): stop data call request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_data_get_call_end_reason
===========================================================================*/
/*!
@brief
  Function used to query call end information

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg ce - out param for passing call end reason information

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_get_call_end_reason
(
  mmclient_data_hndl_t        data_hndl,
  mmclient_call_end_reason_t  *ce
)
{
  int                         i = 0;
  int                         ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t       *dbus_client_state = NULL;
  mmclient_msg_hdr_t          *msg_hdr = NULL;
  DBusMessage                 *message = NULL;
  DBusConnection              *conn = NULL;
  mmclient_call_end_reason_t  ce_reason;

  MMCLIENT_DS_GLOBAL_LOCK();

  if (!ce)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }

  if(MMCLIENT_SUCCESS != mmclient_lookup_data_handle(data_hndl, &i))
  {
    /*Error. No matching DSI handles*/
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): Store handle %p for Data handle %s index %d!\n",
                     __func__, mm_ds_client_info[i].st_hndl, mm_ds_client_info[i].data_hndl, i);

    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_GET_CALL_END_REASON_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message reply */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }
    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_GET_CALL_END_REASON_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_ds_client_info[i].nad_inst;
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                      (void*)(mm_ds_client_info[i].data_hndl),
                                      MMCLIENT_MAX_SRVC_HNDL_LEN,
                                      (void*) &ce_reason,
                                      (unsigned int) sizeof(mmclient_call_end_reason_t));
  }

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    /* Fill in the data into the pointer passed by the client  */
    memcpy(ce, &ce_reason, sizeof(mmclient_call_end_reason_t));

    mmclient_free_msg_hdr(msg_hdr);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_data_get_call_info
===========================================================================*/
/*!
@brief
  Function used for querying the call information

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg *call_info - call information

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_get_call_info
(
  mmclient_data_hndl_t  dh,
  mmclient_call_info_t  *call_info
)
{
  int                            i = 0;
  int                            bytes = 0;
  int                            ret = MMCLIENT_MSG_NO_ERR;
  int                            ip_counter = 0;
  int                            valid_v4 = FALSE;
  int                            valid_v6 = FALSE;
  uint32_t                       ip_addr;
  mmclient_call_info_formatter_t data_call_info;
  mmclient_dbus_state_t          *dbus_client_state = NULL;
  mmclient_msg_hdr_t             *msg_hdr = NULL;
  DBusMessage                    *message = NULL;
  DBusConnection                 *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  if(MMCLIENT_SUCCESS != mmclient_lookup_data_handle(dh, &i))
  {
    /* Error. No matching DSI handles */
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_GET_CALL_INFO_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message reply */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }
    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_GET_CALL_INFO_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_ds_client_info[i].nad_inst;
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                      (void*)(mm_ds_client_info[i].data_hndl),
                                      MMCLIENT_MAX_SRVC_HNDL_LEN,
                                      (void*) &data_call_info,
                                      (unsigned int) sizeof(mmclient_call_info_formatter_t));
  }

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  bytes = snprintf((char *) call_info->iface, (MMCLIENT_MAX_IFACE_NAME_LEN + 1), "%s",
                   (char *) data_call_info.iface);
  if (bytes >= (MMCLIENT_MAX_IFACE_NAME_LEN + 1))
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  call_info->num_addrs = data_call_info.num_addrs;
  valid_v4             = data_call_info.valid_v4;
  valid_v6             = data_call_info.valid_v6;

  ip_counter = 0;

  if(valid_v4)
  {
    /*Setting ip type so that client is aware of it even if query fails*/
    call_info->addrs[ip_counter].iface_addr.ip_type  = data_call_info.ip_type_v4;
    if(!strcmp((char *)data_call_info.iface_addr_v4, MMCLIENT_INVALID_IP_ADDR))
    {
      call_info->addrs[ip_counter].iface_addr.addr.ipv4 = 0;
      call_info->addrs[ip_counter].iface_mask           = 0;
    }
    else
    {
      inet_pton(AF_INET, (char *)data_call_info.iface_addr_v4, &ip_addr);
      ip_addr = ntohl(ip_addr);
      call_info->addrs[ip_counter].iface_addr.addr.ipv4 = ip_addr;
      call_info->addrs[ip_counter].iface_mask           = data_call_info.iface_mask_v4;
    }

    if(!strcmp((char *)data_call_info.gtwy_addr_v4, MMCLIENT_INVALID_IP_ADDR))
    {
      call_info->addrs[ip_counter].gtwy_addr.ip_type    = 0;
      call_info->addrs[ip_counter].gtwy_addr.addr.ipv4  = 0;
      call_info->addrs[ip_counter].gtwy_mask            = 0;
    }
    else
    {
      call_info->addrs[ip_counter].gtwy_addr.ip_type    = data_call_info.ip_type_v4;
      inet_pton(AF_INET, (char *)data_call_info.gtwy_addr_v4, &ip_addr);
      ip_addr = ntohl(ip_addr);
      call_info->addrs[ip_counter].gtwy_addr.addr.ipv4  = ip_addr;
      call_info->addrs[ip_counter].gtwy_mask            = data_call_info.gtwy_mask_v4;
    }

    if(!strcmp((char *)data_call_info.dnsp_addr_v4, MMCLIENT_INVALID_IP_ADDR))
    {
      call_info->addrs[ip_counter].dnsp_addr.ip_type    = 0;
      call_info->addrs[ip_counter].dnsp_addr.addr.ipv4  = 0;
    }
    else
    {
      call_info->addrs[ip_counter].dnsp_addr.ip_type    = data_call_info.ip_type_v4;
      inet_pton(AF_INET, (char *)data_call_info.dnsp_addr_v4, &ip_addr);
      ip_addr = ntohl(ip_addr);
      call_info->addrs[ip_counter].dnsp_addr.addr.ipv4  = ip_addr;
    }

    if(!strcmp((char *)data_call_info.dnss_addr_v4, MMCLIENT_INVALID_IP_ADDR))
    {
      call_info->addrs[ip_counter].dnss_addr.ip_type    = 0;
      call_info->addrs[ip_counter].dnss_addr.addr.ipv4  = 0;
    }
    else
    {
      call_info->addrs[ip_counter].dnss_addr.ip_type    = data_call_info.ip_type_v4;
      inet_pton(AF_INET, (char *)data_call_info.dnss_addr_v4, &ip_addr);
      ip_addr = ntohl(ip_addr);
      call_info->addrs[ip_counter].dnss_addr.addr.ipv4  = ip_addr;
    }

    ip_counter++;
  }

  if(valid_v6)
  {
    /*Setting ip type so that client is aware of it even if query fails*/
    call_info->addrs[ip_counter].iface_addr.ip_type      = data_call_info.ip_type_v6;

    if(!strcmp((char *)data_call_info.iface_addr_v6, MMCLIENT_INVALID_IP_ADDR))
    {
      memset(call_info->addrs[ip_counter].iface_addr.addr.ipv6, 0,
             sizeof(uint8_t)*MMCLIENT_IPV6_ADDR_SIZE);
      call_info->addrs[ip_counter].iface_mask            = 0;
    }
    else
    {
      inet_pton(AF_INET6, (char *)data_call_info.iface_addr_v6,
                call_info->addrs[ip_counter].iface_addr.addr.ipv6);
      call_info->addrs[ip_counter].iface_mask                  = data_call_info.iface_mask_v6;
    }

    if(!strcmp((char *)data_call_info.gtwy_addr_v6, MMCLIENT_INVALID_IP_ADDR))
    {
      memset(call_info->addrs[ip_counter].gtwy_addr.addr.ipv6, 0,
             sizeof(uint8_t)*MMCLIENT_IPV6_ADDR_SIZE);
      call_info->addrs[ip_counter].gtwy_addr.ip_type     = 0;
      call_info->addrs[ip_counter].gtwy_mask             = 0;
    }
    else
    {
      call_info->addrs[ip_counter].gtwy_addr.ip_type     = data_call_info.ip_type_v6;
      inet_pton(AF_INET6, (char *)data_call_info.gtwy_addr_v6,
                call_info->addrs[ip_counter].gtwy_addr.addr.ipv6);

      call_info->addrs[ip_counter].gtwy_mask             = data_call_info.gtwy_mask_v6;
    }

    if(!strcmp((char *)data_call_info.dnsp_addr_v6, MMCLIENT_INVALID_IP_ADDR))
    {
      memset(call_info->addrs[ip_counter].dnsp_addr.addr.ipv6, 0,
             sizeof(uint8_t)*MMCLIENT_IPV6_ADDR_SIZE);
      call_info->addrs[ip_counter].dnsp_addr.ip_type     = 0;
    }
    else
    {
      call_info->addrs[ip_counter].dnsp_addr.ip_type     = data_call_info.ip_type_v6;
      inet_pton(AF_INET6, (char *)data_call_info.dnsp_addr_v6,
                call_info->addrs[ip_counter].dnsp_addr.addr.ipv6);
    }

    if(!strcmp((char *)data_call_info.dnss_addr_v6, MMCLIENT_INVALID_IP_ADDR))
    {
      memset(call_info->addrs[ip_counter].dnss_addr.addr.ipv6, 0,
             sizeof(uint8_t)*MMCLIENT_IPV6_ADDR_SIZE);
      call_info->addrs[ip_counter].dnss_addr.ip_type     = 0;
    }
    else
    {
      call_info->addrs[ip_counter].dnss_addr.ip_type     = data_call_info.ip_type_v6;
      inet_pton(AF_INET6, (char *)data_call_info.dnss_addr_v6,
                call_info->addrs[ip_counter].dnss_addr.addr.ipv6);
    }

    ip_counter++;
  }

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}


/*===========================================================================
  FUNCTION  mmclient_data_get_current_bearer_tech
===========================================================================*/
/*!
@brief
  Function used to query current bearer technology

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg data_hndl - Data call handle
@arg bt - out param for passing bearer tech information

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_get_current_bearer_tech
(
  mmclient_data_hndl_t        data_hndl,
  mmclient_data_bearer_tech_t *bt
)
{
  int                          i = 0;
  int                          ret = MMCLIENT_MSG_NO_ERR;
  mmclient_data_bearer_tech_t  bearer_tech = MMCLIENT__DATA_BEARER_TECH_UNKNOWN;
  mmclient_dbus_state_t        *dbus_client_state = NULL;
  mmclient_msg_hdr_t           *msg_hdr = NULL;
  DBusMessage                  *message = NULL;
  DBusConnection               *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  if(MMCLIENT_SUCCESS != mmclient_lookup_data_handle(data_hndl, &i))
  {
    /* Error. No matching DSI handles */
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    /* Send message via DBUS */
    MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_GET_CURR_BEARER_TECH_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message reply */
    msg_hdr = mmclient_get_msg_hdr();
    if(!msg_hdr)
    {
      goto bail;
    }
    msg_hdr->srvc_id     = SRVC_ID_DATA_CALL;
    msg_hdr->msg_id      = MSG_ID_DATA_GET_CURR_BEARER_TECH_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_ds_client_info[i].nad_inst;
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    ret = mmclient_dbus_send_msg_sync( conn, message, msg_hdr,
                                       (void*)(mm_ds_client_info[i].data_hndl),
                                       MMCLIENT_MAX_SRVC_HNDL_LEN,
                                       (void*) &bearer_tech,
                                       (unsigned int) sizeof(mmclient_data_bearer_tech_t));

    if(ret != MMCLIENT_MSG_NO_ERR)
    {
      MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
      goto bail;
    }
  }

  *bt = bearer_tech;

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;

}


/*===========================================================================
  FUNCTION  mmclient_data_evt_hdlr
===========================================================================*/
/*!
@brief
  Function registered as callback with Modem Manager client Indication
  handler for DATA messages

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg dh  - Data call handle
@arg evt - Indication event

@note
  Dependencies
    - mmclient_data_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_data_evt_hdlr
(
  unsigned char               *dh,
  mmclient_data_call_evt_t     evt
)
{
  int i = 0;

  MMCLIENT_DS_GLOBAL_LOCK();

  for(i = 0 ; i < MMCLIENT_MAX_DATA_CLIENT ; i++)
  {
     if(mm_ds_client_info[i].valid &&
        (!strcmp((char *)mm_ds_client_info[i].data_hndl , (char *)dh)))
       break;
  }

  if(i == MMCLIENT_MAX_DATA_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Error. No matching DSI handles to post evt!\n", __func__);
    goto bail;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): Store handle %p for Data handle %s index %d!\n",
                     __func__, mm_ds_client_info[i].st_hndl, mm_ds_client_info[i].data_hndl, i);
    if(mm_ds_client_info[i].mmclient_cb_func)
    {
      MMCLIENT_DS_GLOBAL_UNLOCK();
      mm_ds_client_info[i].mmclient_cb_func(mm_ds_client_info[i].st_hndl,
                                            mm_ds_client_info[i].user_data,
                                            evt);

      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): No cb function registered!\n", __func__);
      goto bail;
    }
  }

bail:
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_profile_query
===========================================================================*/
/*!
@brief
  Function used to lookup a modem profile based on provided parameters

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *profile_params - Profile parameters that will be used in the
                       lookup
@arg *prof_num - Profile number if found

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_query
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_profile_params_t *profile_params,
  mmclient_profile_id_t     *prof_num
)
{
  int                   ret = MMCLIENT_MSG_NO_ERR;
  int                   bytes = 0;
  mmclient_prof_param_t profile_param;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  memset(&profile_param, 0, sizeof(profile_param));

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_PROFILE_QUERY_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if(!msg_hdr)
  {
    goto bail;
  }
  msg_hdr->srvc_id     = SRVC_ID_PROFILE;
  msg_hdr->msg_id      = MSG_ID_PROFILE_QUERY_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_prof_param_t);

  bytes = snprintf((char *) profile_param.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  profile_param.prof_param = *((mmclient_profile_params_t *)profile_params);

  MMCLIENT_LOG_MED("%s(): Profile Query for nad handle [%s]!\n", __func__, profile_param.nad_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &profile_param,
                                    (unsigned int) sizeof(mmclient_prof_param_t),
                                    (void*) prof_num,
                                    (unsigned int) sizeof(mmclient_profile_id_t));

  MMCLIENT_LOG_LOW("%s(): Profile number returned for query [%u]!\n", __func__, *prof_num);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }
  else if(*prof_num == MMCLIENT_INVALID_PROFILE)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid profile returned. Query failed!\n", __func__);
    goto bail;
  }

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_profile_create
===========================================================================*/
/*!
@brief
  Function used to create a modem profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *profile_params - Profile parameters that will be used in creation
@arg *prof_num - Profile number if created successfully

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_create
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_profile_params_t  *profile_params,
  mmclient_profile_id_t      *prof_num
)
{
  int                    ret = MMCLIENT_MSG_NO_ERR;
  int                    bytes = 0;
  mmclient_prof_param_t  profile_param;
  mmclient_dbus_state_t  *dbus_client_state = NULL;
  mmclient_msg_hdr_t     *msg_hdr = NULL;
  DBusMessage            *message = NULL;
  DBusConnection         *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  memset(&profile_param, 0, sizeof(profile_param));

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_PROFILE_CREATE_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if(!msg_hdr)
  {
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_PROFILE;
  msg_hdr->msg_id      = MSG_ID_PROFILE_CREATE_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_prof_param_t);

  bytes = snprintf((char *) profile_param.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  profile_param.prof_param = *((mmclient_profile_params_t *)profile_params);

  MMCLIENT_LOG_MED("%s(): Profile Creation for nad handle [%s]!\n",
                   __func__, profile_param.nad_hndl);

  ret = mmclient_dbus_send_msg_sync( conn, message, msg_hdr,
                                     (void*) &profile_param,
                                     (unsigned int) sizeof(mmclient_prof_param_t),
                                     (void*) prof_num,
                                     (unsigned int) sizeof(mmclient_profile_id_t));

  MMCLIENT_LOG_LOW("%s(): Profile number returned for creation [%u]!\n", __func__, *prof_num);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }
  else if(*prof_num == MMCLIENT_INVALID_PROFILE)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid profile returned. creation failed!\n", __func__);
    goto bail;
  }

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_profile_delete
===========================================================================*/
/*!
@brief
  Function used to delete a modem profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *prof_num - Profile number to be deleted

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_delete
(
  mmclient_nad_hndl_t    nad_hndl,
  mmclient_profile_id_t  prof_num
)
{
  int                            ret = MMCLIENT_MSG_NO_ERR;
  int                            bytes = 0;
  uint32_t                       reti = MM_FAILURE;
  mmclient_profile_delete_info_t data_profile_param;
  mmclient_dbus_state_t          *dbus_client_state = NULL;
  mmclient_msg_hdr_t             *msg_hdr = NULL;
  DBusMessage                    *message = NULL;
  DBusConnection                 *conn = NULL;

  MMCLIENT_DS_GLOBAL_LOCK();

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_PROFILE_DELETE_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if(!msg_hdr)
  {
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_PROFILE;
  msg_hdr->msg_id      = MSG_ID_PROFILE_DELETE_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_profile_delete_info_t);

  bytes = snprintf((char *) data_profile_param.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  data_profile_param.prof_num = prof_num;

  MMCLIENT_LOG_LOW("%s(): Profile deletion for nad handle [%s]!\n",
                   __func__, data_profile_param.nad_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &data_profile_param,
                                    (unsigned int) sizeof(mmclient_profile_delete_info_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }
  else if(reti != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): Profile Deletion failed!\n", __func__);
    goto bail;
  }

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_profile_modify
===========================================================================*/
/*!
@brief
  Function used to modify an existing profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *prof_num - Profile number to modify
@arg *new_profile_params - new profile parameters

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_modify
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_profile_id_t      prof_num,
  mmclient_profile_params_t  *new_profile_params
)
{
  int                   ret = MMCLIENT_MSG_NO_ERR;
  int                   bytes = 0;
  uint32_t              reti = MM_FAILURE;
  mmclient_prof_param_t profile_param;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  if(!nad_hndl || !new_profile_params)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return MMCLIENT_FAILURE;
  }

  MMCLIENT_DS_GLOBAL_LOCK();

  memset(&profile_param, 0, sizeof(profile_param));

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_PROFILE_MODIFY_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if(!msg_hdr)
  {
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_PROFILE;
  msg_hdr->msg_id      = MSG_ID_PROFILE_MODIFY_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_prof_param_t);

  bytes = snprintf((char *) profile_param.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  profile_param.prof_param = *((mmclient_profile_params_t *)new_profile_params);
  profile_param.prof_num   = prof_num;

  MMCLIENT_LOG_MED("%s(): Profile Modification for nad handle [%s]!\n",
                   __func__, profile_param.nad_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &profile_param,
                                    (unsigned int) sizeof(mmclient_prof_param_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }
  else if(reti != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): Profile modification failed!\n", __func__);
    goto bail;
  }

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;

}

/*===========================================================================
  FUNCTION  mmclient_profile_set_initial_attach
===========================================================================*/
/*!
@brief
  Function used to set initial attach profile

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
@arg *prof_num - Profile num used for initial attach

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_profile_set_initial_attach
(
  mmclient_nad_hndl_t    nad_hndl,
  mmclient_profile_id_t  prof_num
)
{
  int                   ret = MMCLIENT_MSG_NO_ERR;
  int                   bytes = 0;
  uint32_t              reti = MM_FAILURE;
  mmclient_prof_param_t profile_param;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  if(!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return MMCLIENT_FAILURE;
  }

  MMCLIENT_DS_GLOBAL_LOCK();

  memset(&profile_param, 0, sizeof(profile_param));

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_PROFILE_SET_INITIAL_ATTACH_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if(!msg_hdr)
  {
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_PROFILE;
  msg_hdr->msg_id      = MSG_ID_PROFILE_SET_INITIAL_ATTACH_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_prof_param_t);

  bytes = snprintf((char *) profile_param.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  profile_param.prof_num   = prof_num;

  MMCLIENT_LOG_MED("%s(): Setting Initial attach for nad handle [%s]!\n",
                   __func__, profile_param.nad_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) &profile_param,
                                    (unsigned int) sizeof(mmclient_prof_param_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }
  else if(reti != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): Setting Initial attach failed!\n", __func__);
    goto bail;
  }

  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_DS_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_data_cleanup_state_for_nad_inst
===========================================================================*/
/*!
@brief
  Function to cleanup resources

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
void mmclient_data_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst)
{
  int i = 0;

  MMCLIENT_DS_GLOBAL_LOCK();

  if (MMCLIENT_NAD_INSTANCE_INVALID == nad_inst)
  {
    /* NAD instance is invalid or list does not have any entries
       Nothing to be done */
    MMCLIENT_DS_GLOBAL_UNLOCK();
    return;
  }

  MMCLIENT_LOG_MED("%s(): cleaning up data state for NAD inst [%d]\n",
                   __func__, nad_inst);

  MMCLIENT_DS_LIST_MUTEX_LOCK();
  for(i = 0 ; i < MMCLIENT_MAX_DATA_CLIENT ; i++)
  {
    if (mm_ds_client_info[i].valid
          && mm_ds_client_info[i].nad_inst == nad_inst)
    {
      mm_ds_client_info[i].valid            = FALSE;
      mm_ds_client_info[i].st_hndl          = NULL;
      mm_ds_client_info[i].mmclient_cb_func = NULL;
      mm_ds_client_info[i].user_data        = NULL;
      mm_ds_client_info[i].nad_inst         = MMCLIENT_NAD_INSTANCE_INVALID;
    }
  }

  MMCLIENT_DS_LIST_MUTEX_UNLOCK();
  MMCLIENT_DS_GLOBAL_UNLOCK();
}

/*===========================================================================
  FUNCTION  mmclient_data_cleanup_state
===========================================================================*/
/*!
@brief
  Function to cleanup resources

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
int mmclient_data_cleanup_state(void)
{
  int i = 0;

  MMCLIENT_DS_GLOBAL_LOCK();
  MMCLIENT_DS_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_MED("%s(): cleaning up data state...", __func__);

  for (i = 0; i < MMCLIENT_MAX_DATA_CLIENT; i++)
  {
    mm_ds_client_info[i].valid            = FALSE;
    mm_ds_client_info[i].st_hndl          = NULL;
    mm_ds_client_info[i].mmclient_cb_func = NULL;
    mm_ds_client_info[i].user_data        = NULL;
    mm_ds_client_info[i].nad_inst         = MMCLIENT_NAD_INSTANCE_INVALID;
  }

  MMCLIENT_DS_LIST_MUTEX_UNLOCK();
  MMCLIENT_DS_GLOBAL_UNLOCK();

  return MM_SUCCESS;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
