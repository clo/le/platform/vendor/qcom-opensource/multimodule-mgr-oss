/******************************************************************************

             M M C L I E N T _ B W _ H D L R . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <pthread.h>

#include "mmclient_dbus.h"
#include "mmclient_util.h"
#include "mmclient_msg.h"

/*=================================================================================
                             LOCAL DECLARATIONS
=================================================================================*/

#define MMCLIENT_DBUS_GET_CONNECTION_OBJ(conn_state, conn)                      \
  conn_state = mmclient_get_connection_state();                                 \
  if (!conn_state)                                                              \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): failed to get connection state!\n", __func__);      \
    goto bail;                                                                  \
  }                                                                             \
                                                                                \
  conn = conn_state->conn;                                                      \
  if (!conn)                                                                    \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): invalid connection object!", __func__);             \
    goto bail;                                                                  \
  }                                                                             \
                                                                                \
  if (conn_state->connection_status != STATE_CONNECTED)                         \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): DBus connection was terminated. "                   \
                     "Please reconnect!\n", __func__);                          \
    goto bail;                                                                  \
  }

#define MMCLIENT_MAX_BW_HANDLES 10

struct mmclient_bw_info_t
{
  mmclient_nad_inst_t         nad_inst;
  char                        nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_bw_ind_cb_func_t   cb;
  void                        *user_data;
  struct mmclient_bw_info_t   *next;
};

/* Save handle information locally */
struct mmclient_bw_info_t *bw_hndl_list;
static unsigned int list_size;

pthread_mutex_t clnt_bw_list_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_BW_LIST_MUTEX_LOCK()                             \
  MMCLIENT_LOG_LOW("%s(): locking BW list mutex\n", __func__);    \
  pthread_mutex_lock(&clnt_bw_list_mtx);

#define MMCLIENT_BW_LIST_MUTEX_UNLOCK()                           \
  MMCLIENT_LOG_LOW("%s(): unlocking BW list mutex\n", __func__);  \
  pthread_mutex_unlock(&clnt_bw_list_mtx);

pthread_mutex_t mmclient_bw_global_mutex = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_BW_GLOBAL_LOCK()                                  \
  MMCLIENT_LOG_LOW("%s(): locking BW global mutex\n", __func__);   \
  pthread_mutex_lock(&mmclient_bw_global_mutex);

#define MMCLIENT_BW_GLOBAL_UNLOCK()                                \
  MMCLIENT_LOG_LOW("%s(): unlocking BW global mutex\n", __func__); \
  pthread_mutex_unlock(&mmclient_bw_global_mutex);

extern mmclient_nad_inst_t mmclient_get_nad_inst_for_nad_hndl
(
  mmclient_hndl_t *nad_hndl
);

/*============================================================================
  FUNCTION  mmclient_alloc_bw_hndl
============================================================================*/
  /*!
  @brief
    Stores information about the allocated BW callback info in a list

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
static int mmclient_alloc_bw_hndl
(
  char                       *nad_hndl,
  mmclient_bw_ind_cb_func_t   cb,
  void                       *user_data
)
{
  int                       rc = MM_FAILURE;
  int                       bytes = 0;
  struct mmclient_bw_info_t *bw_hndl_entry = NULL;

  MMCLIENT_BW_LIST_MUTEX_LOCK();

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid NAD handle!\n", __func__);
    goto bail;
  }

  if (MMCLIENT_MAX_BW_HANDLES == list_size)
  {
    MMCLIENT_LOG_ERR("%s(): max handles reached, cannot store more information!\n", __func__);
    goto bail;
  }

  bw_hndl_entry = (struct mmclient_bw_info_t*) malloc(sizeof(struct mmclient_bw_info_t));
  if (!bw_hndl_entry)
  {
    MMCLIENT_LOG_ERR("%s(): failed to allocate memory for LQE handle information!\n", __func__);
    goto bail;
  }

  bytes = snprintf(bw_hndl_entry->nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s", nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    free(bw_hndl_entry);
    bw_hndl_entry = NULL;
    goto bail;
  }
  bw_hndl_entry->nad_inst = mmclient_get_nad_inst_for_nad_hndl((mmclient_nad_hndl_t) nad_hndl);
  bw_hndl_entry->cb        = cb;
  bw_hndl_entry->user_data = user_data;

  /* Insert into list */
  bw_hndl_entry->next = bw_hndl_list;
  bw_hndl_list = bw_hndl_entry;

  list_size++;
  MMCLIENT_LOG_LOW("%s(): successfully added BW handle for NAD [%s] to list\n",
                   __func__, nad_hndl);

  rc = MM_SUCCESS;

  bail:
    MMCLIENT_BW_LIST_MUTEX_UNLOCK();
    return rc;
}

/*============================================================================
   FUNCTION  mmclient_lookup_bw_hndl
============================================================================*/
/*!
  @brief
    Helper function to lookup BW call information for the provided NAD handle

  @return Valid handle information if it exists, NULL otherwise

  @arg nad_hndl - NAD handle
*/
/*==========================================================================*/
static struct mmclient_bw_info_t* mmclient_lookup_bw_hndl
(
  char *nad_hndl
)
{
  int found = 0;
  struct mmclient_bw_info_t *bw_hndl_entry = NULL;

  MMCLIENT_BW_LIST_MUTEX_LOCK();

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid NAD handle!\n", __func__);
    goto bail;
  }

  bw_hndl_entry = bw_hndl_list;

  while (bw_hndl_entry != NULL)
  {
    if (!strcmp(bw_hndl_entry->nad_hndl, (char *)nad_hndl))
    {
      found = 1;
      break;
    }
     bw_hndl_entry = bw_hndl_entry->next;
  }

  if (!found)
  {
    MMCLIENT_LOG_ERR("%s(): unable to lookup NAD handle [%s]!\n", __func__, nad_hndl);
  }

bail:
  MMCLIENT_BW_LIST_MUTEX_UNLOCK();
  return bw_hndl_entry;
}

/*============================================================================
                           GLOBAL DEFINITIONS
============================================================================*/
/*============================================================================
  FUNCTION  mmclient_bandwidth_ind_hdlr
============================================================================*/
  /*!
  @brief
    Calls the registered callback function when bandwidth indications
    are received

  @return
    MMCLIENT_SUCCESS
    MMCLIENT_FAILURE
  */
/*==========================================================================*/
mmclient_status_t mmclient_bandwidth_ind_hdlr
(
  mmclient_bw_ind_container_t *cb_params
)
{
  struct mmclient_bw_info_t *bw_list_entry = NULL;

  MMCLIENT_BW_GLOBAL_LOCK();

  if(!cb_params)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    goto bail;
  }

  if(cb_params->evt < MMCLIENT_BW_DOWNLINK_THROUGHPUT_IND_EV
       || cb_params->evt > MMCLIENT_BW_UPLINK_DOWNLINK_THROUGHPUT_IND_EV)
  {
    MMCLIENT_LOG_ERR("%s(): Received invalid event [%d] !\n", __func__, cb_params->evt);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): callback invoked for NAD [%s] evt [%d]!\n",
                   __func__, (char *)cb_params->nad_hndl, cb_params->evt);

  bw_list_entry = mmclient_lookup_bw_hndl((char *) cb_params->nad_hndl);
  if(!bw_list_entry)
  {
    MMCLIENT_LOG_ERR("%s(): Received event on Invalid handle [%s] !\n",
                     __func__, (char *)cb_params->nad_hndl);
    goto bail;
  }

  /* Invoke callback registered with the associated NAD handle */
  if(bw_list_entry->cb)
  {
    MMCLIENT_BW_GLOBAL_UNLOCK();
    bw_list_entry->cb(cb_params->evt, &(cb_params->ind_cb), bw_list_entry->user_data);
    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): No cb function registered!\n", __func__);
  }

bail:
  MMCLIENT_BW_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*============================================================================
  FUNCTION  mmclient_bandwidth_register_ind
============================================================================*/
  /*!
  @brief
    Registers for the requested bandwidth related indication on the modem
    associated with the given NAD handle

  @return
    MMCLIENT_SUCCESS
    MMCLIENT_FAILURE
  */
/*==========================================================================*/
mmclient_status_t mmclient_bandwidth_register_ind
(
  mmclient_nad_hndl_t      nad_hndl,
  mmclient_bw_ind_events_t evt,
  mmclient_bw_cb_info_t   *bw_cb
)
{
  int                   ret = MMCLIENT_FAILURE;
  int                   reti = MM_FAILURE;
  int                   rc = MMCLIENT_MSG_NO_ERR;
  int                   bytes = 0;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;
  mmclient_bw_query_t   bw_query;

  MMCLIENT_BW_GLOBAL_LOCK();

  memset(&bw_query, 0, sizeof(bw_query));

  if(!bw_cb)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    goto bail;
  }

  if(evt < MMCLIENT_BW_DOWNLINK_THROUGHPUT_IND_EV
       || evt > MMCLIENT_BW_UPLINK_DOWNLINK_THROUGHPUT_IND_EV)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid event!\n", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_BANDWIDTH_IND_REG_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_BW_REG;
  msg_hdr->msg_id      = MSG_ID_BANDWIDTH_IND_REG_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_bw_query_t);

  MMCLIENT_LOG_MED("%s(): Registering for BW indication for NAD handle %s!\n",
                   __func__, (char *)nad_hndl);

  bytes = snprintf((char *) bw_query.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  bw_query.evt = evt;

  rc = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &bw_query,
                                   (unsigned int) sizeof(mmclient_bw_query_t),
                                   (void*) &reti, (unsigned int) sizeof(int));

  if(rc != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, rc);
    goto bail;
  }

  if(reti != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): BW ind registration failed for Nad handle %s!\n",
                     __func__, (char *)nad_hndl);
    goto bail;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): BW ind registration succeeded for Nad handle %s!\n",
                     __func__, (char *)nad_hndl);
  }

  rc = mmclient_alloc_bw_hndl((char *)nad_hndl, bw_cb->cb, bw_cb->user_data);
  if(rc != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): List allocation failed for NAD handle %s!\n",
                     __func__, (char *)nad_hndl);
    goto bail;
  }

  ret = MMCLIENT_SUCCESS;

bail:
  if(msg_hdr)
  {
    free(msg_hdr);
  }

  MMCLIENT_BW_GLOBAL_UNLOCK();
  return ret;
}

/*============================================================================
  FUNCTION  mmclient_bandwidth_query
============================================================================*/
  /*!
  @brief
    Quries current bandwidth setting on the modem associated with the
    given NAD handle

  @return
    MMCLIENT_SUCCESS
    MMCLIENT_FAILURE
  */
/*==========================================================================*/
mmclient_status_t mmclient_bandwidth_query
(
  mmclient_nad_hndl_t      nad_hndl,
  mmclient_bw_ind_info_t  *bw_info,
  mmclient_bw_ind_events_t evt
)
{
  int                         ret = MMCLIENT_FAILURE;
  int                         rc = MMCLIENT_MSG_NO_ERR;
  int                         bytes = 0;
  mmclient_dbus_state_t       *dbus_client_state = NULL;
  mmclient_msg_hdr_t          *msg_hdr = NULL;
  DBusMessage                 *message = NULL;
  DBusConnection              *conn = NULL;
  mmclient_bw_query_t         bw_query;
  mmclient_bw_ind_container_t bw_resp;

  memset(&bw_query, 0, sizeof(bw_query));

  if(!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    goto bail;
  }

  if(evt < MMCLIENT_BW_DOWNLINK_THROUGHPUT_IND_EV
       || evt > MMCLIENT_BW_UPLINK_DOWNLINK_THROUGHPUT_IND_EV)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass valid event %d!\n", __func__, evt);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_BANDWIDTH_QUERY_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_BW_REG;
  msg_hdr->msg_id      = MSG_ID_BANDWIDTH_QUERY_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_bw_query_t);

  MMCLIENT_LOG_MED("%s(): Querying for BW evts for NAD handle %s!\n", __func__, (char *)nad_hndl);

  bytes = snprintf((char *) bw_query.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred while preparing NAD handle!\n", __func__);
    goto bail;
  }

  bw_query.evt = evt;

  rc = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &bw_query,
                                   (unsigned int) sizeof(mmclient_bw_query_t),
                                   (void*) &bw_resp,
                                   (unsigned int) sizeof(mmclient_bw_ind_container_t));

  if(rc != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, rc);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): Copying query resp %s!\n", __func__, (char *)nad_hndl);
  *bw_info = bw_resp.ind_cb;

  ret = MMCLIENT_SUCCESS;

bail:
  if(msg_hdr)
  {
    free(msg_hdr);
  }

  return ret;
}

/*============================================================================
  FUNCTION  mmclient_bandwidth_cleanup_state_for_nad_inst
============================================================================*/
  /*!
  @brief
    Cleanup references related to the provided NAD instance

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
void mmclient_bandwidth_cleanup_state_for_nad_inst
(
  mmclient_nad_inst_t nad_inst
)
{
  struct mmclient_bw_info_t *curr = NULL;
  struct mmclient_bw_info_t *prev = NULL;

  MMCLIENT_BW_GLOBAL_LOCK();
  MMCLIENT_BW_LIST_MUTEX_LOCK();

  if (MMCLIENT_NAD_INSTANCE_INVALID == nad_inst || list_size == 0)
  {
    /* NAD instance is invalid or list does not have any entries
       Nothing to be done */
    MMCLIENT_BW_LIST_MUTEX_UNLOCK();
    MMCLIENT_BW_GLOBAL_UNLOCK();
    return;
  }

  curr = bw_hndl_list;

  while (curr != NULL)
  {
    if (curr->nad_inst == nad_inst)
    {
      MMCLIENT_LOG_LOW("%s(): releasing BW information for NAD handle [%s]"
                       " on NAD inst [%d]\n",
                       __func__, curr->nad_hndl, nad_inst);
      if (curr == bw_hndl_list)
      {
        bw_hndl_list = curr->next;
        list_size--;
        free(curr);
        curr = bw_hndl_list;
      }
      else
      {
        prev->next = curr->next;
        list_size--;
        free(curr);
        curr = prev->next;
      }
    }
    else
    {
      prev = curr;
      curr = curr->next;
    }
  }

  MMCLIENT_BW_LIST_MUTEX_UNLOCK();
  MMCLIENT_BW_GLOBAL_UNLOCK();
}

/*============================================================================
  FUNCTION  mmclient_bandwidth_cleanup_state
============================================================================*/
  /*!
  @brief
    Cleanup full BW handle list

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
void mmclient_bandwidth_cleanup_state(void)
{
  struct mmclient_bw_info_t *curr_node = NULL;
  struct mmclient_bw_info_t *next_node = NULL;

  MMCLIENT_BW_GLOBAL_LOCK();
  MMCLIENT_BW_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_MED("%s(): cleaning up BW state...\n", __func__);

  curr_node = bw_hndl_list;
  while (curr_node != NULL)
  {
    next_node = curr_node->next;
    free(curr_node);
    list_size--;
    curr_node = next_node;
  }

  bw_hndl_list = NULL;
  list_size = 0;

  MMCLIENT_BW_LIST_MUTEX_UNLOCK();
  MMCLIENT_BW_GLOBAL_UNLOCK();
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
