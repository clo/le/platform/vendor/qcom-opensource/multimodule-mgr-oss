/******************************************************************************

                  M M C L I E N T _ D B U S . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>

#include "mmclient_util.h"
#include "mmclient_dbus.h"
#include "mmclient_msg.h"

/*============================================================================
                             LOCAL DECLARATIONS
============================================================================*/
#define MMCLIENT_DBUS_IS_ERROR_SET(err, msg)                   \
  if (dbus_error_is_set(&err))                                 \
  {                                                            \
    MMCLIENT_LOG_ERR("%s(): %s! Err name: %s, Err msg: %s",    \
                     __func__, msg, err.name, err.message);    \
    goto bail;                                                 \
  }

#define MMCLIENT_DBUS_MAX_RULE_LEN 1024
#define MAX_WATCH_LIST_LEN         20

/* Global DBus connection state */
static mmclient_dbus_state_t *conn_state;

/* Flag to indicate that the client dbus connection has been initialized */
static dbus_bool_t dbus_inited = FALSE;
static dbus_bool_t connection_active = FALSE;

/* File descriptor set used for event listening */
static fd_set read_fds;
static int max_fd;

typedef struct
{
  DBusWatch *watch;
  int       watch_fd;
} mmclient_watch_info_t;

struct clnt_timeout_list_t
{
  DBusTimeout *timeout;
  struct clnt_timeout_list_t *next;
};

static struct clnt_timeout_list_t *tlist;

static mmclient_watch_info_t watch_list[MAX_WATCH_LIST_LEN];
static int wlist_size;

pthread_t signal_thread;

/* Pipe which connects the signal thread with the main thread. Mainly used for sending
   a message to unblock select */
int signal_thrd_fd[2];

extern mmclient_status_t mmclient_process_nad_callback
(
  nad_cb_info_t *nad_ind
);

extern mmclient_status_t mmclient_data_evt_hdlr
(
  unsigned char               *dh,
  mmclient_data_call_evt_t     evt
);

extern mmclient_status_t mmclient_sim_availability_hdlr
(
  char                        *sim_hndl,
  mmclient_sim_availability_t sim_availability
);

extern mmclient_status_t mmclient_sim_properties_hdlr
(
  char                      *sim_hndl,
  mmclient_sim_properties_t sim_properties
);

extern mmclient_status_t mmclient_nw_reg_properties_hdlr
(
  char                         *nw_reg_hndl,
  mmclient_nw_reg_properties_t nw_reg_properties
);

extern mmclient_status_t mmclient_nw_reg_scan_operators_hdlr
(
  char                                *nw_reg_hndl,
  mmclient_nw_reg_scanned_operators_t *scanned_operators
);

extern mmclient_status_t mmclient_bandwidth_ind_hdlr
(
  mmclient_bw_ind_container_t *cb
);

extern mmclient_status_t mmclient_dsd_ind_hdlr
(
  mmclient_data_sys_info_param_t *cb
);

extern void mmclient_data_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst);
extern void mmclient_nw_reg_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst);
extern void mmclient_sim_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst);
extern void mmclient_bandwidth_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst);
extern void mmclient_dsd_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst);
extern void mmclient_nad_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst);

pthread_mutex_t clnt_dbus_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_DBUS_LOCK()                                     \
  MMCLIENT_LOG_LOW("%s(): locking client mutex\n", __func__);    \
  pthread_mutex_lock(&clnt_dbus_mtx);

#define MMCLIENT_DBUS_UNLOCK()                                   \
  MMCLIENT_LOG_LOW("%s(): unlocking client mutex\n", __func__);  \
  pthread_mutex_unlock(&clnt_dbus_mtx);

/*============================================================================
  FUNCTION: mmclient_dbus_register_signals
============================================================================*/
/*!
@brief
  Helper function to register for signals
*/
/*==========================================================================*/
static int mmclient_dbus_register_signals(DBusError *error)
{
  char rule[MMCLIENT_DBUS_MAX_RULE_LEN] = "";
  int rc = MM_FAILURE;
  DBusError err;

  if (!error)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  err = *error;

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_NAD_STATUS_CHANGE_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_DATA_CALL_INFO_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SIM_AVAILABILITY_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SIM_PROPERTIES_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_NW_REG_PROPERTIES_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_NW_REG_SCAN_OPERATORS_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_BW_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_SERVER_DOWN_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_SERVER_READY_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_MODEM_OUT_OF_SERVICE_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_MODEM_IN_SERVICE_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  /* NameOwnerChanged signal is emitted whenever a process owns or loses ownership of a connection
     We can use this signal to detect when server process dies and cleanup state accordingly */
  snprintf(rule, sizeof(rule), "type='signal',interface='%s',member='NameOwnerChanged',"
                               "arg0='%s'",
           DBUS_INTERFACE_DBUS,
           MM_SERVER_NAME);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_DATA_SYSTEM_IND);

  dbus_bus_add_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  rc = MM_SUCCESS;

bail:
  return rc;
}


/*============================================================================
  FUNCTION: mmclient_dbus_unregister_signals
============================================================================*/
/*!
@brief
  Helper function to unregister for signals
*/
/*==========================================================================*/
static int mmclient_dbus_unregister_signals(DBusError *error)
{
  char rule[MMCLIENT_DBUS_MAX_RULE_LEN] = "";
  int rc = MM_FAILURE;
  DBusError err;

  if (!error)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  err = *error;

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_NAD_STATUS_CHANGE_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to remove match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_DATA_CALL_INFO_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SIM_AVAILABILITY_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SIM_PROPERTIES_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_NW_REG_PROPERTIES_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_NW_REG_SCAN_OPERATORS_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_BW_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_SERVER_DOWN_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_SERVER_READY_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_MODEM_OUT_OF_SERVICE_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_SYS_MODEM_IN_SERVICE_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  snprintf(rule, sizeof(rule), "type='signal',path='%s',interface='%s',member='%s'",
           MM_SERVER_OBJECT_PATH,
           MM_SERVER_INTERFACE_NAME,
           SIGNAL_DATA_SYSTEM_IND);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  /* NameOwnerChanged signal is emitted whenever a process owns or loses ownership of a connection
     We can use this signal to detect when server process dies and cleanup state accordingly */
  snprintf(rule, sizeof(rule), "type='signal',interface='%s',member='NameOwnerChanged',"
                               "arg0='%s'",
           DBUS_INTERFACE_DBUS,
           MM_SERVER_NAME);

  dbus_bus_remove_match(conn_state->conn, rule, error);
  MMCLIENT_DBUS_IS_ERROR_SET(err, "failed to add match");

  rc = MM_SUCCESS;

bail:
  return rc;
}

/*============================================================================
  FUNCTION: mmclient_dbus_dispatch_msg
============================================================================*/
/*!
@brief
  Function to dispatch the message after processing it within watch
*/
/*==========================================================================*/
static void mmclient_dbus_dispatch_msg(DBusConnection *conn)
{
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): invalid connection object!\n", __func__);
    goto bail;
  }

  /* Till there is data remaining on the DBus continue to dispatch */
  while (DBUS_DISPATCH_DATA_REMAINS == dbus_connection_get_dispatch_status(conn))
  {
    dbus_connection_dispatch(conn);
  }

  if (DBUS_DISPATCH_NEED_MEMORY == dbus_connection_get_dispatch_status(conn))
  {
    MMCLIENT_LOG_ERR("%s(): could not complete message dispatch due "
                     "to low memory!\n", __func__);
  }

bail:
  return;
}

/*============================================================================
  FUNCTION: mmclient_handle_nad_indications
============================================================================*/
/*!
@brief
  Helper method to process NAD signals
*/
/*==========================================================================*/
static int mmclient_handle_nad_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  int result = MM_FAILURE;
  nad_cb_info_t nad_ind_info;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  switch (hdr->msg_id)
  {
  case MSG_ID_NAD_STATUS_CHANGE_IND:
    {
      if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_decode_msg(hdr, args, (void*) &nad_ind_info,
                                                          (unsigned int) sizeof(nad_cb_info_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_NAD_STATUS_CHANGE_IND!\n", __func__);
        goto bail;
      }
    }

    break;

  default:
    MMCLIENT_LOG_ERR("%s(): unknown indication type [%d]\n", __func__, hdr->msg_id);
    goto bail;
  }

  if (MMCLIENT_SUCCESS != mmclient_process_nad_callback(&nad_ind_info))
  {
    MMCLIENT_LOG_ERR("%s(): callback invocation failed!\n", __func__);
    goto bail;
  }

  result = MM_SUCCESS;

bail:
  return result;
}

/*============================================================================
  FUNCTION: mmclient_handle_data_indications
============================================================================*/
/*!
@brief
  Helper method to process data signals
*/
/*==========================================================================*/
static int mmclient_handle_data_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  int                             result = MM_FAILURE;
  mmclient_call_info_param_info_t client_cb;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  memset(&client_cb, 0, sizeof(mmclient_call_info_param_info_t));

  switch (hdr->msg_id)
  {
    case MSG_ID_DATA_CALL_INFO_IND:
    {
      MMCLIENT_LOG_LOW("%s(): Handling MSG_ID_DATA_CALL_INFO_IND.\n", __func__);
      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &client_cb,
                                        (unsigned int) sizeof(mmclient_call_info_param_info_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_DATA_CALL_INFO_IND!\n", __func__);
        goto bail;
      }
    }

      break;

    default:
      MMCLIENT_LOG_ERR("%s(): unknown indication type [%d]\n", __func__, hdr->msg_id);
      goto bail;
  }

  if (MMCLIENT_SUCCESS != mmclient_data_evt_hdlr(client_cb.data_hndl, client_cb.evt))
  {
    MMCLIENT_LOG_ERR("%s(): callback invocation failed!\n", __func__);
    goto bail;
  }

  result = MM_SUCCESS;

bail:
  return result;
}

/*============================================================================
  FUNCTION: mmclient_handle_sim_indications
============================================================================*/
/*!
@brief
  Helper method to process SIM signals
*/
/*==========================================================================*/
static int mmclient_handle_sim_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  int                          result = MM_FAILURE;
  sim_cb_availability_params_t sim_availability_params;
  sim_cb_properties_params_t   sim_properties_params;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  switch (hdr->msg_id)
  {
  case MSG_ID_SIM_AVAILABILITY_IND:
    {
      MMCLIENT_LOG_LOW("%s(): Handling sim availability callback indication!\n", __func__);
      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &sim_availability_params,
                                        (unsigned int) sizeof(sim_cb_availability_params_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_SIM_AVAILABILITY_IND!\n", __func__);
        goto bail;
      }

      MMCLIENT_LOG_LOW("%s(): Decoded params: sim_hndl=%s  sim_availability=%d\n",
                       __func__,
                       sim_availability_params.sim_hndl,
                       sim_availability_params.sim_availability);

      if (MMCLIENT_SUCCESS
            != mmclient_sim_availability_hdlr(sim_availability_params.sim_hndl,
                                              sim_availability_params.sim_availability))
      {
        MMCLIENT_LOG_ERR("%s(): availability callback invocation failed!\n", __func__);
        goto bail;
      }
    }
    break;

  case MSG_ID_SIM_PROPERTIES_IND:
    {
      MMCLIENT_LOG_LOW("%s(): Handling sim properties callback indication!\n", __func__);
      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &sim_properties_params,
                                        (unsigned int) sizeof(sim_cb_properties_params_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_SIM_PROPERTIES_IND!\n", __func__);
        goto bail;
      }

      MMCLIENT_LOG_LOW("%s(): Decoded params: sim_hndl=%s  sim_availability=%d imsi:%s\n",
                       __func__,
                       sim_properties_params.sim_hndl,
                       sim_properties_params.sim_properties.availability,
                       sim_properties_params.sim_properties.imsi);

      if (MMCLIENT_SUCCESS
            != mmclient_sim_properties_hdlr(sim_properties_params.sim_hndl,
                                            sim_properties_params.sim_properties))
      {
        MMCLIENT_LOG_ERR("%s(): properties callback invocation failed!\n", __func__);
        goto bail;
      }
    }
    break;

  default:
    MMCLIENT_LOG_ERR("%s(): unknown indication type [%d]\n", __func__, hdr->msg_id);
    goto bail;
  }

  result = MM_SUCCESS;

bail:
  return result;
}

/*============================================================================
  FUNCTION: mmclient_handle_nw_reg_indications
============================================================================*/
/*!
@brief
  Helper method to process NAD signals
*/
/*==========================================================================*/
static int mmclient_handle_nw_reg_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  int                               result = MM_FAILURE;
  nw_reg_cb_properties_params_t     nw_reg_properties_params;
  nw_reg_cb_scan_operators_params_t nw_reg_scan_params;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  switch (hdr->msg_id)
  {
  case MSG_ID_NW_REG_PROPERTIES_IND:
    {
      MMCLIENT_LOG_LOW("%s(): Handling nw reg properties callback indication!\n", __func__);
      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &nw_reg_properties_params,
                                        (unsigned int) sizeof(nw_reg_cb_properties_params_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_NW_REG_PROPERTIES_IND!\n", __func__);
        goto bail;
      }

      MMCLIENT_LOG_LOW("%s(): Decoded params: nw_reg_hndl=%s status=%d mcc=%s\n",
                       __func__,
                       nw_reg_properties_params.nw_reg_hndl,
                       nw_reg_properties_params.nw_reg_properties.status,
                       nw_reg_properties_params.nw_reg_properties.mcc);

      if (MMCLIENT_SUCCESS
            != mmclient_nw_reg_properties_hdlr(nw_reg_properties_params.nw_reg_hndl,
                                               nw_reg_properties_params.nw_reg_properties))
      {
        MMCLIENT_LOG_ERR("%s(): properties callback invocation failed!\n", __func__);
        goto bail;
      }
    }
    break;

  case MSG_ID_NW_REG_SCAN_OPERATORS_IND:
    {
      MMCLIENT_LOG_LOW("%s(): Handling nw reg scan callback indication!\n", __func__);
      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &nw_reg_scan_params,
                                        (unsigned int) sizeof(nw_reg_cb_scan_operators_params_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_NW_REG_SCAN_OPERATORS_IND!\n", __func__);
        goto bail;
      }

      MMCLIENT_LOG_LOW("%s(): Decoded params: nw_reg_hndl=%s num_operators=%d\n",
                       __func__,
                       nw_reg_scan_params.nw_reg_hndl,
                       nw_reg_scan_params.nw_reg_scanned_operators.num_operators);

      if (MMCLIENT_SUCCESS
            != mmclient_nw_reg_scan_operators_hdlr(nw_reg_scan_params.nw_reg_hndl,
                                                   &(nw_reg_scan_params.nw_reg_scanned_operators)))
      {
        MMCLIENT_LOG_ERR("%s(): scan callback invocation failed!\n", __func__);
        goto bail;
      }
    }
    break;

  default:
    MMCLIENT_LOG_ERR("%s(): unknown indication type [%d]\n", __func__, hdr->msg_id);
    goto bail;
  }

  result = MM_SUCCESS;

bail:
  return result;
}

/*============================================================================
  FUNCTION: mmclient_handle_bw_reg_indications
============================================================================*/
/*!
@brief
  Helper method to process BW signals
*/
/*==========================================================================*/
static int mmclient_handle_bw_reg_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  int                         result = MM_FAILURE;
  mmclient_bw_ind_container_t client_cb;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  switch (hdr->msg_id)
  {
    case MSG_ID_BANDWIDTH_THROUGHPUT_IND:
    {
      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &client_cb,
                                        (unsigned int) sizeof(mmclient_bw_ind_container_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_BANDWIDTH_THROUGHPUT_IND!\n", __func__);
        goto bail;
      }
    }

    break; /* MSG_ID_BANDWIDTH_THROUGHPUT_IND */

    default:
      MMCLIENT_LOG_ERR("%s(): unknown indication type [%d]\n", __func__, hdr->msg_id);
      goto bail;
  }

  if (MMCLIENT_SUCCESS != mmclient_bandwidth_ind_hdlr(&client_cb))
  {
    MMCLIENT_LOG_ERR("%s(): callback invocation failed!\n", __func__);
    goto bail;
  }

  result = MM_SUCCESS;

bail:
  return result;
}

/*============================================================================
  FUNCTION: mmclient_handle_mdmmgr_indications
============================================================================*/
/*!
@brief
  Helper method to process mdmmgr signals
*/
/*==========================================================================*/
static int mmclient_handle_mdmmgr_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  int                     result = MM_FAILURE;
  mmclient_sys_ind_info_t sys_ind_info;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  if (MMCLIENT_MSG_NO_ERR
        != mmclient_dbus_decode_msg(hdr, args, (void*) &sys_ind_info,
                                    (unsigned int) sizeof(mmclient_sys_ind_info_t)))
  {
    MMCLIENT_LOG_ERR("%s(): failed to decode the message!\n", __func__);
    goto bail;
  }

  switch (sys_ind_info.ind_type)
  {
  case MMCLIENT_IND_TYPE_SERVER:
    {
      if (MMCLIENT_SERVER_DOWN == sys_ind_info.data.server_status)
      {
        MMCLIENT_LOG_ERR("%s(): server is DOWN!\n", __func__);

        /* Server is down, cleanup all the internal handles */
        mmclient_cleanup_state();
        conn_state->client_id = MMCLIENT_INVALID_CLIENT_ID;
      }
      else if (MMCLIENT_SERVER_READY == sys_ind_info.data.server_status)
      {
        MMCLIENT_LOG_MED("%s(): server is READY!\n", __func__);
      }
    }

    break; /* MMCLIENT_IND_TYPE_SERVER */

  case MMCLIENT_IND_TYPE_SYSTEM:
    {
      if (MMCLIENT_SYS_IND_OOS == sys_ind_info.data.system_status.sys_ind)
      {
        /* Cleanup all internal state linked with the NAD going down */
        MMCLIENT_LOG_ERR("%s(): NAD [%d] is down!\n",
                         __func__, sys_ind_info.data.system_status.nad_inst);

        mmclient_data_cleanup_state_for_nad_inst(sys_ind_info.data.system_status.nad_inst);
        mmclient_nw_reg_cleanup_state_for_nad_inst(sys_ind_info.data.system_status.nad_inst);
        mmclient_sim_cleanup_state_for_nad_inst(sys_ind_info.data.system_status.nad_inst);
        mmclient_bandwidth_cleanup_state_for_nad_inst(sys_ind_info.data.system_status.nad_inst);
        mmclient_dsd_cleanup_state_for_nad_inst(sys_ind_info.data.system_status.nad_inst);
        mmclient_nad_cleanup_state_for_nad_inst(sys_ind_info.data.system_status.nad_inst);
      }
      else if (MMCLIENT_SYS_IND_IS == sys_ind_info.data.system_status.sys_ind)
      {
        MMCLIENT_LOG_MED("%s(): NAD [%d] is up!\n", __func__,
                          sys_ind_info.data.system_status.nad_inst);
      }
    }

    break; /* MMCLIENT_IND_TYPE_SYSTEM */
  }

  mmclient_process_sys_indications(&sys_ind_info);

  result = MM_SUCCESS;

bail:
  return result;
}

/*============================================================================
  FUNCTION: mmclient_handle_dsd_indications
============================================================================*/
/*!
@brief
  Helper method to process DSD signals
*/
/*==========================================================================*/
static int mmclient_handle_dsd_indications
(
  mmclient_msg_hdr_t *hdr,
  DBusMessageIter    *args
)
{
  mmclient_data_sys_info_param_t client_cb;

  if (!hdr || !args)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return MM_FAILURE;
  }

  switch (hdr->msg_id)
  {
    case MSG_ID_DATA_SYSTEM_IND:

      if (MMCLIENT_MSG_NO_ERR
            != mmclient_dbus_decode_msg(hdr, args, (void*) &client_cb,
                                        (unsigned int) sizeof(mmclient_data_sys_info_param_t)))
      {
        MMCLIENT_LOG_ERR("%s(): failed to decode the message"
                         " for MSG_ID_DATA_SYSTEM_IND!\n", __func__);
        return MM_FAILURE;
      }
      break;

    default:
      MMCLIENT_LOG_ERR("%s(): unknown indication type [%d]\n", __func__, hdr->msg_id);
      return MM_FAILURE;
  }

  if (MMCLIENT_SUCCESS != mmclient_dsd_ind_hdlr(&client_cb))
  {
    MMCLIENT_LOG_ERR("%s(): callback invocation failed!\n", __func__);
    return MM_FAILURE;
  }

  return MM_SUCCESS;
}

/*============================================================================
  FUNCTION: mmclient_dbus_msg_filter
============================================================================*/
/*!
@brief
  Filter function registered with DBus to receive incoming signal requests.
*/
/*==========================================================================*/
static DBusHandlerResult mmclient_dbus_msg_filter
(
  DBusConnection *conn,
  DBusMessage    *message,
  void           *data
)
{
  DBusHandlerResult       result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  int                     msg_type = 0;
  const char              *signal_name;
  const char              *msg_interface;
  char                    *name = NULL;
  char                    *prev_owner = NULL;
  char                    *new_owner = NULL;
  mmclient_msg_hdr_t      *signal_hdr = NULL;
  mmclient_sys_ind_info_t sys_ind_info;
  DBusMessageIter         signal_iter;
  DBusError               err;

  if (!conn || !message)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return result;
  }

  MMCLIENT_DBUS_LOCK();

  VAR_UNUSED(data);

  msg_type = dbus_message_get_type(message);
  signal_name = dbus_message_get_member(message);
  msg_interface = dbus_message_get_interface(message);

  dbus_error_init(&err);

  if (DBUS_MESSAGE_TYPE_SIGNAL == msg_type)
  {
    MMCLIENT_LOG_LOW("%s(): Dbus Signal received: msg_type=[%d] signal_name=[%s] "
                     "msg_iface=[%s]\n", __func__, msg_type, signal_name, msg_interface);

    if (!strcmp(signal_name, "NameAcquired"))
    {
      /* As of now we don't do anything with this signal, ignore it */
      goto bail;
    }

    if (!strcmp(signal_name, "NameOwnerChanged"))
    {
      if (!dbus_message_get_args(message, &err,
                                 DBUS_TYPE_STRING, &name,
                                 DBUS_TYPE_STRING, &prev_owner,
                                 DBUS_TYPE_STRING, &new_owner,
                                 DBUS_TYPE_INVALID))
      {
        if (dbus_error_is_set(&err))
        {
          MMCLIENT_LOG_ERR("%s(): Failed to parse NameOwnerChanged signal!"
                           " Err name: %s, Err msg: %s",
                           __func__, err.name, err.message);
        }

        goto bail;
      }

      if (strcmp(name, MM_SERVER_NAME) != 0)
      {
        /* Ignore signal if not from modem manager daemon */
        goto bail;
      }

      if (prev_owner && new_owner)
      {
        memset(&sys_ind_info, 0, sizeof(mmclient_sys_ind_info_t));
        sys_ind_info.ind_type = MMCLIENT_IND_TYPE_SERVER;

        if (strlen(prev_owner) == 0 && strlen(new_owner) > 0)
        {
          MMCLIENT_LOG_MED("%s(): server is UP!\n", __func__);

          sys_ind_info.data.server_status = MMCLIENT_SERVER_READY;
        }
        else if (strlen(prev_owner) > 0 && strlen(new_owner) == 0)
        {
          MMCLIENT_LOG_ERR("%s(): server is DOWN!\n", __func__);

          /* Cleanup state */
          MMCLIENT_LOG_LOW("%s(): cleanup state...\n", __func__);
          mmclient_cleanup_state();

          /* Reset client id value */
          MMCLIENT_LOG_LOW("%s(): reset client id...\n", __func__);
          conn_state->client_id = MMCLIENT_INVALID_CLIENT_ID;

          sys_ind_info.data.server_status = MMCLIENT_SERVER_DOWN;
        }

        mmclient_process_sys_indications(&sys_ind_info);
      }
    }

    if (!strcmp(signal_name, "Disconnected"))
    {
      MMCLIENT_LOG_ERR("%s(): disconnected from DBus! Interface: %s\n", __func__, msg_interface);
      dbus_connection_set_exit_on_disconnect(conn_state->conn, FALSE);

      connection_active = dbus_connection_get_is_connected(conn_state->conn);
      conn_state->connection_status = STATE_DISCONNECTED;

      goto bail;
    }

    /* Initialize the iterator for parsing the command result */
    if (!dbus_message_iter_init(message, &signal_iter))
    {
      MMCLIENT_LOG_ERR("%s(): failed to initialize iterator for signal!\n", __func__);
      goto bail;
    }

    /* Decode the header to check what message it is */
    signal_hdr = mmclient_dbus_decode_hdr(message, &signal_iter);
    if (!signal_hdr)
    {
      MMCLIENT_LOG_ERR("%s(): failed to decode error!\n", __func__);
      goto bail;
    }

    MMCLIENT_LOG_LOW("\n%s(): srvc_id [%d], msg_id [%d], client_id [%d]\n",
                     __func__, signal_hdr->srvc_id, signal_hdr->msg_id, signal_hdr->client_id);

    switch (signal_hdr->srvc_id)
    {
    case SRVC_ID_NAD:
      {
        if (MM_SUCCESS != mmclient_handle_nad_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to pass NAD signal!\n", __func__);
          break;
        }
      }

      break; /* SRVC_ID_NAD */

    case SRVC_ID_DATA_CALL:
      {
        if (MM_SUCCESS != mmclient_handle_data_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to pass DATA signal!\n", __func__);
          break;
        }
      }

      break; /* SRVC_ID_DATA_CALL */

    case SRVC_ID_SIM:
      {
        if (MM_SUCCESS != mmclient_handle_sim_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to pass SIM signal!\n", __func__);
          break;
        }
      }

      break; /* SRVC_ID_SIM */

    case SRVC_ID_NW_REG:
      {
        if (MM_SUCCESS != mmclient_handle_nw_reg_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to pass Nw Reg signal!\n", __func__);
          break;
        }
      }

      break; /* SRVC_ID_NW_REG */

    case SRVC_ID_BW_REG:
      { 
        if (MM_SUCCESS != mmclient_handle_bw_reg_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to pass BW signal!\n", __func__);
          break;
        }
      }

      break; /* case SRVC_ID_BW_REG */

    case SRVC_ID_DATA_SYSTEM:
      {
        if (MM_SUCCESS != mmclient_handle_dsd_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to pass DSD signal!\n", __func__);
          break;
        }

      }

      break; /* case SRVC_ID_DATA_SYSTEM */

    case SRVC_ID_MDMMGR:
      {
        if (MM_SUCCESS != mmclient_handle_mdmmgr_indications(signal_hdr, &signal_iter))
        {
          MMCLIENT_LOG_ERR("%s(): failed to process sys signal!\n", __func__);
          break;
        }
      }

      break; /* SRVC_ID_MDMMGR */
    }

    dbus_error_free(&err);
    mmclient_free_msg_hdr(signal_hdr);

    result = DBUS_HANDLER_RESULT_HANDLED;
  }

bail:
  MMCLIENT_DBUS_UNLOCK();
  return result;
}

/*============================================================================
  FUNCTION: mmclient_dbus_add_watch
============================================================================*/
/*!
@brief
  Add watch function
*/
/*==========================================================================*/
static dbus_bool_t mmclient_dbus_add_watch(DBusWatch *watch, void *data)
{
  int fd = -1;
  uint8_t flags = 0;
  dbus_bool_t ret = FALSE;

  VAR_UNUSED(data);

  if (!watch)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  if (FALSE == dbus_watch_get_enabled(watch))
  {
    MMCLIENT_LOG_ERR("%s(): watch is disabled already!\n", __func__);
    ret = TRUE;
    goto bail;
  }

  if (MAX_WATCH_LIST_LEN == wlist_size)
  {
    MMCLIENT_LOG_ERR("%s(): reached maximum internal capacity to"
                     " add watches!\n", __func__);
    goto bail;
  }

  fd = dbus_watch_get_unix_fd(watch);
  flags = dbus_watch_get_flags(watch);

  if (flags & DBUS_WATCH_READABLE)
  {
    FD_SET(fd, &read_fds);
    if (max_fd <= fd)
    {
      max_fd = fd;
    }

    /* Add the watch fd to the list */
    watch_list[wlist_size].watch = watch;
    watch_list[wlist_size].watch_fd = fd;
    wlist_size++;

    MMCLIENT_LOG_LOW("%s(): adding readable watch [%p], fd [%d], wlist_size [%d]\n",
                     __func__, (void*) watch, fd, wlist_size);
  }

  ret = TRUE;

bail:
  return ret;
}

/*============================================================================
  FUNCTION: mmclient_dbus_remove_watch
============================================================================*/
/*!
@brief
  Remove watch function
*/
/*==========================================================================*/
static void mmclient_dbus_remove_watch(DBusWatch *watch, void *data)
{
  int fd = -1;
  int i = 0;
  uint8_t flags = 0;

  VAR_UNUSED(data);

  if (!watch)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  if (0 == wlist_size)
  {
    MMCLIENT_LOG_ERR("%s(): watch fd list is already empty\n", __func__);
  }

  fd = dbus_watch_get_unix_fd(watch);
  flags = dbus_watch_get_flags(watch);
  for (i = 0; i < wlist_size; i++)
  {
    if ((flags & DBUS_WATCH_READABLE) && (watch_list[i].watch_fd == fd))
    {
      MMCLIENT_LOG_LOW("%s(): removing fd [%d] for watch [%p]\n",
                       __func__, fd, (void*) watch);
      /* Clear the fd from the read_fds set */
      FD_CLR(fd, &read_fds);

      /* Remove the entry from the watch_list */
      watch_list[i].watch = NULL;
      watch_list[i].watch_fd = -1;

      /* Decrement the size */
      wlist_size--;

      break;
    }
  }

bail:
  return;
}

/*============================================================================
  FUNCTION: mmclient_dbus_toggle_watch
============================================================================*/
/*!
@brief
  Toggle watch function
*/
/*==========================================================================*/
static void mmclient_dbus_toggle_watch(DBusWatch *watch, void *data)
{
  VAR_UNUSED(data);

  if (!watch)
  {
    MMCLIENT_LOG_ERR("%s(): invalid param!\n", __func__);
    goto bail;
  }

  if (dbus_watch_get_enabled(watch))
  {
    mmclient_dbus_add_watch(watch, data);
  }
  else
  {
    mmclient_dbus_remove_watch(watch, data);
  }

bail:
  return;
}

/*============================================================================
  FUNCTION: mmclient_dbus_process_watches
============================================================================*/
/*!
@brief
  select-wait function for DBusWatch objects
*/
/*==========================================================================*/
static int mmclient_dbus_process_watches(DBusConnection *conn)
{
  int ret;
  int fd = -1;
  int i = 0;
  int rc = MM_FAILURE;

  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): invalid connection!\n", __func__);
    goto bail;
  }

  /* Apart from the watch fds add the read fd of the pipe created at powerup
     to the read_fds set To gracefully terminate the signal thread the main
     thread will write one byte to this pipe fd which will unblock select
     and we can exit the function */
  FD_SET(signal_thrd_fd[0], &read_fds);

  if (max_fd <= signal_thrd_fd[0])
  {
    max_fd = signal_thrd_fd[0];
  }

  MMCLIENT_LOG_LOW("%s(): waiting for signal...\n", __func__);

  ret = select(max_fd + 1, &read_fds, NULL, NULL, NULL);
  if (-1 == ret)
  {
    MMCLIENT_LOG_ERR("%s(): select was interrupted. Possibly the underlying "
                     "fd has been closed!",
                     __func__);
    goto bail;
  }

  if (ret > 0)
  {
    if (FD_ISSET(signal_thrd_fd[0], &read_fds))
    {
      MMCLIENT_LOG_MED("%s(): received message on the read pipe fd [%d]! "
                       "Exiting the loop\n",
                       __func__, signal_thrd_fd[0]);
      goto bail;
    }

    /* Check if any of the watch fds are ready */
    for (i = 0; i < wlist_size; i++)
    {
      fd = watch_list[i].watch_fd;

      if (FD_ISSET(fd, &read_fds))
      {
        MMCLIENT_LOG_LOW("%s(): select unblocked on fd [%d], "
                         "dispatch message\n", __func__, fd);

        /* Handle the watch, may result in the watch getting freed */
        dbus_watch_handle(watch_list[i].watch, DBUS_WATCH_READABLE);

        mmclient_dbus_dispatch_msg(conn);
      }
    }
  }

  rc = MM_SUCCESS;

bail:
  return rc;
}

/*============================================================================
  FUNCTION: mmclient_dbus_add_timeout
============================================================================*/
/*!
@brief
  Add watch function
*/
/*==========================================================================*/
static dbus_bool_t mmclient_dbus_add_timeout(DBusTimeout *timeout, void *data)
{
  dbus_bool_t ret = FALSE;
  struct clnt_timeout_list_t *t_node;

  VAR_UNUSED(data);

  if (!timeout)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  if (FALSE == dbus_timeout_get_enabled(timeout))
  {
    MMCLIENT_LOG_ERR("%s(): timeout is disabled!\n", __func__);
    ret = TRUE;
    goto bail;
  }

  t_node = (struct clnt_timeout_list_t*) malloc(sizeof(struct clnt_timeout_list_t));
  if (!t_node)
  {
    MMCLIENT_LOG_ERR("%s(): could not allocate memory for timeout node!\n", __func__);
    goto bail;
  }

  t_node->timeout = timeout;

  if (tlist == NULL)
  {
    t_node->next = NULL;
  }
  else
  {
    t_node->next = tlist;
  }

  tlist = t_node;

  MMCLIENT_LOG_LOW("%s(): added timeout [%p], interval [%d]\n",
                   __func__, (void*) timeout, dbus_timeout_get_interval(timeout));

  ret = TRUE;

bail:
  return ret;
}

/*============================================================================
  FUNCTION: mmclient_dbus_remove_timeout
============================================================================*/
/*!
@brief
  Remove timeout
*/
/*==========================================================================*/
static void mmclient_dbus_remove_timeout(DBusTimeout *timeout, void* data)
{

  struct clnt_timeout_list_t *pre = NULL;
  struct clnt_timeout_list_t *curr = NULL;

  VAR_UNUSED(data);

  curr = tlist;
  while (curr != NULL)
  {
    if (curr->timeout == timeout)
    {
      if (pre == NULL)
      {
        tlist = curr->next;
      }
      else
      {
        pre->next = curr->next;
      }

      MMCLIENT_LOG_LOW("%s(): removing timeout [%p]!\n", __func__, (void*) timeout);
      curr->timeout = NULL;
      free(curr);
      curr = NULL;
      break;
    }

    pre = curr;
    curr = curr->next;
  }
}

/*============================================================================
  FUNCTION: mmclient_dbus_toggle_timeout
============================================================================*/
/*!
@brief
  Toggle timeout function
*/
/*==========================================================================*/
static void mmclient_dbus_toggle_timeout(DBusTimeout *timeout, void *data)
{
  if (!timeout)
  {
    MMCLIENT_LOG_ERR("%s(): invalid param!\n", __func__);
    goto bail;
  }

  if (dbus_timeout_get_enabled(timeout))
  {
    mmclient_dbus_add_timeout(timeout, data);
  }
  else
  {
    mmclient_dbus_remove_timeout(timeout, data);
  }

bail:
  return;
}

/*============================================================================
  FUNCTION: mmclient_dbus_clear_watch_list
============================================================================*/
/*!
@brief
  Clears watch list
*/
/*==========================================================================*/
void mmclient_dbus_clear_watch_list(void)
{
  int i = 0;

  for (i = 0; i < MAX_WATCH_LIST_LEN; i++)
  {
    watch_list[i].watch = NULL;
    watch_list[i].watch_fd = -1;
  }
}

/*============================================================================
  FUNCTION: mmclient_dbus_signal_hdlr
============================================================================*/
/*!
@brief
  Thread function which listens for signals

@note
  Dependencies
    - Client DBus connection should have already been setup
    - Client connection state should be valid
    - Main thread should have created the signal_thr_fd pipe
*/
/*==========================================================================*/
static void *mmclient_dbus_signal_hdlr(void* args)
{
  dbus_bool_t ret = 0;
  DBusError error;

  (void) args;

  dbus_error_init(&error);

  if (!conn_state->conn)
  {
    MMCLIENT_LOG_ERR("%s(): Bad connection object!!\n", __func__);
    goto bail;
  }

  /* Register for signals sent from modem manager */
  if (MM_SUCCESS != mmclient_dbus_register_signals(&error))
  {
    MMCLIENT_LOG_ERR("%s(): failed to register for signals!\n", __func__);
    goto bail;
  }

  ret = dbus_connection_add_filter(conn_state->conn,           /* Client DBus connection object*/
                                   mmclient_dbus_msg_filter,   /* Signal handler function */
                                   NULL,                       /* userdata - not used currently */
                                   NULL);                      /* Signal handler free function
                                                                  not used currently */
  if (FALSE == ret)
  {
    MMCLIENT_LOG_ERR("%s(): failed to add filter function!\n", __func__);
    goto bail;
  }

  /* We are only interested in reading from the fd */
  FD_ZERO(&read_fds);

  dbus_connection_set_exit_on_disconnect(conn_state->conn, FALSE);

  mmclient_dbus_clear_watch_list();

  /* Set watch and timeout functions */
  ret = dbus_connection_set_watch_functions(conn_state->conn,
                                            mmclient_dbus_add_watch,
                                            mmclient_dbus_remove_watch,
                                            mmclient_dbus_toggle_watch,
                                            NULL, NULL);
  if (FALSE == ret)
  {
    MMCLIENT_LOG_ERR("%s(): failed to set watch functions!\n", __func__);
    goto bail;
  }

  ret = dbus_connection_set_timeout_functions(conn_state->conn,
                                              mmclient_dbus_add_timeout,
                                              mmclient_dbus_remove_timeout,
                                              mmclient_dbus_toggle_timeout,
                                              NULL, NULL);
  if (FALSE == ret)
  {
    MMCLIENT_LOG_ERR("%s(): failed to set timeout functions!\n", __func__);
    goto bail;
  }

  /* Run main loop. The messages will be received and processed in the following order
     1. Block on select()
     2. When a signal arrives select will get unblocked. Mark the watch as 'handled'
     3. Dispatch the message to the registered filter function
     4. Receive the response in the filter function and process it
     5. Set RESULT_HANDLED flag if handled successfully
     6. Go back to wait-select loop */

  while (TRUE == dbus_connection_get_is_connected(conn_state->conn))
  {
    ret = mmclient_dbus_process_watches(conn_state->conn);
    if (MM_SUCCESS != ret)
    {
      MMCLIENT_LOG_ERR("%s(): client loop was interrupted...exiting thread\n", __func__);
      break;
    }
  }

bail:
  if (TRUE == dbus_connection_get_is_connected(conn_state->conn))
  {
    /* Cleanup */
    MMCLIENT_LOG_MED("%s(): cleaning up state...\n", __func__);
    ret = dbus_connection_set_watch_functions(conn_state->conn, NULL, NULL, NULL, NULL, NULL);
    if (!ret)
    {
      MMCLIENT_LOG_ERR("%s(): failed to set watch functions!\n", __func__);
    }

    ret = dbus_connection_set_timeout_functions(conn_state->conn, NULL, NULL, NULL, NULL, NULL);
    if (!ret)
    {
      MMCLIENT_LOG_ERR("%s(): failed to set timeout functions!\n", __func__);
    }

    /* Remove the connection filter */
    dbus_connection_remove_filter(conn_state->conn, mmclient_dbus_msg_filter, NULL);
    MMCLIENT_DBUS_IS_ERROR_SET(error, "Failed to unregister "
                                      "filter function from DBus");

    if (MM_SUCCESS != mmclient_dbus_unregister_signals(&error))
    {
      MMCLIENT_LOG_ERR("%s(): failed to unregister signals!\n", __func__);
    }

    FD_ZERO(&read_fds);
    mmclient_dbus_clear_watch_list();

    /* If the loop was not terminated as a result of "Disconnected" signal
     we can perform some cleanup*/
    (void) dbus_bus_release_name(conn_state->conn, MMCLIENT_DBUS_NAME, NULL);

    dbus_connection_close(conn_state->conn);
    dbus_connection_unref(conn_state->conn);
    conn_state->conn = NULL;
    free(conn_state);
    conn_state = NULL;
  }

  /* We are done with error object by this point, free it */
  dbus_error_free(&error);
  return NULL;
}

/*============================================================================
                             GLOBAL DEFINITIONS
============================================================================*/
/*============================================================================
  FUNCTION: mmclient_get_connection_state
============================================================================*/
/*!
@brief
  Returns the DBus connection state object

@return
  mmclient_dbus_state_t reference
*/
/*===========================================================================*/
mmclient_dbus_state_t* mmclient_get_connection_state(void)
{
  /* Check if the connection was previously terminated */
  if (conn_state && conn_state->connection_status == STATE_DISCONNECTED)
  {
    MMCLIENT_LOG_ERR("%s(): the previously setup connection was "
                     "disconnected....\n", __func__);

    /* Join the signal thread to reclaim resouces */
    MMCLIENT_LOG_LOW("%s(): waiting for old signal thread to "
                     "terminate...\n", __func__);
    pthread_join(signal_thread, NULL);

    /* The connection is already closed so we just need to unreference
       the previous connection */
    dbus_connection_unref(conn_state->conn);

    conn_state->conn = NULL;
    free(conn_state);
    conn_state = NULL;

    mmclient_dbus_deinit();
  }

  if (!dbus_inited)
  {
    MMCLIENT_LOG_ERR("%s(): connection is not yet initialized!", __func__);
    return NULL;
  }

  return conn_state;
}

/*============================================================================
  FUNCTION: mmclient_dbus_init
============================================================================*/
/*!
@brief
  Initialize DBus connection

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*===========================================================================*/
int mmclient_dbus_init(void)
{
  int rc = MM_FAILURE;
  int reti = 0;
  pid_t proc_pid;
  char proc_name[MAX_PROC_NAMELEN] = "";
  char client_dbus_name[MMCLIENT_MAX_DBUS_STR_LEN] = "";
  size_t bytes = 0;
  int    cpy_bytes = 0;
  DBusError error;

  conn_state = (mmclient_dbus_state_t*) malloc(sizeof(mmclient_dbus_state_t));
  if (!conn_state)
  {
    MMCLIENT_LOG_ERR("%s(): failed to initialize connection state!\n", __func__);
    goto bail;
  }

  memset(conn_state, 0, sizeof(mmclient_dbus_state_t));

  dbus_error_init(&error);

  /* Obtain a DBusConnection reference */
  conn_state->conn = dbus_bus_get_private(DBUS_BUS_SYSTEM, &error);
  if (dbus_error_is_set(&error))
  {
    MMCLIENT_LOG_ERR("%s(): connection error! Err: %s\n", __func__, error.message);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): obtained connection reference to DBus session [%p]\n",
                   __func__, (void*) conn_state->conn);

  memset(proc_name, 0, MAX_PROC_NAMELEN);
  memset(client_dbus_name, 0, MMCLIENT_MAX_DBUS_STR_LEN);

  proc_pid = getpid();
  mmclient_util_get_proc_name(proc_pid, proc_name, MAX_PROC_NAMELEN);
  bytes = snprintf(client_dbus_name, sizeof(client_dbus_name), "%s.%s_%d",
                   MMCLIENT_DBUS_NAME, proc_name, proc_pid);
  if (bytes >= MMCLIENT_MAX_DBUS_STR_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): bus name was truncated! Able to copy only [%s]\n",
                     __func__, client_dbus_name);
  }

  /* Request a unique bus name from DBus session */
  MMCLIENT_LOG_LOW("%s(): requesting for bus name: %s\n", __func__, client_dbus_name);
  reti = dbus_bus_request_name(conn_state->conn,
                               client_dbus_name,
                               DBUS_NAME_FLAG_REPLACE_EXISTING,
                               &error);
  if (dbus_error_is_set(&error))
  {
    MMCLIENT_LOG_ERR("%s(): failed to get unique bus name! Err: %s\n", __func__, error.message);
    goto bail;
  }

  cpy_bytes = snprintf(conn_state->conn_name, MMCLIENT_MAX_DBUS_STR_LEN, "%s", client_dbus_name);
  if (cpy_bytes >= MMCLIENT_MAX_DBUS_STR_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  cpy_bytes = snprintf(conn_state->path, MMCLIENT_MAX_DBUS_STR_LEN, "%s",
                       MMCLIENT_DBUS_OBJECT_PATH);
  if (cpy_bytes >= MMCLIENT_MAX_DBUS_STR_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  conn_state->connection_status = STATE_CONNECTED;

  MMCLIENT_LOG_LOW("%s(): obtained unique name [%s] for connection [%p]\n",
                   __func__, conn_state->conn_name, (void *) conn_state->conn);

  /* Create a pipe */
  reti = pipe(signal_thrd_fd);
  if (reti != 0)
  {
    MMCLIENT_LOG_ERR("%s(): pipe failed! Err: %s\n", __func__, strerror(errno));
    goto bail;
  }

  if (0 != pthread_create(&signal_thread, NULL, mmclient_dbus_signal_hdlr, NULL))
  {
    MMCLIENT_LOG_ERR("%s(): failed to start signal handler thread!\n", __func__);
    goto bail;
  }

  conn_state->client_id = MMCLIENT_INVALID_CLIENT_ID;

  /* Ensures DBus performs some level of thread locking on its data structures */
  dbus_threads_init_default();

  /* DBus state is inited */
  dbus_inited = TRUE;

  /* Flag to indicate that the connection object to DBus is valid and in connected state */
  connection_active = TRUE;

  rc = MM_SUCCESS;

bail:
  dbus_error_free(&error);
  return rc;
}

/*============================================================================
  FUNCTION: mmclient_dbus_deinit
============================================================================*/
/*!
@brief
  De-initialize DBus connection

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*===========================================================================*/
int mmclient_dbus_deinit()
{
  int rc = MM_FAILURE;
  char ch = 'q';
  ssize_t wrt;

  if (dbus_inited == FALSE)
  {
    MMCLIENT_LOG_MED("%s(): already de-inited\n", __func__);
    rc = MM_SUCCESS;
    return rc;
  }

  MMCLIENT_LOG_MED("%s(): entry\n", __func__);

  /* The connection_active flag will be set to FALSE when we receive a
     "Disconnected" signal on the connection. In that case we would have
     exited the signal thread. If the connection was not forcefully
     terminated we need to manually unblock the signal thread and
     un-initialize the state */
  if (connection_active)
  {
    /* Write something on the pipe fd to unblock select in the signal
       thread and exit */
    wrt = write(signal_thrd_fd[1], &ch, 1);
    if (wrt == -1)
    {
      MMCLIENT_LOG_ERR("%s(): failed to write! Err: %s\n", __func__, strerror(errno));
    }

    pthread_join(signal_thread, NULL);

    close(signal_thrd_fd[0]);
    close(signal_thrd_fd[1]);
  }

  dbus_inited = FALSE;

  rc = MM_SUCCESS;

  MMCLIENT_LOG_MED("%s(): exit\n", __func__);
  return rc;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
