/******************************************************************************

             M M C L I E N T _ N W _ R E G _ H D L R . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <pthread.h>

#include "mmclient_util.h"
#include "mmclient_msg.h"
#include "mmclient_dbus.h"
#define VAR_UNUSED(X) (void)(X)

typedef uint8_t boolean;
#define MM_FALSE 0
#define MM_TRUE 1

#define MMCLIENT_MAX_NW_REG_CLIENT  (10)

typedef struct
{
  boolean                      valid;
  mmclient_nw_reg_hndl_t       st_hndl;
  mmclient_nw_reg_ind_cb_fn_t  mmclient_cb_func;
  void                         *user_data;
  mmclient_nw_reg_scan_cb_fn_t mmclient_scan_cb_func;
  void                         *scan_user_data;
  char                         nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nad_inst_t          nad_inst;
} mmclient_nw_reg_client_info_t;

static mmclient_nw_reg_client_info_t mm_nw_reg_client_info[MMCLIENT_MAX_NW_REG_CLIENT];

pthread_mutex_t clnt_nw_reg_list_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_NW_REG_LIST_MUTEX_LOCK()                          \
  MMCLIENT_LOG_LOW("%s(): locking NW REG list mutex\n", __func__); \
  pthread_mutex_lock(&clnt_nw_reg_list_mtx);

#define MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK()                          \
  MMCLIENT_LOG_LOW("%s(): unlocking NW REG list mutex\n", __func__); \
  pthread_mutex_unlock(&clnt_nw_reg_list_mtx);

pthread_mutex_t mmclient_nw_reg_global_mutex = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_NW_REG_GLOBAL_LOCK()                                  \
  MMCLIENT_LOG_LOW("%s(): locking NW REG global mutex\n", __func__);   \
  pthread_mutex_lock(&mmclient_nw_reg_global_mutex);

#define MMCLIENT_NW_REG_GLOBAL_UNLOCK()                                \
  MMCLIENT_LOG_LOW("%s(): unlocking NW REG global mutex\n", __func__); \
  pthread_mutex_unlock(&mmclient_nw_reg_global_mutex);

extern mmclient_nad_inst_t mmclient_get_nad_inst_for_nad_hndl
(
  mmclient_hndl_t *nad_hndl
);

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_srvc_hndl
===========================================================================*/
/*!
@brief
  Used to get network registration service handle. This handle can be used
  by the caller to exercise network service related functionalities.

@return
  mmclient_nw_reg_hndl_t reference

@arg nad_hndl - Data call handle
@arg user_data - any information which the caller wants to pass along

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_nw_reg_hndl_t mmclient_nw_reg_get_srvc_hndl
(
  mmclient_nad_hndl_t  nad_hndl,
  void                 *user_data
)
{
  int                   i = 0;
  int                   bytes = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  char                  nw_reg_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  MMCLIENT_LOG_LOW("%s(): Running mmclient_nw_reg_get_srvc_hndl\n", __func__);

  if(!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
     if(!mm_nw_reg_client_info[i].valid)
       break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    /*Error. No more NW Reg handles*/
    MMCLIENT_LOG_ERR("%s(): No more handles available!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_GET_SRVC_HNDL_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_GET_SRVC_HNDL_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) nad_hndl,
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) &nw_reg_hndl,
                                    MMCLIENT_MAX_SRVC_HNDL_LEN);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_NW_REG_LIST_MUTEX_LOCK();

    bytes = snprintf(mm_nw_reg_client_info[i].nw_reg_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
             nw_reg_hndl);
    if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
    {
      MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();
      goto bail;
    }

    mm_nw_reg_client_info[i].valid = MM_TRUE;
    mm_nw_reg_client_info[i].user_data = user_data;
    mm_nw_reg_client_info[i].st_hndl
        = (mmclient_nw_reg_hndl_t) mm_nw_reg_client_info[i].nw_reg_hndl;
    mm_nw_reg_client_info[i].nad_inst = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);

    MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();
 
    MMCLIENT_LOG_MED("%s(): Returning store handle %p for NW_REG handle %s index %d!\n",
                     __func__, mm_nw_reg_client_info[i].st_hndl,
                     mm_nw_reg_client_info[i].nw_reg_hndl, i);

    mmclient_free_msg_hdr(msg_hdr);
    MMCLIENT_NW_REG_GLOBAL_UNLOCK();
    return mm_nw_reg_client_info[i].st_hndl;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }
 
bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return NULL;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_release_srvc_hndl
===========================================================================*/
/*!
@brief
  Release the network service handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_release_srvc_hndl
(
  mmclient_nw_reg_hndl_t nw_reg_hndl
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
     if(mm_nw_reg_client_info[i].valid &&
        mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
       break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    /*Error. No matching NW Reg handles to release*/
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_RELEASE_SRVC_HNDL_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_RELEASE_SRVC_HNDL_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg(conn, message, msg_hdr,
                               (void*) &(mm_nw_reg_client_info[i].nw_reg_hndl),
                               MMCLIENT_MAX_SRVC_HNDL_LEN);

  MMCLIENT_NW_REG_LIST_MUTEX_LOCK();

  mm_nw_reg_client_info[i].valid = MM_FALSE;
  mm_nw_reg_client_info[i].st_hndl = NULL;
  mm_nw_reg_client_info[i].mmclient_cb_func = NULL;
  mm_nw_reg_client_info[i].user_data = NULL;
  mm_nw_reg_client_info[i].mmclient_scan_cb_func = NULL;
  mm_nw_reg_client_info[i].scan_user_data = NULL;
  mm_nw_reg_client_info[i].nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;

  MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();
 
  mmclient_free_msg_hdr(msg_hdr);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_NW_REG_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_properties_register_ind
===========================================================================*/
/*!
@brief
  Function to register for network service related indications

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg cb_fn - Callback to be invoked when one of the registered events happen
@arg validity_mask - bitmask to indicate which indications caller is
                     interested in
@arg *user_data - any information user wishes to pass along

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_properties_register_ind
(
  mmclient_nw_reg_hndl_t          nw_reg_hndl,
  mmclient_nw_reg_ind_cb_fn_t     cb_fn,
  mmclient_nw_reg_validity_mask_t validity_mask,
  void*                           user_data
)
{
  int                            i = 0;
  int                            bytes = 0;
  int                            ret = MMCLIENT_MSG_NO_ERR;
  int                            reti = MM_FAILURE;
  mmclient_nw_reg_register_ind_t register_ind;
  mmclient_dbus_state_t          *dbus_client_state = NULL;
  mmclient_msg_hdr_t             *msg_hdr = NULL;
  DBusMessage                    *message = NULL;
  DBusConnection                 *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
         mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_REGISTER_IND_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_REGISTER_IND_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_nw_reg_register_ind_t);

  bytes = snprintf(register_ind.nw_reg_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_nw_reg_client_info[i].nw_reg_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  register_ind.validity_mask = validity_mask;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &register_ind,
                                    (unsigned int) sizeof(mmclient_nw_reg_register_ind_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  if (ret == MMCLIENT_MSG_NO_ERR)
  {
    if (reti == MM_SUCCESS)
    {
      MMCLIENT_NW_REG_LIST_MUTEX_LOCK();

      MMCLIENT_LOG_MED("%s(): Register ind succeeded, setting local call back\n", __func__);
      mm_nw_reg_client_info[i].mmclient_cb_func = cb_fn;
      mm_nw_reg_client_info[i].user_data = user_data;

      MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();

      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_NW_REG_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_properties
===========================================================================*/
/*!
@brief
  Function to query for the registered network properties

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *nw_reg_properties - out param which will get filled with the
                          network properties

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_properties
(
  mmclient_nw_reg_hndl_t       nw_reg_hndl,
  mmclient_nw_reg_properties_t *nw_reg_properties
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl || !nw_reg_properties)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
     if(mm_nw_reg_client_info[i].valid &&
        mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
       break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_GET_PROPERTIES_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_GET_PROPERTIES_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*)(mm_nw_reg_client_info[i].nw_reg_hndl),
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) nw_reg_properties,
                                    (unsigned int) sizeof(mmclient_nw_reg_properties_t));
 
  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed!\n", __func__);
    goto bail;
  }

  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_register_network
===========================================================================*/
/*!
@brief
  Function to register to a particular network

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *register_network_info - Information about the network to register to

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_register_network
(
  mmclient_nw_reg_hndl_t                  nw_reg_hndl,
  mmclient_nw_reg_register_network_info_t *register_network_info
)
{
  int                               i = 0;
  int                               bytes = 0;
  int                               ret = MMCLIENT_MSG_NO_ERR;
  int                               reti = MM_FAILURE;
  mmclient_nw_reg_register_params_t register_nw_params;
  mmclient_dbus_state_t             *dbus_client_state = NULL;
  mmclient_msg_hdr_t                *msg_hdr = NULL;
  DBusMessage                       *message = NULL;
  DBusConnection                    *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl || !register_network_info)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
       mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_REGISTER_NETWORK_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_REGISTER_NETWORK_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_nw_reg_register_params_t);

  bytes = snprintf(register_nw_params.nw_reg_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_nw_reg_client_info[i].nw_reg_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(register_nw_params.register_info), register_network_info,
         sizeof(mmclient_nw_reg_register_network_info_t));

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) &register_nw_params,
                                    (unsigned int) sizeof(mmclient_nw_reg_register_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_LOG_MED("%s(): Register Network succeeded!\n", __func__);
      MMCLIENT_NW_REG_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): failed to register network!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_operator
===========================================================================*/
/*!
@brief
  Function to get the current operator

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *curr_operator - information about the current operator will be
                      filled here

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_operator
(
  mmclient_nw_reg_hndl_t       nw_reg_hndl,
  mmclient_nw_reg_properties_t *curr_operator
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl || !curr_operator)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
     if(mm_nw_reg_client_info[i].valid &&
        mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
       break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_GET_OPERATOR_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_GET_OPERATOR_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*)(mm_nw_reg_client_info[i].nw_reg_hndl),
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) curr_operator,
                                    (unsigned int) sizeof(mmclient_nw_reg_properties_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_scan_operators
===========================================================================*/
/*!
@brief
  Function to scan for all the available operators in the system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *operators - array which will be filled with operators information
@arg *num_operators - in/out param which is set to the size of the array

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_scan_operators
(
  mmclient_nw_reg_hndl_t        nw_reg_hndl,
  mmclient_nw_reg_scan_cb_fn_t  cb_fn,
  void*                         user_data
)
{
  int                     i = 0;
  int                     ret = MMCLIENT_MSG_NO_ERR;
  int                     reti = MM_FAILURE;
  mmclient_dbus_state_t   *dbus_client_state = NULL;
  mmclient_msg_hdr_t      *msg_hdr = NULL;
  DBusMessage             *message = NULL;
  DBusConnection          *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
       mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_SCAN_OPERATORS_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_SCAN_OPERATORS_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) mm_nw_reg_client_info[i].nw_reg_hndl,
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) &reti, (unsigned int) sizeof(int));

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_NW_REG_LIST_MUTEX_LOCK();

      MMCLIENT_LOG_MED("%s(): Scan Operator req sent, setting local scan call back\n", __func__);
      mm_nw_reg_client_info[i].mmclient_scan_cb_func = cb_fn;
      mm_nw_reg_client_info[i].scan_user_data = user_data;

      MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();

      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_NW_REG_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_radio_settings
===========================================================================*/
/*!
@brief
  Function to get radio settings

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *radio_settings - current radio settings

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_radio_settings
(
  mmclient_nw_reg_hndl_t           nw_reg_hndl,
  mmclient_nw_reg_radio_settings_t *radio_settings
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl || !radio_settings)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
        mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
       break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_GET_RADIO_SETTINGS_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_GET_RADIO_SETTINGS_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*)(mm_nw_reg_client_info[i].nw_reg_hndl),
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) radio_settings,
                                    (unsigned int) sizeof(mmclient_nw_reg_radio_settings_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_set_radio_settings
===========================================================================*/
/*!
@brief
  Function to set radio settings

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *radio_settings - current radio settings

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_set_radio_settings
(
  mmclient_nw_reg_hndl_t            nw_reg_hndl,
  mmclient_nw_reg_radio_settings_t  *radio_settings
)
{
  int                                     i = 0;
  int                                     bytes = 0;
  int                                     ret = MMCLIENT_MSG_NO_ERR;
  int                                     reti = MM_FAILURE;
  mmclient_nw_reg_radio_settings_params_t radio_settings_params;
  mmclient_dbus_state_t                   *dbus_client_state = NULL;
  mmclient_msg_hdr_t                      *msg_hdr = NULL;
  DBusMessage                             *message = NULL;
  DBusConnection                          *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl || !radio_settings)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
       mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_SET_RADIO_SETTINGS_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_SET_RADIO_SETTINGS_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_nw_reg_radio_settings_params_t);

  bytes = snprintf(radio_settings_params.nw_reg_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_nw_reg_client_info[i].nw_reg_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(radio_settings_params.radio_settings),
         radio_settings,
         sizeof(mmclient_nw_reg_radio_settings_t));

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) &radio_settings_params,
                                    (unsigned int) sizeof(mmclient_nw_reg_radio_settings_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_LOG_MED("%s(): Set radio Settings succeeded!\n", __func__);
      MMCLIENT_NW_REG_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): failed to set radio settings!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}


/*===========================================================================
  FUNCTION  mmclient_nw_reg_get_network_time
===========================================================================*/
/*!
@brief
  Function to get the network time

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *network_time - current network time

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_get_network_time
(
  mmclient_nw_reg_hndl_t nw_reg_hndl,
  mmclient_nw_reg_time_t *network_time
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nw_reg_hndl || !network_time)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
        mm_nw_reg_client_info[i].st_hndl == nw_reg_hndl)
       break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NW_REG_GET_NETWORK_TIME_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_NW_REG_GET_NETWORK_TIME_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_nw_reg_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*)(mm_nw_reg_client_info[i].nw_reg_hndl),
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) network_time,
                                    (unsigned int) sizeof(mmclient_nw_reg_time_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_properties_hdlr
===========================================================================*/
/*!
@brief
  Function to call the registered callback function

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg nw_reg_properties - current radio settings

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_properties_hdlr
(
  char                           *nw_reg_hndl,
  mmclient_nw_reg_properties_t   nw_reg_properties
)
{
  int i = 0;

  MMCLIENT_LOG_LOW("%s(): In mmclient_nw_reg_properties_hdlr...\n", __func__);
  MMCLIENT_LOG_LOW("%s(): status is %d\n", __func__, nw_reg_properties.status);

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if (nw_reg_hndl == NULL)
  {
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
       (!strcmp(mm_nw_reg_client_info[i].nw_reg_hndl , nw_reg_hndl)))
      break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Error. No matching NW Reg handles to post evt!\n", __func__);
    goto bail;
  }

  if(mm_nw_reg_client_info[i].mmclient_cb_func)
  {
    MMCLIENT_NW_REG_GLOBAL_UNLOCK();
    MMCLIENT_LOG_LOW("%s(): Calling client properties cb function\n", __func__);
    mm_nw_reg_client_info[i].mmclient_cb_func(&nw_reg_properties,
                                               mm_nw_reg_client_info[i].user_data);

    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): No cb function registered for nw reg properties ind!\n", __func__);
  }

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}


/*===========================================================================
  FUNCTION  mmclient_nw_reg_scan_operators_hdlr
===========================================================================*/
/*!
@brief
  Function to call the scan callback function

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg *scanned_operators - struct of scan results

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_nw_reg_scan_operators_hdlr
(
  char                                *nw_reg_hndl,
  mmclient_nw_reg_scanned_operators_t *scanned_operators
)
{
  int i = 0;

  MMCLIENT_LOG_LOW("%s(): In mmclient_nw_reg_scan_operators_hdlr...\n", __func__);
  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if (!nw_reg_hndl || !scanned_operators)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): num_operators %d\n", __func__, scanned_operators->num_operators);

  for(i = 0 ; i < MMCLIENT_MAX_NW_REG_CLIENT ; i++)
  {
    if(mm_nw_reg_client_info[i].valid &&
       (!strcmp(mm_nw_reg_client_info[i].nw_reg_hndl, nw_reg_hndl)))
      break;
  }

  if(i == MMCLIENT_MAX_NW_REG_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Error. No matching NW Reg handles to post evt!\n", __func__);
    goto bail;
  }

  if(mm_nw_reg_client_info[i].mmclient_scan_cb_func)
  {
    MMCLIENT_NW_REG_GLOBAL_UNLOCK();
    MMCLIENT_LOG_MED("%s(): Calling client scan cb function\n", __func__);
    mm_nw_reg_client_info[i].mmclient_scan_cb_func(scanned_operators,
                                                   mm_nw_reg_client_info[i].scan_user_data);

    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): No cb function registered for nw reg scan operators!\n", __func__);
  }

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_attach_get_status
===========================================================================*/
/*!
@brief
  Function to get the current network attach status

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg attach_info - attach_info (out param)

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_attach_get_status
(
  mmclient_nad_hndl_t  nad_hndl,
  mmclient_attach_t    *attach_info
)
{
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_ATTACH_GET_STATUS_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_ATTACH_GET_STATUS_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) nad_hndl,
                                    MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) attach_info,
                                    (unsigned int) sizeof(mmclient_attach_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_attach_set_status
===========================================================================*/
/*!
@brief
  Function to initiate network attach

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nw_reg_hndl - Network registration handle
@arg attach_info

@note
  Dependencies
    - mmclient_nw_reg_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_attach_set_attach
(
  mmclient_nad_hndl_t  nad_hndl,
  mmclient_attach_t    attach_info
)
{
  int                           ret = MMCLIENT_MSG_NO_ERR;
  int                           reti = MM_FAILURE;
  int                           bytes = 0;
  mmclient_attach_info_params_t attach_info_params;
  mmclient_dbus_state_t         *dbus_client_state = NULL;
  mmclient_msg_hdr_t            *msg_hdr = NULL;
  DBusMessage                   *message = NULL;
  DBusConnection                *conn = NULL;

  MMCLIENT_NW_REG_GLOBAL_LOCK();

  if(!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_ATTACH_SET_ATTACH_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NW_REG;
  msg_hdr->msg_id      = MSG_ID_ATTACH_SET_ATTACH_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_attach_info_params_t);

  bytes = snprintf(attach_info_params.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char*) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(); truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }
  attach_info_params.attach_info = attach_info;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) &attach_info_params,
                                    (unsigned int) sizeof(mmclient_attach_info_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_LOG_LOW("%s(): Attach/Detach succeeded!\n", __func__);
      MMCLIENT_NW_REG_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): attach/detach operation failed!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_cleanup_state_for_nad_inst
===========================================================================*/
/*!
@brief
  Function to cleanup resources tied to the given NAD instance

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
void mmclient_nw_reg_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst)
{
  int i = 0;

  MMCLIENT_NW_REG_GLOBAL_LOCK();
  MMCLIENT_NW_REG_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_MED("%s(): cleaning up NW REG state for NAD inst [%d]\n",
                   __func__, nad_inst);

  for (i = 0; i < MMCLIENT_MAX_NW_REG_CLIENT; i++)
  {
    if (mm_nw_reg_client_info[i].valid == MM_TRUE
          && mm_nw_reg_client_info[i].nad_inst == nad_inst)
    {
      mm_nw_reg_client_info[i].valid = MM_FALSE;
      mm_nw_reg_client_info[i].st_hndl = NULL;
      mm_nw_reg_client_info[i].mmclient_cb_func = NULL;
      mm_nw_reg_client_info[i].user_data = NULL;
      mm_nw_reg_client_info[i].mmclient_scan_cb_func = NULL;
      mm_nw_reg_client_info[i].scan_user_data = NULL;
      mm_nw_reg_client_info[i].nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;
    }
  }

  MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();
}

/*===========================================================================
  FUNCTION  mmclient_nw_reg_cleanup_state
===========================================================================*/
/*!
@brief
  Function to cleanup resources

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
int mmclient_nw_reg_cleanup_state(void)
{
  int i = 0;

  MMCLIENT_NW_REG_GLOBAL_LOCK();
  MMCLIENT_NW_REG_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_MED("%s(): cleaning up NW REG state...", __func__);

  for (i = 0; i < MMCLIENT_MAX_NW_REG_CLIENT; i++)
  {
    mm_nw_reg_client_info[i].valid = MM_FALSE;
    mm_nw_reg_client_info[i].st_hndl = NULL;
    mm_nw_reg_client_info[i].mmclient_cb_func = NULL;
    mm_nw_reg_client_info[i].user_data = NULL;
    mm_nw_reg_client_info[i].mmclient_scan_cb_func = NULL;
    mm_nw_reg_client_info[i].scan_user_data = NULL;
    mm_nw_reg_client_info[i].nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;
  }

  MMCLIENT_NW_REG_LIST_MUTEX_UNLOCK();
  MMCLIENT_NW_REG_GLOBAL_UNLOCK();

  return MM_SUCCESS;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
