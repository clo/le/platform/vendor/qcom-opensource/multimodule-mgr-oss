/******************************************************************************

                M M C L I E N T _ S I M _ H D L R . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <pthread.h>

#include "mmclient_dbus.h"
#include "mmclient_util.h"
#include "mmclient_msg.h"

#define VAR_UNUSED(X) (void)(X)

typedef uint8_t boolean;
#define MM_FALSE 0
#define MM_TRUE 1

#define MMCLIENT_MAX_SIM_CLIENT  (10)

typedef struct
{
  boolean                  valid;
  mmclient_sim_hndl_t      st_hndl;
  mmclient_sim_cb_func_t   mmclient_cb_func;
  mmclient_sim_ind_cb_fn_t mmclient_ind_cb_fn;
  void                     *user_data;
  char                     sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nad_inst_t      nad_inst;
} mmclient_sim_client_info_t;

static mmclient_sim_client_info_t mm_sim_client_info[MMCLIENT_MAX_SIM_CLIENT];

pthread_mutex_t clnt_sim_list_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_SIM_LIST_MUTEX_LOCK()                            \
  MMCLIENT_LOG_LOW("%s(): locking SIM list mutex\n", __func__);   \
  pthread_mutex_lock(&clnt_sim_list_mtx);

#define MMCLIENT_SIM_LIST_MUTEX_UNLOCK()                          \
  MMCLIENT_LOG_LOW("%s(): unlocking SIM list mutex\n", __func__); \
  pthread_mutex_unlock(&clnt_sim_list_mtx);

pthread_mutex_t mmclient_sim_global_mutex = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_SIM_GLOBAL_LOCK()                                  \
  MMCLIENT_LOG_LOW("%s(): locking SIM global mutex\n", __func__);   \
  pthread_mutex_lock(&mmclient_sim_global_mutex);

#define MMCLIENT_SIM_GLOBAL_UNLOCK()                                \
  MMCLIENT_LOG_LOW("%s(): unlocking SIM global mutex\n", __func__); \
  pthread_mutex_unlock(&mmclient_sim_global_mutex);

extern mmclient_nad_inst_t mmclient_get_nad_inst_for_nad_hndl
(
  mmclient_hndl_t *nad_hndl
);

/*===========================================================================
  FUNCTION  mmclient_sim_get_srvc_hndl
===========================================================================*/
/*!
@brief
  Function to get handle for SIM service

@return
  mmclient_sim_hndl_t reference

@arg nad_hndl - NAD handle
@arg cb - callback function to receive indication about SIM status
@arg *user_data - any information the caller wants to pass through

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_sim_hndl_t mmclient_sim_get_srvc_hndl
(
  mmclient_nad_hndl_t      nad_hndl,
  mmclient_sim_cb_func_t   cb,
  void                     *user_data
)
{
  int                   i = 0;
  int                   bytes = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  char                  sim_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  MMCLIENT_LOG_LOW("%s(): Running mmclient_sim_get_srvc_hndl\n", __func__);

  if(!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(!mm_sim_client_info[i].valid)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    /*Error. No more SIM handles*/
    MMCLIENT_LOG_ERR("%s(): No more handles available!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_GET_SRVC_HNDL_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_GET_SRVC_HNDL_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN,
                                    (void*) &sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_SIM_LIST_MUTEX_LOCK();

    bytes = snprintf(mm_sim_client_info[i].sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s", sim_hndl);
    if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
    {
      MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_SIM_LIST_MUTEX_UNLOCK();
      goto bail;
    }

    mm_sim_client_info[i].valid = MM_TRUE;
    mm_sim_client_info[i].mmclient_cb_func = cb;
    mm_sim_client_info[i].user_data = user_data;
    mm_sim_client_info[i].st_hndl = (mmclient_sim_hndl_t) mm_sim_client_info[i].sim_hndl;
    mm_sim_client_info[i].nad_inst = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);

    MMCLIENT_SIM_LIST_MUTEX_UNLOCK();

    MMCLIENT_LOG_MED("%s(): Returning store handle %p for SIM handle %s index %d!\n",
                     __func__,
                     mm_sim_client_info[i].st_hndl,
                     mm_sim_client_info[i].sim_hndl,
                     i);

    mmclient_free_msg_hdr(msg_hdr);
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return (mm_sim_client_info[i].st_hndl);
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return NULL;
}


/*===========================================================================
  FUNCTION  mmclient_sim_release_srvc_hndl
===========================================================================*/
/*!
@brief
  Function to release SIM service handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_release_srvc_hndl
(
  mmclient_sim_hndl_t sim_hndl
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
        mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    /*Error. No matching SIM handles to release*/
    MMCLIENT_LOG_ERR("%s(): Matching handle not found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_RELEASE_SRVC_HNDL_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message header */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_RELEASE_SRVC_HNDL_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  ret = mmclient_dbus_send_msg(conn, message, msg_hdr,
                               (void*) &(mm_sim_client_info[i].sim_hndl),
                               MMCLIENT_MAX_SRVC_HNDL_LEN);

  /*Call function to release SIM handle
  /Rel_data_srvc_handle never fails*/
  MMCLIENT_SIM_LIST_MUTEX_LOCK();

  mm_sim_client_info[i].valid = MM_FALSE;
  mm_sim_client_info[i].st_hndl = NULL;
  mm_sim_client_info[i].mmclient_cb_func = NULL;
  mm_sim_client_info[i].mmclient_ind_cb_fn = NULL;
  mm_sim_client_info[i].user_data = NULL;
  mm_sim_client_info[i].nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;

  MMCLIENT_SIM_LIST_MUTEX_UNLOCK();

  mmclient_free_msg_hdr(msg_hdr);

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_enter_pin
===========================================================================*/
/*!
@brief
  Function to enter SIM pin

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *enter_pin_info - Information about SIM pin

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_enter_pin
(
  mmclient_sim_hndl_t           sim_hndl,
  mmclient_sim_enter_pin_info_t *enter_pin_info
)
{
  int                             i = 0;
  int                             bytes = 0;
  int                             ret = MMCLIENT_MSG_NO_ERR;
  int                             reti = MM_FAILURE;
  mmclient_sim_enter_pin_params_t enter_pin_params;
  mmclient_dbus_state_t           *dbus_client_state = NULL;
  mmclient_msg_hdr_t              *msg_hdr = NULL;
  DBusMessage                     *message = NULL;
  DBusConnection                  *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !enter_pin_info)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
       mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Max SIM handles reached!\n", __func__);
        goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_ENTER_PIN_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_ENTER_PIN_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_enter_pin_params_t);

  bytes = snprintf(enter_pin_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(enter_pin_params.enter_pin_info), enter_pin_info, sizeof(mmclient_sim_enter_pin_info_t));

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*) &enter_pin_params,
                                    (unsigned int) sizeof(mmclient_sim_enter_pin_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  if(reti == MM_SUCCESS)
  {
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_change_pin
===========================================================================*/
/*!
@brief
  Function to change the PIN

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *change_pin_info - New PIN information

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_change_pin
(
  mmclient_sim_hndl_t            sim_hndl,
  mmclient_sim_change_pin_info_t *change_pin_info
)
{
  int                              i = 0;
  int                              bytes = 0;
  int                              ret = MMCLIENT_MSG_NO_ERR;
  int                              reti = MM_SUCCESS;
  mmclient_sim_change_pin_params_t change_pin_params;
  mmclient_dbus_state_t            *dbus_client_state = NULL;
  mmclient_msg_hdr_t               *msg_hdr = NULL;
  DBusMessage                      *message = NULL;
  DBusConnection                   *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !change_pin_info)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
       mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Max SIM handles reached!\n", __func__);
        goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_CHANGE_PIN_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_CHANGE_PIN_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_change_pin_params_t);

  bytes = snprintf(change_pin_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(change_pin_params.change_pin_info), change_pin_info,
         sizeof(mmclient_sim_change_pin_info_t));

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &change_pin_params,
                                    (unsigned int) sizeof(mmclient_sim_change_pin_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  if(reti == MM_SUCCESS)
  {
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_reset_pin
===========================================================================*/
/*!
@brief
  Function to reset SIM pin

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *reset_pin_info - New PIN information

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_reset_pin
(
  mmclient_sim_hndl_t           sim_hndl,
  mmclient_sim_reset_pin_info_t *reset_pin_info
)
{
  int                             i = 0;
  int                             bytes = 0;
  int                             ret = MMCLIENT_MSG_NO_ERR;
  int                             reti = MM_FAILURE;
  mmclient_sim_reset_pin_params_t reset_pin_params;
  mmclient_dbus_state_t           *dbus_client_state = NULL;
  mmclient_msg_hdr_t              *msg_hdr = NULL;
  DBusMessage                     *message = NULL;
  DBusConnection                  *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !reset_pin_info)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
       mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Max SIM handles reached!\n", __func__);
        goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_RESET_PIN_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_RESET_PIN_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_reset_pin_params_t);

  bytes = snprintf(reset_pin_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(reset_pin_params.reset_pin_info), reset_pin_info,
         sizeof(mmclient_sim_reset_pin_info_t));

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &reset_pin_params,
                                    (unsigned int) sizeof(mmclient_sim_reset_pin_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  if(reti == MM_SUCCESS)
  {
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_set_pin_protection
===========================================================================*/
/*!
@brief
  Function to set PIN protection

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *lock_pin_info - PIN information

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_set_pin_protection
(
  mmclient_sim_hndl_t          sim_hndl,
  mmclient_sim_lock_pin_info_t *lock_pin_info
)
{
  int                                  i = 0;
  int                                  bytes = 0;
  int                                  ret = MMCLIENT_MSG_NO_ERR;
  int                                  reti = MM_FAILURE;
  mmclient_sim_pin_protection_params_t pin_protection_params;
  mmclient_dbus_state_t                *dbus_client_state = NULL;
  mmclient_msg_hdr_t                   *msg_hdr = NULL;
  DBusMessage                          *message = NULL;
  DBusConnection                       *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !lock_pin_info)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  /* Check if pin type is supported by the modem */
  if (lock_pin_info->type > UIM_PIN_ID_HIDDEN_KEY)
  {
    MMCLIENT_LOG_ERR("%s(): Unsupported pin_type: %d\n",
                     __func__, lock_pin_info->type);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
       mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Max SIM handles reached!\n", __func__);
        goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_SET_PIN_PROTECTION_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_SET_PIN_PROTECTION_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_pin_protection_params_t);

  bytes = snprintf(pin_protection_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(pin_protection_params.lock_pin_info), lock_pin_info,
         sizeof(mmclient_sim_lock_pin_info_t));

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &pin_protection_params,
                                    (unsigned int) sizeof(mmclient_sim_pin_protection_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  if(reti == MM_SUCCESS)
  {
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_SUCCESS;
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_get_properties
===========================================================================*/
/*!
@brief
  Function to get SIM properties

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg *sim_properties - SIM properties

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_get_properties
(
  mmclient_sim_hndl_t       sim_hndl,
  mmclient_sim_properties_t *sim_properties
)
{
  int                   i = 0;
  int                   ret = MMCLIENT_MSG_NO_ERR;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusMessage           *message = NULL;
  DBusConnection        *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !sim_properties)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
        mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    /*Error. No matching SIM handles to release*/
    MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }
  else
  {
    dbus_client_state = mmclient_get_connection_state();
    if (!dbus_client_state)
    {
      MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
      goto bail;
    }

    conn = dbus_client_state->conn;
    if (!conn)
    {
      MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
      goto bail;
    }

    /* Get message object */
    message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_GET_PROPERTIES_REQ, NULL);
    if(!message)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
      goto bail;
    }

    /* Prepare message reply */
    msg_hdr = mmclient_get_msg_hdr();
    if (!msg_hdr)
    {
      MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
      goto bail;
    }

    msg_hdr->srvc_id     = SRVC_ID_SIM;
    msg_hdr->msg_id      = MSG_ID_SIM_GET_PROPERTIES_REQ;
    msg_hdr->client_id   = dbus_client_state->client_id;
    msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
    msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

    MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                     __func__, mm_sim_client_info[i].sim_hndl);

    ret = mmclient_dbus_send_msg_sync( conn, message, msg_hdr,
                                       (void*)(mm_sim_client_info[i].sim_hndl),
                                       MMCLIENT_MAX_SRVC_HNDL_LEN, (void*) sim_properties,
                                       (unsigned int) sizeof(mmclient_sim_properties_t));

    mmclient_free_msg_hdr(msg_hdr);
  }

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_register_ind
===========================================================================*/
/*!
@brief
  Function to register for SIM related indications

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg cb_fn - callback function to listen for SIM related indications
@arg validity_mask - bitmask to identify the indications caller is
                     interested in
@arg user_data - Any information user wants to pass

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_register_ind
(
  mmclient_sim_hndl_t                     sim_hndl,
  mmclient_sim_ind_cb_fn_t                cb_fn,
  mmclient_sim_properties_validity_mask_t validity_mask,
  void*                                   user_data
)
{
  int                         i = 0;
  int                         bytes = 0;
  int                         ret = MMCLIENT_MSG_NO_ERR;
  int                         reti = MM_FAILURE;
  mmclient_sim_register_ind_t register_ind_params;
  mmclient_dbus_state_t       *dbus_client_state = NULL;
  mmclient_msg_hdr_t          *msg_hdr = NULL;
  DBusMessage                 *message = NULL;
  DBusConnection              *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid && mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    /*Error. No matching SIM handles to release*/
     MMCLIENT_SIM_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_REGISTER_IND_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_REGISTER_IND_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_register_ind_t);

  bytes = snprintf(register_ind_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  register_ind_params.validity_mask = validity_mask;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr,
                                    (void*)&register_ind_params,
                                    (unsigned int) sizeof(mmclient_sim_register_ind_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_SIM_LIST_MUTEX_LOCK();

      MMCLIENT_LOG_MED("%s(): Register ind succeeded, setting local call back\n", __func__);
      mm_sim_client_info[i].mmclient_ind_cb_fn = cb_fn;
      mm_sim_client_info[i].user_data = user_data;

      MMCLIENT_SIM_LIST_MUTEX_UNLOCK();

      mmclient_free_msg_hdr(msg_hdr);
      MMCLIENT_SIM_GLOBAL_UNLOCK();
      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): failed to register for SIM related indications!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}


/*===========================================================================
  FUNCTION  mmclient_sim_read_record
===========================================================================*/
/*!
@brief
  Function to read a linear fixed/cyclic file from the SIM file system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
     file_info_ptr - pointer to struct with the file_id, path, and path_len
     record - record number in file - starting from 1
     length - length of content to be read - 0 means read complete record
@arg *sim_record_content - output param containing content of record and len

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_read_record
(
  mmclient_sim_hndl_t           sim_hndl,
  mmclient_sim_file_info_t      *file_info_ptr,
  uint16_t                      record,
  uint16_t                      length,
  mmclient_sim_record_content_t *sim_record_content
)
{
  int                               i = 0;
  int                               bytes = 0;
  int                               ret = MMCLIENT_MSG_NO_ERR;
  mmclient_sim_read_record_params_t read_record_params;
  mmclient_dbus_state_t             *dbus_client_state = NULL;
  mmclient_msg_hdr_t                *msg_hdr = NULL;
  DBusMessage                       *message = NULL;
  DBusConnection                    *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !file_info_ptr || !sim_record_content)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
        mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): No Matching sim handles found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_READ_RECORD_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_READ_RECORD_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_read_record_params_t);

  bytes = snprintf(read_record_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(read_record_params.file_info), file_info_ptr, sizeof(mmclient_sim_file_info_t));
  read_record_params.length = length;
  read_record_params.record = record;

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &read_record_params,
                                    (unsigned int) sizeof(mmclient_sim_read_record_params_t),
                                    (void*) sim_record_content,
                                    (unsigned int) sizeof(mmclient_sim_record_content_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}


/*===========================================================================
  FUNCTION  mmclient_sim_read_transparent
===========================================================================*/
/*!
@brief
  Function to read a transparent file from the SIM file system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
     file_info_ptr - pointer to struct with the file_id, path, and path_len
     offset - offset of the read operation
     length - length of content to be read - 0 means read complete record
@arg *sim_transparent_content - output param containing content and len

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/

mmclient_status_t mmclient_sim_read_transparent
(
  mmclient_sim_hndl_t                sim_hndl,
  mmclient_sim_file_info_t           *file_info_ptr,
  uint16_t                           offset,
  uint16_t                           length,
  mmclient_sim_transparent_content_t *sim_transparent_content
)
{
  int                                    i = 0;
  int                                    bytes = 0;
  int                                    ret = MMCLIENT_MSG_NO_ERR;
  mmclient_sim_read_transparent_params_t read_transparent_params;
  mmclient_dbus_state_t                  *dbus_client_state = NULL;
  mmclient_msg_hdr_t                     *msg_hdr = NULL;
  DBusMessage                            *message = NULL;
  DBusConnection                         *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !file_info_ptr || !sim_transparent_content)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
        mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): No Matching sim handles found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_READ_TRANSPARENT_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_READ_TRANSPARENT_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_read_transparent_params_t);

  bytes = snprintf(read_transparent_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(read_transparent_params.file_info), file_info_ptr, sizeof(mmclient_sim_file_info_t));
  read_transparent_params.length = length;
  read_transparent_params.offset = offset;

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &read_transparent_params,
                                    (unsigned int) sizeof(mmclient_sim_read_transparent_params_t),
                                    (void*) sim_transparent_content,
                                    (unsigned int) sizeof(mmclient_sim_transparent_content_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed! [%d]\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_get_file_attributes
===========================================================================*/
/*!
@brief
  Function to get the file attributes from the SIM file system

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
     file_info_ptr - pointer to struct with the file_id, path, and path_len
@arg *sim_file_attributes - output param containing file attributes

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_get_file_attributes
(
  mmclient_sim_hndl_t            sim_hndl,
  mmclient_sim_file_info_t       *file_info_ptr,
  mmclient_sim_file_attributes_t *sim_file_attributes
)
{
  int                                       i = 0;
  int                                       bytes = 0;
  int                                       ret = MMCLIENT_MSG_NO_ERR;
  mmclient_sim_get_file_attributes_params_t get_file_attrib_params;
  mmclient_dbus_state_t                     *dbus_client_state = NULL;
  mmclient_msg_hdr_t                        *msg_hdr = NULL;
  DBusMessage                               *message = NULL;
  DBusConnection                            *conn = NULL;

  MMCLIENT_SIM_GLOBAL_LOCK();

  if(!sim_hndl || !file_info_ptr || !sim_file_attributes)
  {
    MMCLIENT_LOG_ERR("%s(): Invalid params passed!\n", __func__);
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
        mm_sim_client_info[i].st_hndl == sim_hndl)
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): No Matching sim handles found!\n", __func__);
    goto bail;
  }

  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_SIM_GET_FILE_ATTRIBUTES_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Prepare message reply */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_SIM;
  msg_hdr->msg_id      = MSG_ID_SIM_GET_FILE_ATTRIBUTES_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mm_sim_client_info[i].nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_sim_get_file_attributes_params_t);

  bytes = snprintf(get_file_attrib_params.sim_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   mm_sim_client_info[i].sim_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    mmclient_free_msg_hdr(msg_hdr);
    goto bail;
  }

  memcpy(&(get_file_attrib_params.file_info), file_info_ptr, sizeof(mmclient_sim_file_info_t));

  MMCLIENT_LOG_LOW("%s(): Sending message to dbus sim_hndl=%s\n",
                   __func__, mm_sim_client_info[i].sim_hndl);

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &get_file_attrib_params,
                                    (unsigned int)sizeof(mmclient_sim_get_file_attributes_params_t),
                                    (void*) sim_file_attributes,
                                    (unsigned int) sizeof(mmclient_sim_file_attributes_t));

  mmclient_free_msg_hdr(msg_hdr);

  if(ret != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
    goto bail;
  }

  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_SUCCESS;

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_availability_hdlr
===========================================================================*/
/*!
@brief
  Function to call the registered client callback function

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg sim_availability

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_availability_hdlr
(
  char                        *sim_hndl,
  mmclient_sim_availability_t sim_availability
)
{
  int i = 0;
  MMCLIENT_SIM_GLOBAL_LOCK();

  if (sim_hndl == NULL)
  {
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
       (!strcmp(mm_sim_client_info[i].sim_hndl, sim_hndl)))
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Error. No matching SIM handles to post evt!\n", __func__);
    goto bail;
  }
  else
  {
    if(mm_sim_client_info[i].mmclient_cb_func)
    {
      MMCLIENT_SIM_GLOBAL_UNLOCK();
      MMCLIENT_LOG_LOW("%s(): Calling client avail cb function\n", __func__);
      mm_sim_client_info[i].mmclient_cb_func(sim_availability,
                                              mm_sim_client_info[i].user_data);

      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): No cb function registered for availability ind!\n", __func__);
    }
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_properties_hdlr
===========================================================================*/
/*!
@brief
  Function to call the registered client callback function for receiving
  property change events

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg sim_hndl - SIM service handle
@arg sim_properties

@note
  Dependencies
    - mmclient_sim_get_srvc_hndl() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_sim_properties_hdlr
(
  char                        *sim_hndl,
  mmclient_sim_properties_t   sim_properties
)
{
  int i = 0;

  MMCLIENT_LOG_LOW("%s(): In mmclient_sim_properties_hdlr...\n", __func__);
  MMCLIENT_LOG_LOW("%s(): imsi is %s\n", __func__, sim_properties.imsi);
  MMCLIENT_SIM_GLOBAL_LOCK();

  if (sim_hndl == NULL)
  {
    goto bail;
  }

  for(i = 0 ; i < MMCLIENT_MAX_SIM_CLIENT ; i++)
  {
    if(mm_sim_client_info[i].valid &&
       (!strcmp(mm_sim_client_info[i].sim_hndl , sim_hndl)))
      break;
  }

  if(i == MMCLIENT_MAX_SIM_CLIENT)
  {
    MMCLIENT_LOG_ERR("%s(): Error. No matching SIM handles to post evt!\n", __func__);
    goto bail;
  }
  else
  {
    if(mm_sim_client_info[i].mmclient_ind_cb_fn)
    {
      MMCLIENT_SIM_GLOBAL_UNLOCK();
      MMCLIENT_LOG_LOW("%s(): Calling client properties cb function\n", __func__);
      mm_sim_client_info[i].mmclient_ind_cb_fn(&sim_properties,
                                                mm_sim_client_info[i].user_data);

      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): No cb function registered for properties ind!\n", __func__);
    }
  }

bail:
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*===========================================================================
  FUNCTION  mmclient_sim_cleanup_state_for_nad_inst
===========================================================================*/
/*!
@brief
  Function to cleanup resources

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
void mmclient_sim_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst)
{
  int i = 0;

  MMCLIENT_SIM_GLOBAL_LOCK();
  MMCLIENT_SIM_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_MED("%s(): cleaning up SIM state for NAD inst [%d]\n",
                   __func__, nad_inst);

  for (i = 0; i < MMCLIENT_MAX_SIM_CLIENT; i++)
  {
    if (mm_sim_client_info[i].valid == MM_TRUE
          && mm_sim_client_info[i].nad_inst == nad_inst)
    {
      mm_sim_client_info[i].valid = MM_FALSE;
      mm_sim_client_info[i].st_hndl = NULL;
      mm_sim_client_info[i].mmclient_cb_func = NULL;
      mm_sim_client_info[i].mmclient_ind_cb_fn = NULL;
      mm_sim_client_info[i].user_data = NULL;
      mm_sim_client_info[i].nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;
    }
  }

  MMCLIENT_SIM_LIST_MUTEX_UNLOCK();
  MMCLIENT_SIM_GLOBAL_UNLOCK();
}

/*===========================================================================
  FUNCTION  mmclient_sim_cleanup_state
===========================================================================*/
/*!
@brief
  Function to cleanup resources

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
int mmclient_sim_cleanup_state(void)
{
  int i = 0;

  MMCLIENT_SIM_GLOBAL_LOCK();
  MMCLIENT_SIM_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_MED("%s(): cleaning up SIM state...", __func__);

  for (i = 0; i < MMCLIENT_MAX_SIM_CLIENT; i++)
  {
    mm_sim_client_info[i].valid = MM_FALSE;
    mm_sim_client_info[i].st_hndl = NULL;
    mm_sim_client_info[i].mmclient_cb_func = NULL;
    mm_sim_client_info[i].mmclient_ind_cb_fn = NULL;
    mm_sim_client_info[i].user_data = NULL;
    mm_sim_client_info[i].nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;
  }

  MMCLIENT_SIM_LIST_MUTEX_UNLOCK();
  MMCLIENT_SIM_GLOBAL_UNLOCK();
  return MM_SUCCESS;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
