/******************************************************************************

                  M M C L I E N T _ U T I L . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <fcntl.h>
#include <errno.h>

#include "mmclient_util.h"

/*============================================================================
                            LOCAL DEFINITIONS
============================================================================*/

#ifdef MMCLIENT_SYSLOG_LOG
  #define MMCLIENT_LOG_MAXLEN 256
  #define MMCLIENT_LOG_PREFIX "MM_CLIENT "
#endif /* MMCLIENT_SYSLOG_LOG */

#ifdef MMCLIENT_FILE_LOG
/*============================================================================
  FUNCTION: mmclient_util_init_logfile
/*==========================================================================*/
/*!
@brief
  Helper function to initialize file based logging
*/
/*==========================================================================*/
static void mmclient_util_init_logfile(void)
{
  char proc_name[MAX_PROC_NAMELEN];
  char logfile_name[MAX_LOGFILE_NAMELEN];
  size_t bytes = 0;
  pid_t proc_id;

  log_to_file = TRUE;
  memset(proc_name, 0, sizeof(proc_name));
  memset(logfile_name, 0, sizeof(logfile_name));

  proc_id = getpid();
  mmclient_dbus_get_proc_name(proc_id, proc_name, MAX_PROC_NAMELEN);

  bytes = snprintf(logfile_name, sizeof(logfile_name), "%s/%s_%s_%ld.txt",
                   LOG_FILE_LOCATION, LOG_FILE_PREFIX, proc_name, (long) proc_id);
  if (bytes >= MAX_LOGFILE_NAMELEN)
  {
    /* String was truncated */
    logging_fd = stdout;
  }
  else
  {
    /* Open the logging file */
    /* TODO: Make the log file as a circular buffer */
    logging_fd = fopen(logfile_name, "w+");
    if (!logging_fd)
    {
      logging_fd = stdout;
      MMCLIENT_LOG_ERR("%s(): failed to open log file [%s]! Using stdout instead\n",
                       __func__, strerror(errno));
    }
  }
}
#endif /* MMCLIENT_FILE_LOG */

/*============================================================================
                            GLOBAL DEFINITIONS
/*==========================================================================*/
/*============================================================================
  FUNCTION: mmclient_util_get_proc_name
============================================================================*/
/*!
@brief
  Utility function to get the name of the process
*/
/*==========================================================================*/
int mmclient_util_get_proc_name
(
  pid_t proc_id,
  char  *name,
  int   namelen
)
{
  int      rc = MM_FAILURE;
  int      fd = -1;
  int      trunc = 0;
  int      bytes = 0;
  char     proc_entry[MAX_PROC_NAMELEN];
  char     temp[MAX_PROC_NAMELEN];
  ssize_t  num_read;
  char     *p = NULL;

  if (!name || namelen <= 0)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  /* The name can be determined from the proc entry */
  snprintf(proc_entry, sizeof(proc_entry), "/proc/%d/cmdline", proc_id);
  if (trunc >= MAX_PROC_NAMELEN)
  {
    MMCLIENT_LOG_ERR("%s(): process name truncated!\n", __func__);
    goto bail;
  }

  fd = open(proc_entry, O_RDONLY);
  if (fd < 0)
  {
    MMCLIENT_LOG_ERR("%s(): failed to open proc file! Err: %s\n",
                     __func__,strerror(errno));
    goto bail;
  }

  num_read = read(fd, temp, sizeof(temp) - 1);
  if (num_read <= 0)
  {
    MMCLIENT_LOG_ERR("%s(): failed to read from proc file! Err: %s\n",
                     __func__, strerror(errno));
    goto bail;
  }

  temp[num_read] = '\0';

  p = strrchr(temp, '/');
  if (NULL == p)
  {
    bytes = snprintf(name, namelen, "%s", temp);
    if (bytes >= namelen)
    {
      MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    }
  }
  else
  {
    bytes = snprintf(name, namelen, "%s", (p + 1));
    if (bytes >= namelen)
    {
      MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    }
  }

  rc = MM_SUCCESS;

bail:
  if (fd >= 0)
  {
    close(fd);
  }
  return rc;
}

/*============================================================================
  FUNCTION: mmclient_util_log_init
============================================================================*/
/*!
@brief
  Utility function to initialize logging state
*/
/*==========================================================================*/
void mmclient_util_log_init(void)
{
  log_level = LOG_LEVEL_VERBOSE;

#ifdef MMCLIENT_SYSLOG_LOG
  log_to_syslog = TRUE;

  proc_id = getpid();
  memset(proc_name, 0, sizeof(proc_name));

  mmclient_util_get_proc_name(proc_id, proc_name, MAX_PROC_NAMELEN);

  openlog(MMCLIENT_LOG_PREFIX, LOG_NDELAY, LOG_PID);
#endif /* MMCLIENT_SYSLOG_LOG */

#ifdef MMCLIENT_FILE_LOG
  log_to_file = TRUE;

  mmclient_util_init_logfile();
#endif /* MMCLIENT_FILE_LOG */
}

/*============================================================================
  FUNCTION: mmclient_util_log_deinit
============================================================================*/
/*!
@brief
  Utility function to de-initialize logging state
*/
/*==========================================================================*/
void mmclient_util_log_deinit(void)
{
#ifdef MMCLIENT_SYSLOG_LOG
  closelog();
  log_to_syslog = FALSE;
#endif /* MMCLIENT_SYSLOG_LOG */

#ifdef MMCLIENT_FILE_LOG
  /* Close the logging file */
  if (log_to_file == TRUE)
  {
    if (logging_fd)
    {
      fclose(logging_fd);
      logging_fd = NULL;
    }

    log_to_file = FALSE;
  }
#endif /* MMCLIENT_FILE_LOG */
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
