/******************************************************************************

                        M M C L I E N T . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/

#include <errno.h>
#include <signal.h>

#include "mmclient.h"
#include "mmclient_util.h"
#include "mmclient_dbus.h"

/* External cleanup functions*/
extern int mmclient_data_cleanup_state(void);
extern int mmclient_nw_reg_cleanup_state(void);
extern int mmclient_sim_cleanup_state(void);
extern void mmclient_bandwidth_cleanup_state(void);
extern void mmclient_dsd_cleanup_state(void);
extern int mmclient_nad_cleanup_state(void);

static int cleanup_complete = FALSE;

static mmclient_sys_cb_info_t *client_sys_cb_info;

/*=================================================================================
                            LOCAL DEFINITIONS
=================================================================================*/
/*=================================================================================
  FUNCTION: mmclient_signal_hdlr
=================================================================================*/
/*!
@brief
  Gracefully handle signals such has SIGINT
*/
/*===============================================================================*/
static void mmclient_signal_hdlr(int signo)
{
  struct sigaction act;

  switch (signo)
  {
  case SIGINT:
  case SIGTERM:
    {
      /* Perform cleanup */
      MMCLIENT_LOG_LOW("%s(): perform cleanup!\n", __func__);
      mmclient_deinit();

      /* Re-raise the signal and allow the default behavior of the
         signal to kick-in */
      memset(&act, 0, sizeof(act));
      act.sa_handler = SIG_DFL;
      (void) sigaction(signo, &act, NULL);
      raise(signo);
    }

    break;

  case SIGUSR1:
    {
      /* We start with default logging level as VERBOSE
         Init: Default set to VERBOSE
         Signal sent 1 time : Downgrade from VERBOSE to NONE
         Signal sent 2 times: Upgrade from NONE to ERROR
         Signal sent 3 times: Upgrade from ERROR to VERBOSE */
      if (log_level == LOG_LEVEL_VERBOSE)
      {
        log_level = LOG_LEVEL_NONE;
      }
      else if (log_level == LOG_LEVEL_NONE)
      {
        log_level = LOG_LEVEL_ERROR;
      }
      else if (log_level == LOG_LEVEL_ERROR)
      {
        log_level = LOG_LEVEL_VERBOSE;
      }
    }

    break;

  default:
    break;
  }
}

/*=================================================================================
                            GLOBAL DEFINITIONS
=================================================================================*/
/*=================================================================================
  FUNCTION: mmclient_init
=================================================================================*/
/*!
@brief
  Initialize client library
*/
/*===============================================================================*/
mmclient_status_t mmclient_init(mmclient_sys_cb_info_t *sys_cb_info)
{
  mmclient_status_t rc = MMCLIENT_FAILURE;
  struct sigaction act;

  /* Initialize logging module */
  mmclient_util_log_init();

  if (!sys_cb_info)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  client_sys_cb_info = sys_cb_info;

  /* Initialize DBus IPC connection */
  rc = mmclient_dbus_init();
  if (MMCLIENT_SUCCESS != rc)
  {
    goto bail;
  }

  memset(&act, 0, sizeof(act));
  act.sa_handler = &mmclient_signal_hdlr;

  sigaction(SIGTERM, &act, NULL);
  sigaction(SIGINT, &act, NULL);
  sigaction(SIGUSR1, &act, NULL);

  rc = MMCLIENT_SUCCESS;

  /* Register atexit handler for cleaning up any resources */
  atexit(mmclient_deinit);

bail:
  return rc;
}

/*=================================================================================
  FUNCTION: mmclient_process_sys_indications
=================================================================================*/
/*!
@brief
  Process sys indications
*/
/*===============================================================================*/
void mmclient_process_sys_indications(mmclient_sys_ind_info_t *sys_ind_info)
{
  if (!sys_ind_info)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return;
  }

  if (client_sys_cb_info && client_sys_cb_info->cb)
  {
    client_sys_cb_info->cb(sys_ind_info, client_sys_cb_info->userdata);
  }
}

/*=================================================================================
  FUNCTION: mmclient_cleanup_state
=================================================================================*/
/*!
@brief
  De-initialize state related to modem manager server
*/
/*===============================================================================*/
void mmclient_cleanup_state(void)
{
  (void) mmclient_data_cleanup_state();
  (void) mmclient_nw_reg_cleanup_state();
  (void) mmclient_sim_cleanup_state();
  (void) mmclient_bandwidth_cleanup_state();
  (void) mmclient_dsd_cleanup_state();
  (void) mmclient_nad_cleanup_state();
}

/*=================================================================================
  FUNCTION: mmclient_deinit
=================================================================================*/
/*!
@brief
  Cleanup library resources
*/
/*===============================================================================*/
void mmclient_deinit(void)
{
  if (cleanup_complete)
  {
    MMCLIENT_LOG_LOW("%s(): cleanup already complete!\n", __func__);
    return;
  }

  mmclient_cleanup_state();
  (void) mmclient_dbus_deinit();

  /* Un-initialize logging module */
  mmclient_util_log_deinit();

  cleanup_complete = TRUE;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
