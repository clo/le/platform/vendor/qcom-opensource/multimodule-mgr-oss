/******************************************************************************

                     M M C L I E N T _ M S G . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <string.h>

#include "mmclient_msg.h"
#include "mmclient_util.h"

/*============================================================================
                           LOCAL DECLARATIONS
============================================================================*/
static mmclient_client_id_t client_id = MMCLIENT_INVALID_CLIENT_ID;
static mmclient_txn_id_t    transaction_id = 0;

typedef struct
{
  mmclient_msg_id_t msg_id;
  char *method_name;
} mmclient_msg_tbl_t;

mmclient_msg_tbl_t mmclient_msg_tbl[] =
{
  { MSG_ID_NAD_GET_NUM_NAD_INSTANCES_REQ, METHOD_GET_NUM_NAD_INSTANCES },
  { MSG_ID_NAD_GET_NAD_HNDL_REQ,          METHOD_GET_NAD_HANDLE },
  { MSG_ID_NAD_GET_NAD_STATUS_REQ,        METHOD_GET_NAD_STATUS },
  { MSG_ID_NAD_SET_NAD_STATUS_REQ,        METHOD_SET_NAD_STATUS },
  { MSG_ID_NAD_GET_NAD_DEVICE_INFO_REQ,   METHOD_GET_NAD_DEVICE_INFO },
  { MSG_ID_NAD_RELEASE_NAD_HNDL_REQ,      METHOD_RELEASE_NAD_HANDLE },
  { MSG_ID_NAD_STATUS_CHANGE_IND,         SIGNAL_NAD_STATUS_CHANGE_IND },
  { MSG_ID_DATA_GET_SRVC_HNDL_REQ,        METHOD_DATA_GET_SRVC_HNDL },
  { MSG_ID_DATA_START_CALL_REQ,           METHOD_DATA_START_CALL },
  { MSG_ID_DATA_STOP_CALL_REQ,            METHOD_DATA_STOP_CALL },
  { MSG_ID_DATA_RELEASE_SRVC_HNDL_REQ,    METHOD_DATA_RELEASE_SRVC_HNDL },
  { MSG_ID_DATA_GET_CURR_BEARER_TECH_REQ, METHOD_DATA_GET_CURR_BEARER_TECH },
  { MSG_ID_DATA_GET_CALL_INFO_REQ,        METHOD_DATA_GET_CALL_INFO },
  { MSG_ID_DATA_GET_CALL_END_REASON_REQ,  METHOD_DATA_GET_CALL_END_REASON },
  { MSG_ID_DATA_CALL_INFO_IND,            SIGNAL_DATA_CALL_INFO_IND},
  { MSG_ID_PROFILE_QUERY_REQ,             METHOD_PROFILE_QUERY},
  { MSG_ID_PROFILE_CREATE_REQ,            METHOD_PROFILE_CREATE},
  { MSG_ID_PROFILE_DELETE_REQ,            METHOD_PROFILE_DELETE},
  { MSG_ID_PROFILE_MODIFY_REQ,            METHOD_PROFILE_MODIFY},
  { MSG_ID_NW_REG_GET_SRVC_HNDL_REQ,      METHOD_NW_REG_GET_SRVC_HNDL },
  { MSG_ID_NW_REG_RELEASE_SRVC_HNDL_REQ,  METHOD_NW_REG_RELEASE_SRVC_HNDL },
  { MSG_ID_NW_REG_GET_PROPERTIES_REQ,     METHOD_NW_REG_GET_PROPERTIES },
  { MSG_ID_NW_REG_REGISTER_NETWORK_REQ,   METHOD_NW_REG_REGISTER_NETWORK },
  { MSG_ID_NW_REG_GET_OPERATOR_REQ,       METHOD_NW_REG_GET_OPERATOR },
  { MSG_ID_NW_REG_SCAN_OPERATORS_REQ,     METHOD_NW_REG_SCAN_OPERATORS },
  { MSG_ID_NW_REG_GET_RADIO_SETTINGS_REQ, METHOD_NW_REG_GET_RADIO_SETTINGS },
  { MSG_ID_NW_REG_SET_RADIO_SETTINGS_REQ, METHOD_NW_REG_SET_RADIO_SETTINGS },
  { MSG_ID_NW_REG_REGISTER_IND_REQ,       METHOD_NW_REG_PROPERTIES_REGISTER_IND },
  { MSG_ID_NW_REG_GET_NETWORK_TIME_REQ,   METHOD_NW_REG_GET_NETWORK_TIME },
  { MSG_ID_NW_REG_PROPERTIES_IND,         SIGNAL_NW_REG_PROPERTIES_IND },
  { MSG_ID_NW_REG_SCAN_OPERATORS_IND,     SIGNAL_NW_REG_SCAN_OPERATORS_IND },
  { MSG_ID_SIM_GET_SRVC_HNDL_REQ,         METHOD_SIM_GET_SRVC_HNDL },
  { MSG_ID_SIM_RELEASE_SRVC_HNDL_REQ,     METHOD_SIM_RELEASE_SRVC_HNDL },
  { MSG_ID_SIM_ENTER_PIN_REQ,             METHOD_SIM_ENTER_PIN },
  { MSG_ID_SIM_CHANGE_PIN_REQ,            METHOD_SIM_CHANGE_PIN },
  { MSG_ID_SIM_RESET_PIN_REQ,             METHOD_SIM_RESET_PIN },
  { MSG_ID_SIM_SET_PIN_PROTECTION_REQ,    METHOD_SIM_SET_PIN_PROTECTION },
  { MSG_ID_SIM_GET_PROPERTIES_REQ,        METHOD_SIM_GET_PROPERTIES },
  { MSG_ID_SIM_REGISTER_IND_REQ,          METHOD_SIM_REGISTER_IND },
  { MSG_ID_SIM_READ_RECORD_REQ,           METHOD_SIM_READ_RECORD },
  { MSG_ID_SIM_READ_TRANSPARENT_REQ,      METHOD_SIM_READ_TRANSPARENT },
  { MSG_ID_SIM_GET_FILE_ATTRIBUTES_REQ,   METHOD_SIM_GET_FILE_ATTRIBUTES },
  { MSG_ID_SIM_AVAILABILITY_IND,          SIGNAL_SIM_AVAILABILITY_IND },
  { MSG_ID_SIM_PROPERTIES_IND,            SIGNAL_SIM_PROPERTIES_IND },
  { MSG_ID_ATTACH_GET_STATUS_REQ,         METHOD_ATTACH_GET_STATUS },
  { MSG_ID_ATTACH_SET_ATTACH_REQ,         METHOD_ATTACH_SET_ATTACH },
  { MSG_ID_BANDWIDTH_IND_REG_REQ,         METHOD_BW_IND_REG},
  { MSG_ID_BANDWIDTH_QUERY_REQ,           METHOD_BW_QUERY},
  { MSG_ID_BANDWIDTH_THROUGHPUT_IND,      SIGNAL_BW_IND},
  { MSG_ID_MDMMGR_CLIENT_DISCONNECT,      METHOD_MDMMGR_CLIENT_DISCONNECT },
  { MSG_ID_SYS_SERVER_DOWN_IND,           SIGNAL_SYS_SERVER_DOWN_IND },
  { MSG_ID_SYS_SERVER_READY_IND,          SIGNAL_SYS_SERVER_READY_IND },
  { MSG_ID_SYS_MODEM_OUT_OF_SERVICE_IND,  SIGNAL_SYS_MODEM_OUT_OF_SERVICE_IND },
  { MSG_ID_SYS_MODEM_IN_SERVICE_IND,      SIGNAL_SYS_MODEM_IN_SERVICE_IND },
  { MSG_ID_DATA_SYSTEM_IND_REG_REQ,       METHOD_DATA_SYSTEM_IND_REG},
  { MSG_ID_DATA_SYSTEM_QUERY_REQ,         METHOD_DATA_SYSTEM_QUERY},
  { MSG_ID_DATA_SYSTEM_IND,               SIGNAL_DATA_SYSTEM_IND},
  { MSG_ID_MDMMGR_CLIENT_DISCONNECT,      METHOD_MDMMGR_CLIENT_DISCONNECT }
};

#define MAX_MSGS (sizeof(mmclient_msg_tbl) / sizeof(mmclient_msg_tbl[0]))

/*============================================================================
  FUNCTION: mmclient_dbus_generate_txn_id
============================================================================*/
/*!
@brief
  Lookup local message ID table for method name
*/
/*==========================================================================*/
static mmclient_txn_id_t mmclient_dbus_generate_txn_id(void)
{
  /* Prevent integer overflow */
  if ((UINT32_MAX - 1) == transaction_id)
  {
    transaction_id = 0;
  }

  transaction_id++;

  return transaction_id;
}

/*============================================================================
  FUNCTION: mmclient_dbus_encode_msg_as_byte_array
============================================================================*/
/*!
@brief
  Helper function to encode DBus messages into a byte array

@arg iter        - Main DBus message iterator to which the sub-iterators are
                   appended
@arg msg_payload - The payload to encode
@arg msg_len     - The length of the payload

@return
  MMCLIENT_MSG_NO_ERR if there are no errors
  Other error codes from mmclient_msg_err_codes_t
  if errors are encountered

@note
  Dependencies
    - In order to encode the message into DBUS_TYPE_BYTE array we need to
    ensure that the data being encoded is a packed structure i.e. has a
    defined length
*/
/*==========================================================================*/
static int mmclient_dbus_encode_msg_as_byte_array
(
  DBusMessageIter  *iter,
  unsigned char    *msg_payload,
  int              msg_len
)
{
  DBusMessageIter iter_array;

  if (!iter || !msg_payload)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  /* Encode the header information as a byte array
     We need to specify the signature argument when initializing an array type iterator */
  if (!dbus_message_iter_open_container(iter, DBUS_TYPE_ARRAY, "y", &iter_array))
  {
    MMCLIENT_LOG_ERR("%s(): failed to open sub-container for array!\n", __func__);
    return MMCLIENT_MSG_ERR_ENCODE_FAIL;
  }

  dbus_message_iter_append_fixed_array(&iter_array, DBUS_TYPE_BYTE, &msg_payload, msg_len);
  dbus_message_iter_close_container(iter, &iter_array);

  return MMCLIENT_MSG_NO_ERR;
}

/*============================================================================
  FUNCTION: mmclient_dbus_decode_msg_as_byte_array
============================================================================*/
/*!
@brief
  Helper function to decode DBus messages encoded as byte array

@arg iter          - Main DBus message iterator to which the sub-iterators are
                     appended
@arg **msg_payload - Pointer to store the decoded information
@arg exp_msg_len   - Expected receive payload length

@return
  MMCLIENT_MSG_NO_ERR if there are no errors
  Other error codes from mmclient_msg_err_codes_t
  if errors are encountered

@note
  Dependencies
    - In order to encode the message into DBUS_TYPE_BYTE array we need to
    ensure that the data being encoded is a packed structure i.e. has a
    defined length
*/
/*==========================================================================*/
static int mmclient_dbus_decode_msg_as_byte_array
(
  DBusMessageIter  *iter,
  unsigned char    **msg_payload,
  int              exp_msg_len
)
{
  int recv_msg_len = 0;
  DBusMessageIter iter_array;

  if (!iter)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  /* While encoding the main outer iterator was of DBUS_TYPE_ARRAY */
  if (DBUS_TYPE_ARRAY != dbus_message_iter_get_arg_type(iter))
  {
    MMCLIENT_LOG_ERR("%s(): unexpected argument type! [%d]\n",
                     __func__, dbus_message_iter_get_arg_type(iter));
    return MMCLIENT_MSG_ERR_DECODE_FAIL;
  }

  /* After confirming the main argument type to be that of DBUS_TYPE_ARRAY
     we need to see whether the internal element type is indeed
     of DBUS_TYPE_BYTE */
  if (DBUS_TYPE_BYTE != dbus_message_iter_get_element_type(iter))
  {
    MMCLIENT_LOG_ERR("%s(): invalid element type received! Expecting "
                     "DBUS_TYPE_BYTE [%d] but received [%d]\n",
                     __func__, DBUS_TYPE_BYTE, dbus_message_iter_get_element_type(iter));
    return MMCLIENT_MSG_ERR_DECODE_FAIL;
  }

  /* Read the data into the payload pointer */
  dbus_message_iter_recurse(iter, &iter_array);

  /* Decode the received message into the msg_payload pointer */
  dbus_message_iter_get_fixed_array(&iter_array, msg_payload, &recv_msg_len);

  if (recv_msg_len != exp_msg_len)
  {
    MMCLIENT_LOG_ERR("%s(): expected len [%d] and don't match received len [%d]\n",
                     __func__, exp_msg_len, recv_msg_len);
    return MMCLIENT_MSG_ERR_DECODE_FAIL;
  }

  return MMCLIENT_MSG_NO_ERR;
}

/*============================================================================
  FUNCTION: mmclient_lookup_method_name
============================================================================*/
/*!
@brief
  Lookup local message ID table for method name
*/
/*==========================================================================*/
static char* mmclient_lookup_method_name(mmclient_msg_id_t msg_id)
{
  unsigned int i = 0;
  for (i = 0; i < MAX_MSGS; i++)
  {
    if (mmclient_msg_tbl[i].msg_id == msg_id)
    {
      return mmclient_msg_tbl[i].method_name;
    }
  }

  MMCLIENT_LOG_ERR("%s(): no matching method/signal name found for msg_id [%d]\n",
                   __func__, msg_id);
  return NULL;
}

/*============================================================================
                           GLOBAL DEFINITIONS
============================================================================*/
/*============================================================================
  FUNCTION: mmclient_get_client_id
============================================================================*/
/*!
@brief
  Returns the client ID received from the server or INVALID if none not set
*/
/*==========================================================================*/
mmclient_client_id_t mmclient_get_client_id(void)
{
  return client_id;
}

/*============================================================================
  FUNCTION: mmclient_get_msg_hdr
============================================================================*/
/*!
@brief
  Prepares an empty message header
*/
/*==========================================================================*/
mmclient_msg_hdr_t* mmclient_get_msg_hdr(void)
{
  mmclient_msg_hdr_t *msg_hdr = NULL;

  msg_hdr = (mmclient_msg_hdr_t*) malloc(sizeof(mmclient_msg_hdr_t));
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to allocate memory for msg header!\n", __func__);
    goto bail;
  }

  memset(msg_hdr, 0, sizeof(mmclient_msg_hdr_t));

  msg_hdr->srvc_id     = SRVC_ID_INVALID;
  msg_hdr->msg_id      = MSG_ID_INVALID;
  msg_hdr->client_id   = MMCLIENT_INVALID_CLIENT_ID;
  msg_hdr->txn_id      = MMCLIENT_INVALID_TXN_ID;
  msg_hdr->nad_inst    = MMCLIENT_NAD_INSTANCE_INVALID;
  msg_hdr->payload_len = MMCLIENT_INVALID_PAYLOAD_LEN;

bail:
  return msg_hdr;
}

/*============================================================================
  FUNCTION: mmclient_free_msg_hdr
============================================================================*/
/*!
@brief
  Frees the message header
*/
/*==========================================================================*/
void mmclient_free_msg_hdr
(
  mmclient_msg_hdr_t *msg_hdr
)
{
  if (msg_hdr)
  {
    free(msg_hdr);
  }
}

/*============================================================================
  FUNCTION: mmclient_dbus_encode_hdr
============================================================================*/
/*!
@brief
  Encodes the message header into DBus format
*/
/*==========================================================================*/
int mmclient_dbus_encode_hdr
(
  DBusMessageIter    *args,
  mmclient_msg_hdr_t *msg_hdr
)
{
  int             rc = MMCLIENT_MSG_NO_ERR;
  int             hdr_len = 0;
  unsigned char   *hdr_data = NULL;

  if (!args || !msg_hdr)
  {
    rc = MMCLIENT_MSG_ERR_INVALID_PARAMS;
    goto bail;
  }

  if (MMCLIENT_INVALID_TXN_ID == msg_hdr->txn_id)
  {
    msg_hdr->txn_id = mmclient_dbus_generate_txn_id();
  }

  /* The payload to be encoded will be contained in the char array */
  hdr_data = malloc(sizeof(mmclient_msg_hdr_t));
  if (!hdr_data)
  {
    MMCLIENT_LOG_ERR("%s(): failed to allocate memory!\n", __func__);
    rc = MMCLIENT_MSG_ERR_LOW_MEMORY;
    goto bail;
  }

  hdr_len = sizeof(mmclient_msg_hdr_t);

  /* Copy the contents of the received header structure into the byte array. Since the structure
     for the message header is packed with a well defined length the buffer defined above should
     be able to hold the entire information */
  memcpy(hdr_data, msg_hdr, sizeof(mmclient_msg_hdr_t));

  /* Encode header */
  if (MMCLIENT_MSG_NO_ERR
        != mmclient_dbus_encode_msg_as_byte_array(args, hdr_data, hdr_len))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode message as byte array!\n", __func__);
    rc = MMCLIENT_MSG_ERR_ENCODE_FAIL;
    goto bail;
  }

bail:
  if (hdr_data)
  {
    free(hdr_data);
  }

  return rc;
}

/*============================================================================
  FUNCTION: mmclient_dbus_decode_hdr
============================================================================*/
/*!
@brief
  Decodes header information from DBus message
*/
/*==========================================================================*/
mmclient_msg_hdr_t* mmclient_dbus_decode_hdr
(
  DBusMessage     *msg,
  DBusMessageIter *iter
)
{
  mmclient_msg_hdr_t *msg_hdr = NULL;
  mmclient_msg_hdr_t *dbus_hdr_data;
  unsigned char      *hdr_payload;

  if (!iter || !msg)
  {
    MMCLIENT_LOG_ERR("%s(): invalid arguments!\n", __func__);
    goto bail;
  }

  /* Step through the main iterator to reach the sub-iterators */
  if (MMCLIENT_MSG_NO_ERR
        != mmclient_dbus_decode_msg_as_byte_array(iter, &hdr_payload,
                                                  sizeof(mmclient_msg_hdr_t)))
  {
    MMCLIENT_LOG_ERR("%s(): failed to decode byte array payload!\n", __func__);
    goto bail;
  }

  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): could not allocate message header!\n", __func__);
    goto bail;
  }

  dbus_hdr_data = (mmclient_msg_hdr_t*) hdr_payload;

  msg_hdr->srvc_id     = dbus_hdr_data->srvc_id;
  msg_hdr->msg_id      = dbus_hdr_data->msg_id;
  msg_hdr->client_id   = dbus_hdr_data->client_id;
  msg_hdr->txn_id      = dbus_hdr_data->txn_id;
  msg_hdr->nad_inst    = dbus_hdr_data->nad_inst;
  msg_hdr->payload_len = dbus_hdr_data->payload_len;

  /* Move to the next argument in the main iterator */
  if (dbus_message_iter_has_next(iter))
  {
    dbus_message_iter_next(iter);
  }

bail:
  return msg_hdr;
}

/*============================================================================
  FUNCTION: mmclient_dbus_get_msg
============================================================================*/
/*!
@brief
  Helper method to return a DBusMessage object

@arg msg_type - to determine the type of message (request/response/signal)
@arg method_name - the name of the method to be invoked
@arg req_msg - if we need to construct a response message based on the request
               message
*/
/*==========================================================================*/
DBusMessage* mmclient_dbus_get_msg
(
  mmclient_msg_type_t  msg_type,
  mmclient_msg_id_t    msg_id,
  DBusMessage          *req_msg
)
{
  DBusMessage *msg = NULL;
  char *method_name = NULL;

  switch (msg_type)
  {
  case TYPE_REQUEST:
    {
      /* Lookup local method table to find the method name to be used for
         DBus RPC invocation */
      method_name = mmclient_lookup_method_name(msg_id);
      if (!method_name)
      {
        MMCLIENT_LOG_ERR("%s(): could not lookup method name for message [%d]\n",
                         __func__, msg_id);
        goto bail;
      }

      msg = dbus_message_new_method_call(MM_SERVER_NAME,
                                         MM_SERVER_OBJECT_PATH,
                                         MM_SERVER_INTERFACE_NAME,
                                         method_name);
      if (!msg)
      {
        MMCLIENT_LOG_ERR("%s(): failed to acquire message object!\n", __func__);
        goto bail;
      }
    }
    break;

  case TYPE_RESPONSE:
    {
      if (!req_msg)
      {
        MMCLIENT_LOG_ERR("%s(): enc_type requires a valid request message object to be passed!\n",
                   __func__);
        goto bail;
      }

      msg = dbus_message_new_method_return(req_msg);

      if (!msg)
      {
        MMCLIENT_LOG_ERR("%s(): failed to acquire message object!\n", __func__);
        goto bail;
      }
    }

    break;

  case TYPE_INDICATION:
    {
      method_name = mmclient_lookup_method_name(msg_id);
      if (!method_name)
      {
        MMCLIENT_LOG_ERR("%s(): could not lookup method name for message [%d]\n",
                         __func__, msg_id);
        goto bail;
      }

      msg = dbus_message_new_signal(MM_SERVER_OBJECT_PATH,
                                    MM_SERVER_INTERFACE_NAME,
                                    method_name);

      if (!msg)
      {
        MMCLIENT_LOG_ERR("%s(): failed to acquire signal message object!\n", __func__);
        goto bail;
      }
    }

    break;

  default:
    MMCLIENT_LOG_ERR("%s(): unknown message type [%d]\n", __func__, msg_type);
    break;
  }

bail:
  return msg;
}

/* In order to keep the IPC module self contained we need to implement a common
   encode and decode function for DBus IPC and keep it localized within this file
   instead of sprinkling the modem manager module with intermittent DBus specific
   APIs. In case we decide to have simultaneous IPC mechanisms in place or fully
   migrate to another IPC mechanism we need not change all the source files
   within modem manager */
/*============================================================================
  FUNCTION: mmclient_dbus_encode_msg
============================================================================*/
/*!
@brief
  Encodes the message in DBus format

@arg msg_hdr  - Message header
@arg args     - DBusMessageIter for encoding the message
@arg data     - Data to encode
@arg data_len - Length of the payload
*/
/*==========================================================================*/
int mmclient_dbus_encode_msg
(
  mmclient_msg_hdr_t   *msg_hdr,
  DBusMessageIter      *args,
  void                 *data,
  unsigned int         data_len
)
{
  unsigned char         *buffer = NULL;
  mmclient_srvc_id_t    srvc_id = SRVC_ID_INVALID;
  mmclient_msg_id_t     msg_id = MSG_ID_INVALID;
  mmclient_client_id_t  client_id = MMCLIENT_INVALID_CLIENT_ID;

  if (!msg_hdr || !args || !data
        || (data_len == 0 || data_len >= MMCLIENT_MAX_PAYLOAD_LEN))
  {
    MMCLIENT_LOG_ERR("%s(): invalid input parameters!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  srvc_id = msg_hdr->srvc_id;
  msg_id = msg_hdr->msg_id;
  client_id = msg_hdr->client_id;

  buffer = (unsigned char*) malloc(data_len);
  if (!buffer)
  {
    MMCLIENT_LOG_ERR("%s(): failed to allocate memory for buffer!\n", __func__);
    return MMCLIENT_MSG_ERR_LOW_MEMORY;
  }

  MMCLIENT_LOG_LOW("%s(): srvc_id [%d], msg_id [%d], client_id [%d], data_len [%d]\n",
                   __func__, srvc_id, msg_id, client_id, data_len);

  memcpy(buffer, data, data_len);
  if (MMCLIENT_MSG_NO_ERR
        != mmclient_dbus_encode_msg_as_byte_array(args, buffer, data_len))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode data!\n", __func__);
    if (buffer)
    {
      free(buffer);
    }
    return MMCLIENT_MSG_ERR_ENCODE_FAIL;
  }

  /* Free the buffer after encoding is complete */
  if (buffer)
  {
    free(buffer);
  }

  return MMCLIENT_MSG_NO_ERR;
}

/*=================================================================================
  FUNCTION: mmclient_dbus_decode_msg
=================================================================================*/
/*!
@brief
  Encodes the message in DBus format

@arg msg_hdr  - Message header
@arg args     - DBusMessageIter for decoding the message
@arg data     - Data to decode
@arg data_len - Length of the payload
*/
/*===============================================================================*/
int mmclient_dbus_decode_msg
(
  mmclient_msg_hdr_t   *msg_hdr,
  DBusMessageIter      *args,
  void                 *data,
  unsigned int         data_len
)
{
  unsigned char         *buffer = NULL;
  mmclient_srvc_id_t    srvc_id = SRVC_ID_INVALID;
  mmclient_msg_id_t     msg_id = MSG_ID_INVALID;
  mmclient_client_id_t  client_id = MMCLIENT_INVALID_CLIENT_ID;

  srvc_id = msg_hdr->srvc_id;
  msg_id = msg_hdr->msg_id;
  client_id = msg_hdr->client_id;

  if (!data || !args || !msg_hdr
        || (data_len == 0 || data_len >= MMCLIENT_MAX_PAYLOAD_LEN))
  {
    MMCLIENT_LOG_ERR("%s(): invalid parameters!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  MMCLIENT_LOG_LOW("%s(): srvc_id [%d], msg_id [%d], client_id [%d], data_len [%d]\n",
                   __func__, srvc_id, msg_id, client_id, data_len);

  if (MMCLIENT_MSG_NO_ERR
       != mmclient_dbus_decode_msg_as_byte_array(args, &buffer, data_len))
  {
    MMCLIENT_LOG_ERR("%s(): failed to decode data!\n", __func__);
    return MMCLIENT_MSG_ERR_DECODE_FAIL;
  }

  if (!buffer)
  {
    MMCLIENT_LOG_ERR("%s(): failed to decode data!\n", __func__);
    return MMCLIENT_MSG_ERR_DECODE_FAIL;
  }

  memcpy(data, (void*) buffer, data_len);

  return MMCLIENT_MSG_NO_ERR;
}

/*=================================================================================
  FUNCTION: mmclient_dbus_send_msg
=================================================================================*/
/*!
@brief
  Send via message via DBus

@arg conn     - DBusConnection reference
@arg msg      - DBusMessage reference
@arg hdr      - Message header
@arg data     - Data to be encoded and sent
@arg data_len - Length of the payload
*/
/*===============================================================================*/
int mmclient_dbus_send_msg
(
  DBusConnection       *conn,
  DBusMessage          *msg,
  mmclient_msg_hdr_t   *hdr,
  void                 *data,
  unsigned int         data_len
)
{
  int rc = MMCLIENT_MSG_NO_ERR;
  DBusMessageIter args;
  DBusError error;

  if (!conn || !hdr || !msg || !data
      || (data_len == 0 || data_len >= MMCLIENT_MAX_PAYLOAD_LEN))
  {
    MMCLIENT_LOG_ERR("%s(): invalid input params!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  dbus_error_init(&error);

  /* Prepare the header */
  dbus_message_iter_init_append(msg, &args);
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_encode_hdr(&args, hdr))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode header!\n", __func__);
    rc = MMCLIENT_MSG_ERR_ENCODE_FAIL;
    goto bail;
  }

  /* Encode message */
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_encode_msg(hdr, &args, data, data_len))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode message!\n", __func__);
    rc = MMCLIENT_MSG_ERR_ENCODE_FAIL;
    goto bail;
  }

  dbus_message_set_no_reply(msg, TRUE);

  if (!dbus_connection_send(conn, msg, NULL))
  {
    MMCLIENT_LOG_ERR("%s(): failed to send reply message!", __func__);
    rc = MMCLIENT_MSG_ERR_MSG_DISPATCH;
    goto bail;
  }

  /* Push the message onto the bus */
  dbus_connection_flush(conn);

bail:
  dbus_error_free(&error);
  return rc;
}

/*=================================================================================
  FUNCTION: mmclient_dbus_send_msg_sync
=================================================================================*/
/*!
@brief
  Send synchronous message via DBus

@arg conn          - DBusConnection reference
@arg msg           - DBusMessage reference
@arg hdr           - Message header
@arg req_data      - Data to be encoded and sent
@arg req_data_len  - Length of the payload
@arg resp_data     - Response data
@arg resp_data_len - Response data length
*/
/*===============================================================================*/
int mmclient_dbus_send_msg_sync
(
  DBusConnection      *conn,
  DBusMessage         *msg,
  mmclient_msg_hdr_t  *hdr,
  void                *req_data,
  unsigned int        req_data_len,
  void                *resp_data,
  unsigned int        resp_data_len
)
{
  int                   rc = MMCLIENT_MSG_NO_ERR;
  mmclient_msg_hdr_t    *resp_hdr = NULL;
  DBusMessageIter       req_iter;
  DBusMessageIter       resp_iter;
  DBusMessage           *reply_msg = NULL;
  DBusError             err;

  if (!conn || !hdr || !msg || !req_data
      || (req_data_len == 0 || req_data_len >= MMCLIENT_MAX_PAYLOAD_LEN)
      || (resp_data_len == 0 || resp_data_len >= MMCLIENT_MAX_PAYLOAD_LEN))
  {
    MMCLIENT_LOG_ERR("%s(): invalid input params!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  dbus_error_init(&err);

  /* Prepare request message arguments */

  /* Prepare the header */
  dbus_message_iter_init_append(msg, &req_iter);
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_encode_hdr(&req_iter, hdr))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode header!\n", __func__);
    goto bail;
  }

  /* Encode message */
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_encode_msg(hdr, &req_iter, req_data, req_data_len))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode message!\n", __func__);
    rc = MMCLIENT_MSG_ERR_ENCODE_FAIL;
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): sending message...\n", __func__);
  reply_msg = dbus_connection_send_with_reply_and_block(conn, msg,
                                                        MMCLIENT_DBUS_DEFAULT_TIMEOUT,
                                                        &err);
  if (dbus_error_is_set(&err))
  {
    MMCLIENT_LOG_ERR("%s(): Failed to send message!\nName: %s Err: %s\n",
                     __func__, err.name, err.message);
    rc = MMCLIENT_MSG_ERR_MSG_DISPATCH;
    goto bail;
  }

  /* Release the reference to the request message object */
  dbus_message_unref(msg);

  if (!reply_msg)
  {
    MMCLIENT_LOG_ERR("%s(): reply is NULL!\n", __func__);
    rc = MMCLIENT_MSG_ERR_MSG_DISPATCH;
    goto bail;
  }

  /* Decode the header */
  MMCLIENT_LOG_LOW("%s(): received response...decoding message\n", __func__);
  dbus_message_iter_init(reply_msg, &resp_iter);
  resp_hdr = mmclient_dbus_decode_hdr(reply_msg, &resp_iter);
  if (!resp_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to decode error!\n", __func__);
    rc = MMCLIENT_MSG_ERR_DECODE_FAIL;
    goto bail;
  }

  /* Save the client ID returned from DBus locally in case its set to the default value.
     Since each client has its own copy of the messaging library the client_id will be
     unique per client*/
  if (MMCLIENT_INVALID_CLIENT_ID == client_id)
  {
    client_id = resp_hdr->client_id;
  }

  /* Decode the response message and return it as part of the resp_data pointer */
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_decode_msg(resp_hdr, &resp_iter,
                                                      resp_data, resp_data_len))
  {
    MMCLIENT_LOG_ERR("%s(): msg decoding failed!\n", __func__);
    rc = MMCLIENT_MSG_ERR_DECODE_FAIL;
    goto bail;
  }

bail:
  dbus_error_free(&err);

  mmclient_free_msg_hdr(resp_hdr);

  /* Unreference the reply message after we have completed the decoding */
  if (reply_msg)
  {
    dbus_message_unref(reply_msg);
  }

  return rc;
}

/*=================================================================================
  FUNCTION: mmclient_dbus_send_signal
=================================================================================*/
/*!
@brief
  Send signal/indication/unsolicited messages via DBus

@arg conn     - DBusConnection reference
@arg msg      - DBusMessage reference
@arg hdr      - Message header
@arg data     - Data to be encoded and sent
@arg data_len - Length of the payload
*/
/*===============================================================================*/
int mmclient_dbus_send_signal
(
  DBusConnection     *conn,
  DBusMessage        *msg,
  mmclient_msg_hdr_t *hdr,
  void               *data,
  unsigned int       data_len
)
{
  DBusError error;
  DBusMessageIter sig_iter;

  if (!hdr || !data || !msg
        || (data_len == 0 || data_len >= MMCLIENT_MAX_PAYLOAD_LEN))
  {
    MMCLIENT_LOG_ERR("%s(): invalid input params!\n", __func__);
    return MMCLIENT_MSG_ERR_INVALID_PARAMS;
  }

  dbus_error_init(&error);

  dbus_message_iter_init_append(msg, &sig_iter);
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_encode_hdr(&sig_iter, hdr))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode header!\n", __func__);
    return MMCLIENT_MSG_ERR_ENCODE_FAIL;
  }

  /* Encode message
     The encoding format for signals will be
     Msg HDR + Service Handle + CB information */
  if (MMCLIENT_MSG_NO_ERR != mmclient_dbus_encode_msg(hdr, &sig_iter, data, data_len))
  {
    MMCLIENT_LOG_ERR("%s(): failed to encode message!\n", __func__);
    return MMCLIENT_MSG_ERR_ENCODE_FAIL;
  }

  if (!dbus_connection_send(conn, msg, NULL))
  {
    MMCLIENT_LOG_ERR("%s(): failed to send reply message!", __func__);
    return MMCLIENT_MSG_ERR_MSG_DISPATCH;
  }

  /* Push the message onto the bus */
  dbus_connection_flush(conn);

  dbus_error_free(&error);
  return MMCLIENT_MSG_NO_ERR;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
