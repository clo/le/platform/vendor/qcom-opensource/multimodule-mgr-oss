/******************************************************************************

                  M M C L I E N T _ D B U S . H

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <dbus/dbus.h>
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>

/*===========================================================================
                          GLOBAL DECLARATIONS
===========================================================================*/
#define MMCLIENT_DBUS_NAME               "org.codeaurora.mdmmgr.client"
#define MMCLIENT_DBUS_OBJECT_PATH        "/org/codeaurora/mdmmgr/client"
#define MMCLIENT_DBUS_INTERFACE_NAME     "org.codeaurora.mdmmgr.client"
#define MMCLIENT_DBUS_INTERFACE_PATH     MMCLIENT_DBUS_OBJECT_PATH "/Interfaces"

#define MMCLIENT_MAX_DBUS_STR_LEN 256
#define MAX_PROC_NAMELEN          50

typedef enum
{
  STATE_INVALID = -1,
  STATE_CONNECTED,
  STATE_DISCONNECTED
} conn_status_t;

typedef struct
{
  DBusConnection *conn;
  char           path[MMCLIENT_MAX_DBUS_STR_LEN];
  char           conn_name[MMCLIENT_MAX_DBUS_STR_LEN];
  conn_status_t  connection_status;
  uint16_t       client_id;
} mmclient_dbus_state_t;

/*============================================================================
  FUNCTION: mmclient_dbus_get_proc_name
============================================================================*/
/*!
@brief
  Utility function to get the name of the process
*/
/*==========================================================================*/
int mmclient_dbus_get_proc_name
(
  pid_t proc_id,
  char  *name,
  int   namelen
);

/*============================================================================
  FUNCTION: mmclient_dbus_init
============================================================================*/
/*!
@brief
  Initialize DBus connection

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*===========================================================================*/
int mmclient_dbus_init(void);

/*============================================================================
  FUNCTION: mmclient_get_connection_state
============================================================================*/
/*!
@brief
  Returns the DBus connection state object

@return
  mmclient_dbus_state_t reference
*/
/*===========================================================================*/
mmclient_dbus_state_t* mmclient_get_connection_state(void);

/*============================================================================
  FUNCTION: mmclient_dbus_deinit
============================================================================*/
/*!
@brief
  De-initialize DBus connection

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*===========================================================================*/
int mmclient_dbus_deinit();

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
