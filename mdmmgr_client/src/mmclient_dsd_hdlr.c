/******************************************************************************

             M M C L I E N T _ D S D _ H D L R . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/

#include <pthread.h>
#include "mmclient_dbus.h"
#include "mmclient_util.h"
#include "mmclient_msg.h"

/*=================================================================================
                             LOCAL DECLARATIONS
=================================================================================*/

#define MMCLIENT_MAX_DSD_HANDLES 10

struct mmclient_dsd_info_t
{
  mmclient_nad_inst_t            nad_inst;
  char                           nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_data_sys_cb_info_t    cb_info;
  struct mmclient_dsd_info_t   * next;
};

/* Save handle information locally */
struct mmclient_dsd_info_t *dsd_hndl_list;
static int list_size;

pthread_mutex_t clnt_dsd_list_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_DSD_LIST_MUTEX_LOCK()                             \
  MMCLIENT_LOG_LOW("%s(): locking DSD list mutex\n", __func__);    \
  pthread_mutex_lock(&clnt_dsd_list_mtx);

#define MMCLIENT_DSD_LIST_MUTEX_UNLOCK()                           \
  MMCLIENT_LOG_LOW("%s(): unlocking DSD list mutex\n", __func__);  \
  pthread_mutex_unlock(&clnt_dsd_list_mtx);

pthread_mutex_t mmclient_dsd_global_mutex = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_DSD_GLOBAL_LOCK()                                  \
  MMCLIENT_LOG_LOW("%s(): locking DSD global mutex\n", __func__);   \
  pthread_mutex_lock(&mmclient_dsd_global_mutex);

#define MMCLIENT_DSD_GLOBAL_UNLOCK()                                \
  MMCLIENT_LOG_LOW("%s(): unlocking DSD global mutex\n", __func__); \
  pthread_mutex_unlock(&mmclient_dsd_global_mutex);

extern mmclient_nad_inst_t mmclient_get_nad_inst_for_nad_hndl
(
  mmclient_hndl_t *nad_hndl
);

/*============================================================================
  FUNCTION  mmclient_alloc_dsd_hndl
============================================================================*/
  /*!
  @brief
    Stores information about the allocated DSD callback info in a list

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
static int mmclient_alloc_dsd_hndl
(
  char                        * nad_hndl,
  mmclient_data_sys_cb_info_t * cb_info
)
{
  int rc = MM_FAILURE;
  struct mmclient_dsd_info_t *dsd_hndl_entry = NULL;
  int                        bytes = 0;

  MMCLIENT_DSD_LIST_MUTEX_LOCK();

  if (!nad_hndl || !cb_info)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!\n", __func__);
    goto bail;
  }

  if (MMCLIENT_MAX_DSD_HANDLES <= list_size)
  {
    MMCLIENT_LOG_ERR("%s(): max handles reached!\n", __func__);
    goto bail;
  }

  dsd_hndl_entry = (struct mmclient_dsd_info_t *)
                        calloc(1, sizeof(struct mmclient_dsd_info_t));
  if (!dsd_hndl_entry)
  {
    MMCLIENT_LOG_ERR("%s(): failed to allocate memory for DSD handle!\n", __func__);
    goto bail;
  }

  bytes = snprintf(dsd_hndl_entry->nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s", nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occured!\n", __func__);
    free(dsd_hndl_entry);
    dsd_hndl_entry = NULL;
    MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
    return MM_FAILURE;
  }

  dsd_hndl_entry->nad_inst =
           mmclient_get_nad_inst_for_nad_hndl((mmclient_nad_hndl_t)nad_hndl);
  dsd_hndl_entry->cb_info = *cb_info;

  /* Insert into list */
  dsd_hndl_entry->next = dsd_hndl_list;
  dsd_hndl_list = dsd_hndl_entry;

  list_size++;
  MMCLIENT_LOG_LOW("%s(): successfully added DSD handle for NAD [%s] to list\n",
                    __func__, nad_hndl);

  rc = MM_SUCCESS;

bail:
  MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
  return rc;
}

/*============================================================================
  FUNCTION  mmclient_release_dsd_hndl
============================================================================*/
  /*!
  @brief
    Release dsd handle associated with a nad handle.

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
static int mmclient_release_dsd_hndl
(
  char * nad_hndl
)
{
  struct mmclient_dsd_info_t *prev = NULL;
  struct mmclient_dsd_info_t *curr = dsd_hndl_list;

  MMCLIENT_DSD_LIST_MUTEX_LOCK();

  while (curr != NULL)
  {
    if (!strcmp(nad_hndl, curr->nad_hndl))
    {
      MMCLIENT_LOG_MED("%s(): Releasing dsd hndl for nad %s\n",
                                        __func__, nad_hndl);
      if (curr == dsd_hndl_list)
      {
        dsd_hndl_list = curr->next;
      }
      else
      {
        prev->next = curr->next;
      }
      free(curr);
      list_size--;
      break;
    }
    else
    {
      prev = curr;
      curr = curr->next;
    }
  }

  MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
  return MM_SUCCESS;
}

/*============================================================================
  FUNCTION  mmclient_lookup_dsd_hndl
============================================================================*/
/*!
@brief
  Helper function to lookup DSD call information for the provided NAD handle

@return Valid handle information if it exists, NULL otherwise

@arg nad_hndl - NAD handle
*/
/*==========================================================================*/
static struct mmclient_dsd_info_t* mmclient_lookup_dsd_hndl
(
  char *nad_hndl
)
{
  struct mmclient_dsd_info_t *dsd_hndl_entry;

  MMCLIENT_DSD_LIST_MUTEX_LOCK();

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid NAD handle!\n", __func__);
    MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
    return NULL;
  }

  dsd_hndl_entry = dsd_hndl_list;

  while (dsd_hndl_entry != NULL)
  {
    if (!strcmp(dsd_hndl_entry->nad_hndl, nad_hndl))
    {
      MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
      return dsd_hndl_entry;
    }

    dsd_hndl_entry = dsd_hndl_entry->next;
  }

  MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
  return NULL;
}

/*============================================================================
                           GLOBAL DEFINITIONS
============================================================================*/
/*============================================================================
  FUNCTION  mmclient_dsd_ind_hdlr
============================================================================*/
/*!
  @brief
    Handler for dsd indications

  @return
    MMCLIENT_SUCCESS
    MMCLIENT_FAILURE

  @arg cb_params - indication parameters
*/
/*==========================================================================*/
mmclient_status_t mmclient_dsd_ind_hdlr
(
  mmclient_data_sys_info_param_t *cb_params
)
{
  struct mmclient_dsd_info_t *dsd_list_entry = NULL;

  if (!cb_params)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    return MMCLIENT_FAILURE;
  }

  MMCLIENT_LOG_MED("%s(): callback invoked for NAD [%s]!\n",
                  __func__, cb_params->nad_hndl);

  MMCLIENT_DSD_GLOBAL_LOCK();

  do
  {
    dsd_list_entry = mmclient_lookup_dsd_hndl((char *) cb_params->nad_hndl);
    if (!dsd_list_entry)
    {
      MMCLIENT_LOG_ERR("%s(): Received event on Invalid handle [%s] !\n",
                     __func__, cb_params->nad_hndl);
      break;
    }

    /* Invoke callback registered with the associated NAD handle */
    if (dsd_list_entry->cb_info.cb_func)
    {
      MMCLIENT_DSD_GLOBAL_UNLOCK();

      dsd_list_entry->cb_info.cb_func(
             &(cb_params->data_sys_info), dsd_list_entry->cb_info.user_data);

      return MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): client registered callback is invalid\n", __func__);
    }
  } while (0);

  MMCLIENT_DSD_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*============================================================================
  FUNCTION  mmclient_register_data_sys_status_ind
============================================================================*/
/*!
  @brief
    Function to register data system status indications

  @return
    MMCLIENT_SUCCESS
    MMCLIENT_FAILURE

  @arg nad_hndl    - NAD handle
  @arg cb_info     - Data System Status callback info
  @arg reg_options - Registratio options
*/
/*==========================================================================*/
mmclient_status_t mmclient_register_data_sys_status_ind
(
  mmclient_nad_hndl_t              nad_hndl,
  mmclient_data_sys_cb_info_t     *cb_info,
  mmclient_data_sys_reg_options_t *reg_options
)
{
  int                           ret = MMCLIENT_FAILURE;
  int                           reti = MM_FAILURE;
  int                           rc = MMCLIENT_MSG_NO_ERR;
  int                           bytes = 0;
  mmclient_data_sys_req_param_t dsd_query;
  mmclient_dbus_state_t         *dbus_client_state = NULL;
  mmclient_msg_hdr_t            *msg_hdr = NULL;
  DBusMessage                   *message = NULL;
  DBusConnection                *conn = NULL;
  struct mmclient_dsd_info_t    *dsd_hndl_entry = NULL;

  MMCLIENT_DSD_GLOBAL_LOCK();

  memset(&dsd_query, 0, sizeof(dsd_query));

  if (!cb_info)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    MMCLIENT_DSD_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }

  /* Make sure NAD handle is valid */
  if (MMCLIENT_NAD_INSTANCE_INVALID ==
             mmclient_get_nad_inst_for_nad_hndl(nad_hndl))
  {
    MMCLIENT_LOG_ERR("%s(): invalid nad handle\n", __func__);
    MMCLIENT_DSD_GLOBAL_UNLOCK();
    return MMCLIENT_FAILURE;
  }

  /* Send message via DBUS */
  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_SYSTEM_IND_REG_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_DATA_SYSTEM;
  msg_hdr->msg_id      = MSG_ID_DATA_SYSTEM_IND_REG_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_data_sys_req_param_t);

  MMCLIENT_LOG_MED("%s(): Registering for DSD indication for NAD handle %s!\n",
               __func__, (char *)nad_hndl);

  bytes = snprintf((char *) dsd_query.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  if (reg_options)
  {
    dsd_query.reg_options = *reg_options;
  }

  rc = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &dsd_query,
                                   (unsigned int) sizeof(mmclient_data_sys_req_param_t),
                                   (void*) &reti, (unsigned int) sizeof(int));

  if (rc != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, rc);
    goto bail;
  }

  if (reti != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): DSD ind registration failed for Nad handle %s!\n",
                     __func__, (char *)nad_hndl);
    goto bail;
  }
  else
  {
    MMCLIENT_LOG_MED("%s(): DSD ind registration succeeded for Nad handle %s!\n",
                     __func__, (char *)nad_hndl);
  }

  if (reg_options && reg_options->deregister)
  {
   /* Remove dsd hndl if deregistering */
    rc = mmclient_release_dsd_hndl((char *)nad_hndl);
  }
  else
  {
    /* Update existing handle or allocate a new one */
    dsd_hndl_entry = mmclient_lookup_dsd_hndl((char *)nad_hndl);
    if (dsd_hndl_entry != NULL)
    {
      dsd_hndl_entry->cb_info = *cb_info;
      rc = MM_SUCCESS;
    }
    else
    {
      rc = mmclient_alloc_dsd_hndl((char *)nad_hndl, cb_info);
    }
  }

  if (rc != MM_SUCCESS)
  {
    MMCLIENT_LOG_ERR("%s(): List allocation failed for NAD handle %s!\n",
                     __func__, (char *)nad_hndl);
    goto bail;
  }

  ret = MMCLIENT_SUCCESS;

bail:

  if (msg_hdr)
  {
    mmclient_free_msg_hdr(msg_hdr);
  }

  MMCLIENT_DSD_GLOBAL_UNLOCK();

  return ret;
}

/*============================================================================
  FUNCTION  mmclient_query_data_sys_status
============================================================================*/
/*!
  @brief
    Query current data system status

  @return
    MMCLIENT_SUCCESS
    MMCLIENT_FAILURE

  @arg nad_hndl      - NAD handle
       data_sys_info - data sys status
*/
/*==========================================================================*/
mmclient_status_t mmclient_query_data_sys_status
(
  mmclient_nad_hndl_t                  nad_hndl,
  mmclient_data_sys_status_ind_info_t *data_sys_info
)
{
  int                            ret = MMCLIENT_FAILURE;
  int                            rc = MMCLIENT_MSG_NO_ERR;
  int                            bytes = 0;
  mmclient_dbus_state_t          *dbus_client_state = NULL;
  mmclient_msg_hdr_t             *msg_hdr = NULL;
  DBusMessage                    *message = NULL;
  DBusConnection                 *conn = NULL;
  mmclient_data_sys_req_param_t  dsd_query;
  mmclient_data_sys_info_param_t dsd_resp;

  MMCLIENT_DSD_GLOBAL_LOCK();

  memset(&dsd_query, 0, sizeof(dsd_query));
  memset(&dsd_resp, 0, sizeof(dsd_resp));

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    goto bail;
  }

  /* Make sure NAD handle is valid */
  if (MMCLIENT_NAD_INSTANCE_INVALID ==
             mmclient_get_nad_inst_for_nad_hndl(nad_hndl))
  {
    MMCLIENT_LOG_ERR("%s(): invalid nad handle\n", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  dbus_client_state = mmclient_get_connection_state();
  if (!dbus_client_state)
  {
    MMCLIENT_LOG_ERR("%s(): could not get client object!\n", __func__);
    goto bail;
  }

  conn = dbus_client_state->conn;
  if (!conn)
  {
    MMCLIENT_LOG_ERR("%s(): connection ID is invalid!", __func__);
    goto bail;
  }

  /* Get message object */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_DATA_SYSTEM_QUERY_REQ, NULL);
  if(!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get request message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_DATA_SYSTEM;
  msg_hdr->msg_id      = MSG_ID_DATA_SYSTEM_QUERY_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_data_sys_req_param_t);

  MMCLIENT_LOG_MED("%s(): Querying for DSD evts for NAD handle %s!\n",
                   __func__, (char *)nad_hndl);

  bytes = snprintf((char *) dsd_query.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s",
                   (char *) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occurred!\n", __func__);
    goto bail;
  }

  rc = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &dsd_query,
                                   (unsigned int) sizeof(mmclient_data_sys_req_param_t),
                                   (void*) &dsd_resp,
                                   (unsigned int) sizeof(mmclient_data_sys_info_param_t));
  if(rc != MMCLIENT_MSG_NO_ERR)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, rc);
    goto bail;
  }

  *data_sys_info = dsd_resp.data_sys_info;

  ret = MMCLIENT_SUCCESS;

bail:

  if(msg_hdr)
  {
    mmclient_free_msg_hdr(msg_hdr);
  }

  MMCLIENT_DSD_GLOBAL_UNLOCK();

  return ret;
}

/*============================================================================
  FUNCTION  mmclient_dsd_cleanup_state_for_nad_inst
============================================================================*/
  /*!
  @brief
    Cleanup references related to the provided NAD instance

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
void mmclient_dsd_cleanup_state_for_nad_inst
(
  mmclient_nad_inst_t nad_inst
)
{
  struct mmclient_dsd_info_t *curr = NULL;
  struct mmclient_dsd_info_t *prev = NULL;

  if (MMCLIENT_NAD_INSTANCE_INVALID == nad_inst || list_size == 0)
  {
    /* NAD instance is invalid or list does not have any entries
       Nothing to be done */
    return;
  }

  MMCLIENT_DSD_GLOBAL_LOCK();
  MMCLIENT_DSD_LIST_MUTEX_LOCK();

  curr = dsd_hndl_list;

  while (curr != NULL)
  {
    if (curr->nad_inst == nad_inst)
    {
      MMCLIENT_LOG_MED("%s(): releasing dsd information for NAD handle [%s]"
                       " on NAD inst [%d]\n",
                       __func__, curr->nad_hndl, nad_inst);
      if (curr == dsd_hndl_list)
      {
        dsd_hndl_list = curr->next;
        list_size--;
        free(curr);
        curr = dsd_hndl_list;
      }
      else
      {
        prev->next = curr->next;
        list_size--;
        free(curr);
        curr = prev->next;
      }
    }
    else
    {
      prev = curr;
      curr = curr->next;
    }
  }

  MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
  MMCLIENT_DSD_GLOBAL_UNLOCK();
}

/*============================================================================
  FUNCTION  mmclient_dsd_cleanup_state
============================================================================*/
  /*!
  @brief
    Cleanup full dsd handle list

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
void mmclient_dsd_cleanup_state(void)
{
  struct mmclient_dsd_info_t *curr_node = NULL;
  struct mmclient_dsd_info_t *next_node = NULL;

  MMCLIENT_DSD_GLOBAL_LOCK();
  MMCLIENT_DSD_LIST_MUTEX_LOCK();

  MMCLIENT_LOG_LOW("%s(): cleaning up DSD state...\n", __func__);

  curr_node = dsd_hndl_list;
  while (curr_node != NULL)
  {
    next_node = curr_node->next;
    free(curr_node);
    list_size--;
    curr_node = next_node;
  }

  dsd_hndl_list = NULL;
  list_size = 0;

  MMCLIENT_DSD_LIST_MUTEX_UNLOCK();
  MMCLIENT_DSD_GLOBAL_UNLOCK();
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
