/******************************************************************************

             M M C L I E N T _ N A D _ H D L R . C

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <string.h>
#include <pthread.h>

#include "mmclient_util.h"
#include "mmclient_msg.h"
#include "mmclient_dbus.h"

/*=================================================================================
                             LOCAL DECLARATIONS
=================================================================================*/
#define MAX_NADS 5

struct nad_info_t
{
  char                   nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  mmclient_nad_inst_t    nad_inst;
  mmclient_nad_cb_info_t *cb_info;
  struct nad_info_t      *next;
};

/* List to keep track of NAD handle and corresponding callback information */
struct nad_info_t *nad_info_list = NULL;
static int list_size;

#define MMCLIENT_DBUS_GET_CONNECTION_OBJ(conn_state, conn)                      \
  conn_state = mmclient_get_connection_state();                                 \
  if (!conn_state)                                                              \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): failed to get connection state!\n", __func__);      \
    goto bail;                                                                  \
  }                                                                             \
                                                                                \
  conn = conn_state->conn;                                                      \
  if (!conn)                                                                    \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): invalid connection object!", __func__);             \
    goto bail;                                                                  \
  }                                                                             \
                                                                                \
  if (conn_state->connection_status != STATE_CONNECTED)                         \
  {                                                                             \
    MMCLIENT_LOG_ERR("%s(): DBus connection was terminated. "                   \
                     "Please reconnect!\n", __func__);                          \
    goto bail;                                                                  \
  }

pthread_mutex_t clnt_nad_list_mtx = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_NAD_LIST_MUTEX_LOCK()                             \
  MMCLIENT_LOG_LOW("%s(): locking nad list mutex\n", __func__);    \
  pthread_mutex_lock(&clnt_nad_list_mtx);

#define MMCLIENT_NAD_LIST_MUTEX_UNLOCK()                           \
  MMCLIENT_LOG_LOW("%s(): unlocking nad list mutex\n", __func__);  \
  pthread_mutex_unlock(&clnt_nad_list_mtx);

pthread_mutex_t mmclient_nad_global_mutex = PTHREAD_MUTEX_INITIALIZER;

#define MMCLIENT_NAD_GLOBAL_LOCK()                                  \
  MMCLIENT_LOG_LOW("%s(): locking nad global mutex\n", __func__);   \
  pthread_mutex_lock(&mmclient_nad_global_mutex);

#define MMCLIENT_NAD_GLOBAL_UNLOCK()                                \
  MMCLIENT_LOG_LOW("%s(): unlocking nad global mutex\n", __func__); \
  pthread_mutex_unlock(&mmclient_nad_global_mutex);

/*=================================================================================
  FUNCTION: mmclient_alloc_nad_info
=================================================================================*/
/*!
@brief
  Stores NAD handle information locally for bookeeping

@arg nad_inst - NAD instance requested by client
@arg nad_hndl - Handle to be saved
@arg cb_info  - Callback information passed by clients for the specific NAD

@note
  - Dependencies
    - mmclient_get_nad_handle should have been called

  - Side effects
    - None
*/
/*===============================================================================*/
static struct nad_info_t* mmclient_alloc_nad_info
(
  mmclient_nad_inst_t    nad_inst,
  char*                  nad_hndl,
  mmclient_nad_cb_info_t *cb_info
)
{
  int    bytes = 0;
  int    rc = MMCLIENT_FAILURE;
  struct nad_info_t *nad_entry = NULL;

  MMCLIENT_NAD_LIST_MUTEX_LOCK();

  if (!nad_hndl || !cb_info)
  {
    MMCLIENT_LOG_ERR("%s(): invalid arguments!\n", __func__);
    goto bail;
  }

  if (list_size == MAX_NADS)
  {
    MMCLIENT_LOG_ERR("%s(): cannot allocate any more nodes since we have reached the maximum",
                     __func__);
    goto bail;
  }

  nad_entry = (struct nad_info_t*) malloc(sizeof(struct nad_info_t));
  if (!nad_entry)
  {
    MMCLIENT_LOG_ERR("%s(): failed to allocate memory for NAD entry!\n", __func__);
    goto bail;
  }

  /* Valgrind error - ensure we always initialize variables allocated on heap */
  memset(nad_entry, 0, sizeof(struct nad_info_t));

  nad_entry->nad_inst = nad_inst;
  bytes = snprintf(nad_entry->nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s", nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): truncation occured while copying data!\n", __func__);
    free(nad_entry);
    nad_entry = NULL;
    goto bail;
  }

  nad_entry->cb_info = cb_info;

  /* Insert node into the head of the list */
  nad_entry->next = nad_info_list;
  nad_info_list = nad_entry;

  list_size++;

  MMCLIENT_LOG_LOW("%s(): successfully added NAD hndl [%s] to list, size: %d\n",
                   __func__, (char*) nad_entry->nad_hndl, list_size);

  rc = MMCLIENT_SUCCESS;

bail:
  MMCLIENT_NAD_LIST_MUTEX_UNLOCK();

  if (MMCLIENT_SUCCESS == rc)
  {
    return nad_entry;
  }

  return NULL;
}

/*=================================================================================
  FUNCTION: mmclient_lookup_nad_info
=================================================================================*/
/*!
@brief
  Lookup information for the given NAD handle

@arg nad_hndl - Handle to be saved

@note
  - Dependencies
    - mmclient_get_nad_handle should have been called

  - Side effects
    - None
*/
/*===============================================================================*/
static struct nad_info_t* mmclient_lookup_nad_info
(
  mmclient_nad_hndl_t hndl
)
{
  struct nad_info_t *nad_entry = NULL;
  int found = 0;
  char *comp_hndl;

  MMCLIENT_NAD_LIST_MUTEX_LOCK();

  if (!hndl)
  {
    MMCLIENT_LOG_ERR("%s(): NAD info lookup failed! Invalid NAD handle!\n", __func__);
    goto bail;
  }

  nad_entry = nad_info_list;
  while (nad_entry != NULL)
  {
    comp_hndl = (char*) hndl;

    if (!strcmp(nad_entry->nad_hndl, comp_hndl))
    {
      found = 1;
      break;
    }

    nad_entry = nad_entry->next;
  }

  if (!found)
  {
    MMCLIENT_LOG_ERR("%s(): NAD handle [%p] does not exist!\n", __func__, hndl);
  }

bail:
  MMCLIENT_NAD_LIST_MUTEX_UNLOCK();
  return nad_entry;
}

/*=================================================================================
  FUNCTION: mmclient_dealloc_nad_info
=================================================================================*/
/*!
@brief
  Search and remove NAD information from list
*/
/*===============================================================================*/
static mmclient_status_t mmclient_dealloc_nad_info
(
  char* hndl
)
{
  struct nad_info_t *curr = NULL;
  struct nad_info_t *prev = NULL;
  mmclient_status_t rc = MMCLIENT_FAILURE;

  MMCLIENT_NAD_LIST_MUTEX_LOCK();

  if (!hndl)
  {
    MMCLIENT_LOG_ERR("%s(): NAD dealloc failed! Invalid NAD handle!\n", __func__);
    goto bail;
  }

  if (0 == list_size)
  {
    MMCLIENT_LOG_LOW("%s(): List is already empty!\n", __func__);
    rc = MMCLIENT_SUCCESS;
    goto bail;
  }

  curr = nad_info_list;
  while (curr != NULL)
  {
    if (!strcmp(curr->nad_hndl, hndl))
    {
      if (curr == nad_info_list)
      {
        nad_info_list = curr->next;
      }
      else
      {
        prev->next = curr->next;
      }

      MMCLIENT_LOG_LOW("%s(): removing NAD handle [%s] from list\n",
                       __func__, (char*) curr->nad_hndl);
      rc = MMCLIENT_SUCCESS;
      list_size--;
      curr->cb_info = NULL;
      free(curr);
      break;
    }

    prev = curr;
    curr = curr->next;
  }

bail:
  MMCLIENT_NAD_LIST_MUTEX_UNLOCK();
  return rc;
}

/*============================================================================
                           GLOBAL DEFINITIONS
============================================================================*/
/*===========================================================================
  FUNCTION  mmclient_get_nad_inst_for_nad_hndl
===========================================================================*/
/*!
@brief
  Function to return the NAD instance associated with the given NAD handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle
*/
/*=========================================================================*/
mmclient_nad_inst_t mmclient_get_nad_inst_for_nad_hndl
(
  mmclient_hndl_t *nad_hndl
)
{
  mmclient_nad_inst_t nad_inst = MMCLIENT_NAD_INSTANCE_INVALID;
  struct nad_info_t   *nad_entry = NULL;

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid nad_hndl param!\n", __func__);
    goto bail;
  }

  nad_entry = mmclient_lookup_nad_info(nad_hndl);
  if (!nad_entry)
  {
    MMCLIENT_LOG_ERR("%s(): could not find information for handle [%s]\n",
                     __func__, (char*) nad_hndl);
    goto bail;
  }

  nad_inst = nad_entry->nad_inst;

bail:
  return nad_inst;
}

/*===========================================================================
  FUNCTION  mmclient_get_num_nad_instances
===========================================================================*/
/*!
@brief
  Function to query the number of configured NAD instances

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg *num_nads - will be filled with the number of configured NAD instances
*/
/*=========================================================================*/
mmclient_status_t mmclient_get_num_nad_instances(uint32_t *num_nads)
{
  int                   reti = MMCLIENT_MSG_NO_ERR;
  mmclient_status_t     rc = MMCLIENT_FAILURE;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusConnection        *conn = NULL;
  DBusMessage           *message = NULL;

  if (!num_nads)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params!", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NAD_GET_NUM_NAD_INSTANCES_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NAD;
  msg_hdr->msg_id      = MSG_ID_NAD_GET_NUM_NAD_INSTANCES_REQ;
  msg_hdr->client_id   = mmclient_get_client_id();
  msg_hdr->nad_inst    = MMCLIENT_NAD_INSTANCE_0;
  msg_hdr->payload_len = MMCLIENT_MAX_DBUS_NAME_LEN;

  /* Send message */
  reti = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) dbus_client_state->conn_name,
                                     MMCLIENT_MAX_DBUS_NAME_LEN, (void*) num_nads,
                                     (unsigned int) sizeof(uint32_t));
  if (MMCLIENT_MSG_NO_ERR != reti)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, reti);
    goto bail;
  }

  /* Get the client ID from the messaging library */
  dbus_client_state->client_id = mmclient_get_client_id();

  rc = MMCLIENT_SUCCESS;

bail:
  if (MMCLIENT_SUCCESS != rc)
  {
    *num_nads = 0;
  }

  mmclient_free_msg_hdr(msg_hdr);
  return rc;
}

/*===========================================================================
  FUNCTION  mmclient_get_nad_handle
===========================================================================*/
/*!
@brief
  Function to get a handle for the specified NAD instance

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_inst - NAD instance to connect
@arg *cb_info - Callback information

@note
  Dependencies
    - mmclient_get_num_nad_instances() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_hndl_t mmclient_get_nad_handle
(
  mmclient_nad_inst_t    nad_inst,
  mmclient_nad_cb_info_t *cb_info
)
{
  int                   reti = MMCLIENT_MSG_NO_ERR;
  char                  nad_hndl[MMCLIENT_MAX_SRVC_HNDL_LEN];
  struct nad_info_t*    nad_entry = NULL;
  mmclient_dbus_state_t *dbus_client_state = NULL;
  mmclient_msg_hdr_t    *msg_hdr = NULL;
  DBusConnection        *conn = NULL;
  DBusMessage           *message = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();

  if (!cb_info)
  {
    MMCLIENT_LOG_ERR("%s(): client did not pass cb_info for NAD!\n", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NAD_GET_NAD_HNDL_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NAD;
  msg_hdr->msg_id      = MSG_ID_NAD_GET_NAD_HNDL_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = nad_inst;
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_nad_inst_t);

  /* Send the message */
  reti = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void *) &nad_inst,
                                     (unsigned int) sizeof(mmclient_nad_inst_t),
                                     (void *) nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN);
  if (MMCLIENT_MSG_NO_ERR != reti)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, reti);
    goto bail;
  }

  if (!strcmp(nad_hndl, MMCLIENT_INVALID_NAD_HNDL))
  {
    MMCLIENT_LOG_ERR("%s(): invalid NAD handle received from server!\n", __func__);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): server response: NAD hndl: %s\n", __func__, nad_hndl);

  nad_entry = mmclient_alloc_nad_info(nad_inst, nad_hndl, cb_info);
  if (!nad_entry)
  {
    MMCLIENT_LOG_ERR("%s(): failed to add NAD information to list!\n", __func__);
    goto bail;
  }

  reti = MM_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);

  MMCLIENT_NAD_GLOBAL_UNLOCK();

  if (MM_SUCCESS == reti)
  {
    return (mmclient_nad_hndl_t) nad_entry->nad_hndl;
  }

  return NULL;
}

/*===========================================================================
  FUNCTION  mmclient_get_nad_status
===========================================================================*/
/*!
@brief
  Function to query current status of a given NAD

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl    - NAD handle
@arg *nad_status - status of the NAD will be filled here

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_get_nad_status
(
  mmclient_nad_hndl_t   nad_hndl,
  mmclient_nad_status_t *nad_status
)
{
  int                    reti = MMCLIENT_MSG_NO_ERR;
  mmclient_status_t      rc = MMCLIENT_FAILURE;
  mmclient_msg_hdr_t     *msg_hdr = NULL;
  DBusConnection         *conn = NULL;
  DBusMessage            *message = NULL;
  mmclient_dbus_state_t  *dbus_client_state = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();

  if (!nad_hndl || !nad_status)
  {
    MMCLIENT_LOG_ERR("%s(): invalid parameters!\n", __func__);
    goto bail;
  }

  if (!mmclient_lookup_nad_info(nad_hndl))
  {
    MMCLIENT_LOG_ERR("%s(): NAD handle does not exist!\n", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NAD_GET_NAD_STATUS_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NAD;
  msg_hdr->msg_id      = MSG_ID_NAD_GET_NAD_STATUS_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  /* Send the message */
  reti = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void *) nad_hndl,
                                     MMCLIENT_MAX_SRVC_HNDL_LEN, (void *) nad_status,
                                     (unsigned int) sizeof(mmclient_nad_status_t));

  if (MMCLIENT_MSG_NO_ERR != reti)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, reti);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): server response: NAD status: %d\n", __func__, *nad_status);

  if (*nad_status == MMCLIENT_NAD_STATUS_UNKNOWN)
  {
    MMCLIENT_LOG_ERR("%s(): NAD status failed on server or "
                     "status is unknown on server!\n", __func__);
    /* Todo: if server fails, should we still return success? */
  }

  rc = MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NAD_GLOBAL_UNLOCK();
  return rc;
}

/*===========================================================================
  FUNCTION  mmclient_set_nad_status
===========================================================================*/
/*!
@brief
  Function to set the status (operating mode) of a given NAD

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl   - NAD handle
@arg nad_status - status of the NAD

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_set_nad_status
(
  mmclient_nad_hndl_t   nad_hndl,
  mmclient_nad_status_t nad_status
)
{
  int                          ret = MMCLIENT_MSG_NO_ERR;
  int                          reti = MM_FAILURE;
  int                          bytes = 0;
  mmclient_status_t            rc = MMCLIENT_FAILURE;
  mmclient_msg_hdr_t           *msg_hdr = NULL;
  DBusConnection               *conn = NULL;
  DBusMessage                  *message = NULL;
  mmclient_dbus_state_t        *dbus_client_state = NULL;
  mmclient_nad_status_params_t nad_status_params;

  MMCLIENT_NAD_GLOBAL_LOCK();

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid parameters!\n", __func__);
    goto bail;
  }

  if (MMCLIENT_NAD_STATUS_UNKNOWN == nad_status)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params - MMCLIENT_NAD_STATUS_UNKNOWN\n", __func__);
    goto bail;
  }
  else if (MMCLIENT_NAD_OFFLINE == nad_status)
  {
    MMCLIENT_LOG_ERR("%s(): invalid params - setting NAD to offline is unsupported\n", __func__);
    goto bail;
  }

  if (!mmclient_lookup_nad_info(nad_hndl))
  {
    MMCLIENT_LOG_ERR("%s(): NAD handle does not exist!\n", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NAD_SET_NAD_STATUS_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NAD;
  msg_hdr->msg_id      = MSG_ID_NAD_SET_NAD_STATUS_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = (unsigned int) sizeof(mmclient_nad_status_params_t);

  bytes = snprintf(nad_status_params.nad_hndl, MMCLIENT_MAX_SRVC_HNDL_LEN, "%s", (char*) nad_hndl);
  if (bytes >= MMCLIENT_MAX_SRVC_HNDL_LEN)
  {
    MMCLIENT_LOG_ERR("%s(): failed to copy data, truncation occurred!\n", __func__);
    goto bail;
  }

  nad_status_params.nad_status = nad_status;

  ret = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) &nad_status_params,
                                    (unsigned int) sizeof(mmclient_nad_status_params_t),
                                    (void*) &reti, (unsigned int) sizeof(int));

  if(ret == MMCLIENT_MSG_NO_ERR)
  {
    if(reti == MM_SUCCESS)
    {
      MMCLIENT_LOG_LOW("%s(): Set nad status request succeeded - status=%d\n",
                       __func__, nad_status);
      rc = MMCLIENT_SUCCESS;
    }
    else
    {
      MMCLIENT_LOG_ERR("%s(): failed to get NAD status!\n", __func__);
    }
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, ret);
  }

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NAD_GLOBAL_UNLOCK();
  return rc;
}

/*===========================================================================
  FUNCTION  mmclient_get_nad_device_info
===========================================================================*/
/*!
@brief
  Function to get the device info from a given NAD

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl     - NAD handle
@arg *device_info - output param containing IMEI, MEID, and sw_version strings

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_get_nad_device_info
(
  mmclient_nad_hndl_t        nad_hndl,
  mmclient_nad_device_info_t *device_info
)
{
  int                    reti = MMCLIENT_MSG_NO_ERR;
  mmclient_status_t      rc = MMCLIENT_FAILURE;
  mmclient_msg_hdr_t     *msg_hdr = NULL;
  DBusConnection         *conn = NULL;
  DBusMessage            *message = NULL;
  mmclient_dbus_state_t  *dbus_client_state = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();

  if (!nad_hndl || !device_info)
  {
    MMCLIENT_LOG_ERR("%s(): invalid parameters!\n", __func__);
    goto bail;
  }

  if (!mmclient_lookup_nad_info(nad_hndl))
  {
    MMCLIENT_LOG_MED("%s(): NAD handle does not exist!\n", __func__);
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NAD_GET_NAD_DEVICE_INFO_REQ, NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NAD;
  msg_hdr->msg_id      = MSG_ID_NAD_GET_NAD_DEVICE_INFO_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  device_info->validity_mask = 0;
  /* Send the message */
  reti = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void *) nad_hndl,
                                     MMCLIENT_MAX_SRVC_HNDL_LEN, (void *) device_info,
                                     (unsigned int) sizeof(mmclient_nad_device_info_t));

  if (MMCLIENT_MSG_NO_ERR != reti)
  {
    MMCLIENT_LOG_ERR("%s(): request failed! [%d]\n", __func__, reti);
    goto bail;
  }

  MMCLIENT_LOG_LOW("%s(): device_info->validity_mask: 0x%x\n",
                   __func__, device_info->validity_mask);
  MMCLIENT_LOG_LOW("%s():   IMEI: %s\n",
                   __func__, device_info->imei);
  MMCLIENT_LOG_LOW("%s():   MEID: %s\n",
                   __func__, device_info->meid);
  MMCLIENT_LOG_LOW("%s():   sw_version: %s\n",
                   __func__, device_info->sw_version);

  if (device_info->validity_mask == 0)
  {
    MMCLIENT_LOG_ERR("%s(): device info invalid - nothing set on server\n", __func__);
    goto bail;
  }

  rc = MMCLIENT_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NAD_GLOBAL_UNLOCK();
  return rc;
}

/*===========================================================================
  FUNCTION  mmclient_release_nad_handle
===========================================================================*/
/*!
@brief
  Function to release a particular NAD reference

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle to release

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_release_nad_handle
(
  mmclient_nad_hndl_t nad_hndl
)
{
  int                    reti = MMCLIENT_MSG_NO_ERR;
  mmclient_status_t      rc = MMCLIENT_FAILURE;
  mmclient_msg_hdr_t     *msg_hdr = NULL;
  DBusConnection         *conn = NULL;
  DBusMessage            *message = NULL;
  mmclient_dbus_state_t  *dbus_client_state = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();

  if (!nad_hndl)
  {
    MMCLIENT_LOG_ERR("%s(): invalid parameters!\n", __func__);
    goto bail;
  }

  if (!mmclient_lookup_nad_info(nad_hndl))
  {
    MMCLIENT_LOG_MED("%s(): NAD handle does not exist! Nothing to do, returning success\n",
                     __func__);
    rc = MMCLIENT_SUCCESS;
    goto bail;
  }

  /* Send message via DBUS */
  MMCLIENT_DBUS_GET_CONNECTION_OBJ(dbus_client_state, conn);

  /* Get empty message */
  message = mmclient_dbus_get_msg(TYPE_REQUEST, MSG_ID_NAD_RELEASE_NAD_HNDL_REQ , NULL);
  if (!message)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message object!\n", __func__);
    goto bail;
  }

  /* Fill header information */
  msg_hdr = mmclient_get_msg_hdr();
  if (!msg_hdr)
  {
    MMCLIENT_LOG_ERR("%s(): failed to get message header!\n", __func__);
    goto bail;
  }

  msg_hdr->srvc_id     = SRVC_ID_NAD;
  msg_hdr->msg_id      = MSG_ID_NAD_RELEASE_NAD_HNDL_REQ;
  msg_hdr->client_id   = dbus_client_state->client_id;
  msg_hdr->nad_inst    = mmclient_get_nad_inst_for_nad_hndl(nad_hndl);
  msg_hdr->payload_len = MMCLIENT_MAX_SRVC_HNDL_LEN;

  /* Send the message */
  reti = mmclient_dbus_send_msg_sync(conn, message, msg_hdr, (void*) nad_hndl,
                                     MMCLIENT_MAX_SRVC_HNDL_LEN, (void*) &rc,
                                     (unsigned int) sizeof(int));

  if (MMCLIENT_MSG_NO_ERR != reti)
  {
    MMCLIENT_LOG_ERR("%s(): request failed [%d]!\n", __func__, reti);
    goto bail;
  }

  if (MMCLIENT_SUCCESS != rc)
  {
    MMCLIENT_LOG_ERR("%s(): NAD release failed!\n", __func__);
    goto bail;
  }

  /* Release the NAD reference from list */
  rc = mmclient_dealloc_nad_info(nad_hndl);
  if (MMCLIENT_SUCCESS != rc)
  {
    MMCLIENT_LOG_ERR("%s(): failed to cleanup NAD from local bookeeping table!\n", __func__);
    goto bail;
  }

  rc = MM_SUCCESS;

bail:
  mmclient_free_msg_hdr(msg_hdr);
  MMCLIENT_NAD_GLOBAL_UNLOCK();
  return rc;
}

/*===========================================================================
  FUNCTION  mmclient_process_nad_callback
===========================================================================*/
/*!
@brief
  Function to invoke callback registered with the NAD handle

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE

@arg nad_hndl - NAD handle to release

@note
  Dependencies
    - mmclient_get_nad_handle() should have been called

  Side-effects
    - None
*/
/*=========================================================================*/
mmclient_status_t mmclient_process_nad_callback
(
  nad_cb_info_t *nad_ind
)
{
  struct nad_info_t *nad_info = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();

  if (!nad_ind)
  {
    MMCLIENT_LOG_ERR("%s(): invalid param!\n", __func__);
    goto bail;
  }

  nad_info = mmclient_lookup_nad_info((void*) nad_ind->nad_hndl);
  if (!nad_info)
  {
    MMCLIENT_LOG_ERR("%s(): NAD lookup failed!\n", __func__);
    goto bail;
  }

  /* Call the registered callback function */
  if (nad_info->cb_info && nad_info->cb_info->cb)
  {
    MMCLIENT_NAD_GLOBAL_UNLOCK();
    nad_info->cb_info->cb(&nad_ind->ind_info, nad_info->cb_info->userdata);

    return MMCLIENT_SUCCESS;
  }
  else
  {
    MMCLIENT_LOG_ERR("%s(): Client registered callback is invalid!\n", __func__);
  }

bail:
  MMCLIENT_NAD_GLOBAL_UNLOCK();
  return MMCLIENT_FAILURE;
}

/*============================================================================
  FUNCTION  mmclient_nad_cleanup_state_for_nad_inst
============================================================================*/
  /*!
  @brief
    Cleanup references related to the provided NAD instance

  @return
    MM_SUCCESS
    MM_FAILURE
  */
/*==========================================================================*/
void mmclient_nad_cleanup_state_for_nad_inst(mmclient_nad_inst_t nad_inst)
{
  struct nad_info_t *curr = NULL;
  struct nad_info_t *prev = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();
  MMCLIENT_NAD_LIST_MUTEX_LOCK();

  if (MMCLIENT_NAD_INSTANCE_INVALID == nad_inst || list_size == 0)
  {
    /* NAD instance is invalid or list does not have any entries
       Nothing to be done */
    MMCLIENT_NAD_LIST_MUTEX_UNLOCK();
    MMCLIENT_NAD_GLOBAL_UNLOCK();
    return;
  }

  curr = nad_info_list;

  while (curr != NULL)
  {
    if (curr->nad_inst == nad_inst)
    {
      MMCLIENT_LOG_MED("%s(): cleanup state for NAD handle [%s] on NAD inst [%d]\n",
                       __func__, curr->nad_hndl, nad_inst);
      if (curr == nad_info_list)
      {
        nad_info_list = curr->next;
        list_size--;
        free(curr);
        curr = nad_info_list;
      }
      else
      {
        prev->next = curr->next;
        list_size--;
        free(curr);
        curr = prev->next;
      }
    }
    else
    {
      prev = curr;
      curr = curr->next;
    }
  }

  MMCLIENT_NAD_LIST_MUTEX_UNLOCK();
  MMCLIENT_NAD_GLOBAL_UNLOCK();
}

/*===========================================================================
  FUNCTION  mmclient_nad_cleanup_state
===========================================================================*/
/*!
@brief
  Function to cleanup resources

@return
  MMCLIENT_SUCCESS
  MMCLIENT_FAILURE
*/
/*=========================================================================*/
int mmclient_nad_cleanup_state(void)
{
  /* Cleanup resources */
  struct nad_info_t* curr_node = NULL;
  struct nad_info_t* next_node = NULL;

  MMCLIENT_NAD_GLOBAL_LOCK();

  curr_node = nad_info_list;
  while (curr_node != NULL)
  {
    /* Move onto the next node before the current one becomes free */
    next_node = curr_node->next;

    (void) mmclient_dealloc_nad_info(curr_node->nad_hndl);

    curr_node = next_node;
  }

  nad_info_list = NULL;
  list_size = 0;

  MMCLIENT_NAD_GLOBAL_UNLOCK();
  return MM_SUCCESS;
}

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
