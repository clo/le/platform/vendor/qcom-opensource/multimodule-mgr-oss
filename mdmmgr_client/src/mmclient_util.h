/******************************************************************************

                  M M C L I E N T _ U T I L . H

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifdef FEATURE_DATAOSS_TARGET_MCTM
#ifndef _MMCLIENT_UTIL_H_
#define _MMCLIENT_UTIL_H_
/*============================================================================
                             INCLUDE FILES
============================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <unistd.h>
#include <inttypes.h>
#include <time.h>
#include <syslog.h>

/*===========================================================================
                     GLOBAL DEFINITIONS AND DECLARATIONS
===========================================================================*/

#define MM_SERVER_NAME            "com.qualcomm.qti.mdmmgr.server"
#define MM_SERVER_OBJECT_PATH     "/com/qualcomm/qti/mdmmgr/service"
#define MM_SERVER_INTERFACE_NAME  "com.qualcomm.qti.mdmmgr.service"
#define MM_SERVER_INTERFACE_PATH  MM_SERVER_OBJECT_PATH "/Interfaces"
#define MM_SERVER_ERROR_TAG       MM_SERVER_NAME ".Error"

#define TRUE  1
#define FALSE 0

#define MM_SUCCESS 1
#define MM_FAILURE 0

#define LOG_FILE_LOCATION    "/data/misc/mmclient"
#define LOG_FILE_PREFIX      "mmclient_log"
#define MAX_LOGFILE_NAMELEN  512

#define MAX_PROC_NAMELEN     50

#define MAX_TIME_FORMAT_LEN  10
#define MAX_TIME_STAMP_LEN   25

#define LOG_LEVEL_NONE       0x0
#define LOG_LEVEL_ERROR      0x1
#define LOG_LEVEL_VERBOSE    0x3

/* Different logging options for client. To switch logging uncomment
   the desired mode, comment out the other and recompile the code */
#define MMCLIENT_SYSLOG_LOG 1
/*#define MMCLIENT_FILE_LOG   2*/

int log_level;

/*============================================================================
  FUNCTION: mmclient_util_get_proc_name
============================================================================*/
/*!
@brief
  Utility function to get the name of the process
*/
/*==========================================================================*/
int mmclient_util_get_proc_name
(
  pid_t proc_id,
  char  *name,
  int   namelen
);

/*============================================================================
  FUNCTION: mmclient_util_log_init
============================================================================*/
/*!
@brief
  Utility function to initialize logging state
*/
/*==========================================================================*/
void mmclient_util_log_init(void);

/*============================================================================
  FUNCTION: mmclient_util_log_deinit
============================================================================*/
/*!
@brief
  Utility function to de-initialize logging state
*/
/*==========================================================================*/
void mmclient_util_log_deinit(void);

#define TIME_STAMP(time_stamp)                                \
  struct    timeval tv;                                       \
  struct    tm *tmstmp;                                       \
  uint32_t  msec;                                             \
  char      tm_info[MAX_TIME_FORMAT_LEN];                     \
  gettimeofday(&tv, NULL);                                    \
  tmstmp = localtime(&tv.tv_sec);                             \
  msec = tv.tv_usec / 1000;                                   \
  strftime(tm_info, sizeof(tm_info), "%H:%M:%S", tmstmp);     \
  snprintf(time_stamp, sizeof(time_stamp), "%s.%" PRIu32 "",  \
           tm_info,                                           \
           msec);

#ifdef MMCLIENT_SYSLOG_LOG
  int   log_to_syslog;
  char  proc_name[MAX_PROC_NAMELEN];
  pid_t proc_id;

  #define  MMCLIENT_LOG_ERR(fmt, args...)                               \
    do                                                                  \
    {                                                                   \
      if (log_to_syslog && (log_level & LOG_LEVEL_ERROR))               \
      {                                                                 \
        syslog(LOG_ERR, "%s (%d):%s:%d (ERR) " fmt, proc_name,          \
                          proc_id, __FILE__, __LINE__, ##args);         \
      }                                                                 \
    } while(0);

  #define  MMCLIENT_LOG_HIGH(fmt, args...)                              \
    do                                                                  \
    {                                                                   \
      if (log_to_syslog && (log_level == LOG_LEVEL_VERBOSE))            \
      {                                                                 \
        syslog(LOG_WARNING, "%s (%d):%s:%d (HIGH) " fmt, proc_name,     \
                              proc_id, __FILE__, __LINE__, ##args);     \
      }                                                                 \
    } while(0);

  #define  MMCLIENT_LOG_MED(fmt, args...)                               \
    do                                                                  \
    {                                                                   \
      if (log_to_syslog && (log_level & LOG_LEVEL_ERROR))               \
      {                                                                 \
        syslog(LOG_NOTICE, "%s (%d):%s:%d (MED) " fmt, proc_name,       \
                             proc_id, __FILE__, __LINE__, ##args);      \
      }                                                                 \
    } while(0);

  #define  MMCLIENT_LOG_LOW(fmt, args...)                               \
    do                                                                  \
    {                                                                   \
      if (log_to_syslog && (log_level == LOG_LEVEL_VERBOSE))            \
      {                                                                 \
        syslog(LOG_INFO, "%s (%d):%s:%d (LOW) " fmt, proc_name,         \
                           proc_id, __FILE__, __LINE__, ##args);        \
      }                                                                 \
    } while(0);
#endif /* MMCLIENT_SYSLOG_LOG */

#ifdef MMCLIENT_FILE_LOG
  FILE *logging_fd;
  int log_to_file;

  #define MMCLIENT_LOG_ERR(...)                                         \
    do                                                                  \
    {                                                                   \
      if (log_to_file && logging_fd && (log_level & LOG_LEVEL_ERROR))   \
      {                                                                 \
        char timestamp[MAX_TIME_STAMP_LEN];                             \
        TIME_STAMP(timestamp);                                          \
        fprintf(logging_fd,"%25s %70s %40s():%3u (ERR):",               \
                timestamp, __FILE__, __func__, __LINE__);               \
        fprintf(logging_fd,__VA_ARGS__);                                \
        fprintf(logging_fd,"\n");                                       \
        fflush(logging_fd);                                             \
      }                                                                 \
    } while(0);

  #define MMCLIENT_LOG_HIGH(...)                                        \
    do                                                                  \
    {                                                                   \
      if (log_to_file && logging_fd && (log_level == LOG_LEVEL_VERBOSE))\
      {                                                                 \
        char timestamp[MAX_TIME_STAMP_LEN];                             \
        TIME_STAMP(timestamp);                                          \
        fprintf(logging_fd,"%25s %70s %40s():%3u (HIGH):",              \
                timestamp, __FILE__, __func__, __LINE__);               \
        fprintf(logging_fd,__VA_ARGS__);                                \
        fprintf(logging_fd,"\n");                                       \
        fflush(logging_fd);                                             \
      }                                                                 \
    } while(0);

  #define MMCLIENT_LOG_MED(...)                                         \
    do                                                                  \
    {                                                                   \
      if (log_to_file && logging_fd && (log_level & LOG_LEVEL_ERROR))   \
      {                                                                 \
        char timestamp[MAX_TIME_STAMP_LEN];                             \
        TIME_STAMP(timestamp);                                          \
        fprintf(logging_fd,"%25s %70s %40s():%3u (MED):",               \
                timestamp, __FILE__, __func__, __LINE__);               \
        fprintf(logging_fd,__VA_ARGS__);                                \
        fprintf(logging_fd,"\n");                                       \
        fflush(logging_fd);                                             \
      }                                                                 \
    } while(0);

  #define MMCLIENT_LOG_LOW(...)                                         \
    do                                                                  \
    {                                                                   \
      if (log_to_file && logging_fd && (log_level == LOG_LEVEL_VERBOSE))\
      {                                                                 \
        char timestamp[MAX_TIME_STAMP_LEN];                             \
        TIME_STAMP(timestamp);                                          \
        fprintf(logging_fd,"%25s %70s %40s():%3u (LOW):",               \
                timestamp, __FILE__, __func__, __LINE__);               \
        fprintf(logging_fd,__VA_ARGS__);                                \
        fprintf(logging_fd,"\n");                                       \
        fflush(logging_fd);                                             \
      }                                                                 \
    } while(0);
#endif /* MMCLIENT_FILE_LOG */

#endif /* _MMCLIENT_UTIL_H_ */

#endif /* FEATURE_DATAOSS_TARGET_MCTM */
